﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using ImGuizmoNET;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using Plane = System.Numerics.Plane;
using Vector3 = System.Numerics.Vector3;
using System.Runtime.InteropServices;

namespace Apex.Lib {
    public unsafe class CameraH {
        public Matrix4x4 ViewTransform { get; private set; } = Matrix4x4.Identity;
        public Matrix4x4 Projection { get; private set; } = Matrix4x4.Identity;
        public Matrix4x4 ViewProj { get; private set; } = Matrix4x4.Identity;
        public Box2D ViewPortSize { get; private set; } = new Box2D();
        public Plane[] FrustumPlanes { get; private set; } = new Plane[6];
        public Coordinates Coordinates { get; private set; } = new Coordinates(0, 0, 0);

        public CameraH() {

        }

        public void Update() {
            try {
                Coordinates = new Coordinates(
                    SmartBox.smartbox[0]->viewer.objcell_id,
                    SmartBox.smartbox[0]->viewer.frame.m_fOrigin.x,
                    SmartBox.smartbox[0]->viewer.frame.m_fOrigin.y,
                    SmartBox.smartbox[0]->viewer.frame.m_fOrigin.z);
                ViewTransform = GetViewTransform();
                Projection = GetProjection();
                ViewProj = ViewTransform * Projection;

                if (UIElementManager.s_pInstance != null) {
                    var rootEl = *UIElementManager.s_pInstance->m_pRootElement;
                    var viewport = ((UIElement*)GetChildRecursive(ref rootEl, 0x1000049Au));
                    if (viewport != null) {
                        ViewPortSize = viewport->a0.m_box;
                    }
                }

                UpdateFrustumPlanes();
            }
            catch (Exception ex) {
                UtilityBelt.Service.UBService.LogException(ex);
            }
        }

        public bool ToScreen(Coordinates coords, out Vector2 screenPos) {
            var a = new Vector3(coords.LocalX, coords.LocalY, coords.LocalZ);

            return ToScreen(a, out screenPos);
        }

        public bool ToScreen(Position pos, Vector3 a, out Vector2 screenPos) {
            var at = Vector3.Transform(a, pos.ToMatrix());

            return ToScreen(at, out screenPos);
        }

        public bool ToScreen(Vector3 a, out Vector2 screenPos) {
            var isOnScreen = true;
            var matrix_screen_pos = Matrix4x4.CreateTranslation(a) * ViewProj;
            var l = 1f / matrix_screen_pos.M44;

            var vx = matrix_screen_pos.M41 * l;
            var vy = matrix_screen_pos.M42 * l;
            var vz = matrix_screen_pos.M43 * l;

            if (Math.Abs(vx) > 1 || Math.Abs(vy) > 1 || vz > 1.00002) {
                isOnScreen = false;
            }

            var x = (float)Math.Floor(((vx + 1f) * 0.5f) * ViewPortSize.Width());
            var y = (float)Math.Floor(((1f - vy) * 0.5f) * ViewPortSize.Height());

            screenPos = new Vector2(ViewPortSize.m_x0 + x, ViewPortSize.m_y0 + y);

            return isOnScreen;
        }

        private static AC1Legacy.Vector3* Frame_globaltolocal(ref Frame This, AC1Legacy.Vector3* res, AC1Legacy.Vector3* _in) => ((delegate* unmanaged[Thiscall]<ref Frame, AC1Legacy.Vector3*, AC1Legacy.Vector3*, AC1Legacy.Vector3*>)0x004526C0)(ref This, res, _in);
        //.text:004526C0 ; float *__thiscall Frame::globaltolocal(float *this, float *, float *)

        private unsafe static int GetChildRecursive(ref UIElement This, uint _ID) {
            return ((delegate* unmanaged[Thiscall]<ref UIElement, uint, int>)4602880)(ref This, _ID);
        }

        private Matrix4x4 GetProjection() {
            var aspectRatio = RenderDevice.render_device[0]->m_ViewportAspectRatio;
            var fov = SmartBox.smartbox[0]->m_fGameFOV / (aspectRatio - 0.1f);

            return Matrix4x4.CreatePerspectiveFieldOfView(fov, aspectRatio, *Render.znear, *Render.zfar);
        }

        private Matrix4x4 GetViewTransform() {
            var smartbox = SmartBox.smartbox[0];
            var viewerPos = smartbox->viewer;
            var m_fl2gv = viewerPos.frame.m_fl2gv;

            var _in = new AC1Legacy.Vector3();
            var p = new AC1Legacy.Vector3();
            Frame_globaltolocal(ref viewerPos.frame, &p, &_in);

            var xAxis = new Vector3(m_fl2gv[0], m_fl2gv[1], m_fl2gv[2]);
            // Negate Y-axis for right-handed system
            var yAxis = -new Vector3(m_fl2gv[3], m_fl2gv[4], m_fl2gv[5]);
            var zAxis = new Vector3(m_fl2gv[6], m_fl2gv[7], m_fl2gv[8]);

            var res = Matrix4x4.Identity;

            res.M11 = xAxis.X;
            res.M12 = zAxis.X;
            res.M13 = yAxis.X;
            res.M14 = 0;

            res.M21 = xAxis.Y;
            res.M22 = zAxis.Y;
            res.M23 = yAxis.Y;
            res.M24 = 0;

            res.M31 = xAxis.Z;
            res.M32 = zAxis.Z;
            res.M33 = yAxis.Z;
            res.M34 = 0;

            res.M41 = p.a0.x;
            res.M42 = p.a0.z;
            res.M43 = -p.a0.y; // Negate Z-axis for right-handed system
            res.M44 = 1;

            // Right-handed coordinate system adjustment
            var rhBasis = new Matrix4x4(
                1, 0, 0, 0,
                0, 1, 0, 0,
                0, 0, 1, 0,
                0, 0, 0, 1);

            res = Matrix4x4.Multiply(rhBasis, res);

            return res;
        }

        private void UpdateFrustumPlanes() {
            FrustumPlanes[0].Normal = new Vector3(
                ViewProj.M14 - ViewProj.M11,
                ViewProj.M24 - ViewProj.M21,
                ViewProj.M34 - ViewProj.M31);
            FrustumPlanes[0].D = ViewProj.M44 - ViewProj.M41;

            FrustumPlanes[1].Normal = new Vector3(
                ViewProj.M14 + ViewProj.M11,
                ViewProj.M24 + ViewProj.M21,
                ViewProj.M34 + ViewProj.M31);
            FrustumPlanes[1].D = ViewProj.M44 + ViewProj.M41;

            FrustumPlanes[2].Normal = new Vector3(
                ViewProj.M14 - ViewProj.M12,
                ViewProj.M24 - ViewProj.M22,
                ViewProj.M34 - ViewProj.M32);
            FrustumPlanes[2].D = ViewProj.M44 - ViewProj.M42;

            FrustumPlanes[3].Normal = new Vector3(
                ViewProj.M14 + ViewProj.M12,
                ViewProj.M24 + ViewProj.M22,
                ViewProj.M34 + ViewProj.M32);
            FrustumPlanes[3].D = ViewProj.M44 + ViewProj.M42;

            FrustumPlanes[4].Normal = new Vector3(
                ViewProj.M14 - ViewProj.M13,
                ViewProj.M24 - ViewProj.M23,
                ViewProj.M34 - ViewProj.M33);
            FrustumPlanes[4].D = ViewProj.M44 - ViewProj.M43;

            FrustumPlanes[5].Normal = new Vector3(
                ViewProj.M14 + ViewProj.M13,
                ViewProj.M24 + ViewProj.M23,
                ViewProj.M34 + ViewProj.M33);
            FrustumPlanes[5].D = ViewProj.M44 + ViewProj.M43;

            for (var i = 0; i < 6; i++) {
                FrustumPlanes[i] = Plane.Normalize(FrustumPlanes[i]);
            }
        }
    }


    [StructLayout(LayoutKind.Sequential, Pack = 4)]
    public struct CameraFrame {
        public IntPtr vtable;
        public uint landblock;

        // quat
        public float qw, qx, qy, qz;

        // matrix
        public float m11, m12, m13;
        public float m21, m22, m23;
        public float m31, m32, m33;

        // vec
        public float x, y, z;

        public unsafe static CameraFrame Get(int obj) {
            CameraFrame* f = (CameraFrame*)obj;
            return *f;
        }

        public override string ToString() {
            return $"landblock: 0x{landblock:X8}\nqw: {qw}, qx: {qx}, qy: {qy}, qz: {qz}\nm11: {m11}, m12: {m12}, m13: {m13}\nm21: {m21}, m22: {m22}, m23: {m23}\nm31: {m31}, m32: {m32}, m33: {m33}\nx: {x}, y: {y}, z: {z}";
        }
    }

    public static class Camera {
        public static CameraFrame GetFrame() {
            return CameraFrame.Get(CoreManager.Current.Actions.Underlying.SmartboxPtr() + 0x08);
        }

        public static Microsoft.DirectX.Matrix GetD3DViewTransform() {
            var cameraFrame = GetFrame();

            // build a new temporary matrix from cameraFrame, just to get our translation
            // (swapping y/z, which converts from ac coords to d3d coords system)
            var m = new Microsoft.DirectX.Matrix() {
                M11 = cameraFrame.m11,
                M12 = cameraFrame.m13,
                M13 = cameraFrame.m12,
                M21 = cameraFrame.m21,
                M22 = cameraFrame.m23,
                M23 = cameraFrame.m22,
                M31 = cameraFrame.m31,
                M32 = cameraFrame.m33,
                M33 = cameraFrame.m32,
                M41 = cameraFrame.x,
                M42 = cameraFrame.z,
                M43 = cameraFrame.y,
                M44 = 1
            };

            // if we invert the matrix above, we get a good M41,M42,M43 for translation
            m.Invert();

            // (convert from row major to column major)
            var viewTransform = new Microsoft.DirectX.Matrix() {
                M11 = cameraFrame.m11,
                M12 = cameraFrame.m31,
                M13 = cameraFrame.m21,
                M14 = 0,
                M21 = cameraFrame.m13,
                M22 = cameraFrame.m33,
                M23 = cameraFrame.m23,
                M24 = 0,
                M31 = cameraFrame.m12,
                M32 = cameraFrame.m32,
                M33 = cameraFrame.m22,
                M34 = 0,
                M41 = m.M41,
                M42 = m.M43,
                M43 = m.M42,
                M44 = 1
            };

            return viewTransform;
        }
    }
}
