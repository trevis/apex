﻿using Decal.Adapter;
using ImGuiNET;
using Apex.Lib.Extensions;
using ProtoBuf.WellKnownTypes;
using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service.Lib.Settings;

namespace Apex.Lib {
    // https://roguesharp.wordpress.com/2014/07/13/tutorial-5-creating-a-2d-camera-with-pan-and-zoom-in-monogame/
    public class MapCamera2D {
        private bool _isDragging = false;
        private Vector2 _lastDrag;
        private Vector2 _dragStart = Vector2.Zero;

        // Centered Position of the Camera in pixels.
        public Vector2 Position { get; private set; }
        // Current Zoom level with 1.0f being standard
        public float Zoom { get; private set; }
        // Current Rotation amount with 0.0f being standard orientation
        public float Rotation { get; private set; }

        public Vector2 ViewportSize { get; set; }
        public Vector2 MapSize { get; set; }

        // Center of the Viewport which does not account for scale
        public Vector2 ViewportCenter {
            get {
                return new Vector2(ViewportSize.X * 0.5f, ViewportSize.Y * 0.5f);
            }
        }

        // Create a matrix for the camera to offset everything we draw,
        // the map and our objects. since the camera coordinates are where
        // the camera is, we offset everything by the negative of that to simulate
        // a camera moving. We also cast to integers to avoid filtering artifacts.
        public Matrix3x2 TranslationMatrix {
            get {
                return Matrix3x2.CreateTranslation(-Position.X, -Position.Y) *
                   Matrix3x2.CreateRotation(Rotation) *
                   Matrix3x2.CreateScale(new Vector2(Zoom)) *
                   Matrix3x2.CreateTranslation(ViewportCenter);
            }
        }

        public Vector2 RenderPosition { get; internal set; }

        public MapCamera2D() {
            Zoom = 0.475f;
        }

        // Call this method with negative values to zoom out
        // or positive values to zoom in. It looks at the current zoom
        // and adjusts it by the specified amount. If we were at a 1.0f
        // zoom level and specified -0.5f amount it would leave us with
        // 1.0f - 0.5f = 0.5f so everything would be drawn at half size.
        public void AdjustZoom(float amount, Vector2? position = null) {
            var maxZoom = 30f;
            var s = (float)Math.Min(Math.Log10(Zoom + 0.9f) * 10f, maxZoom);
            var ms = ((MapSize) - (ViewportSize / Zoom / 2f)) - (ViewportSize / Zoom / 2f);
            Zoom += amount * s;
            var ms2 = ((MapSize) - (ViewportSize / Zoom / 2f)) - (ViewportSize / Zoom / 2f);
            if ((ms2.X < 0 || ms2.Y < 0) && !(ms2.X > ms.X && ms2.Y > ms.Y)) {
                Zoom -= amount * s;
            }
            else if (position.HasValue) {
                Position += (position.Value - Position) / Zoom * (amount * s);
            }

            Position = MapClampedPosition(Position);
        }

        // Move the camera in an X and Y amount based on the cameraMovement param.
        // if clampToMap is true the camera will try not to pan outside of the
        // bounds of the map.
        public void MoveCamera(Vector2 cameraMovement, bool clampToMap = false) {
            Vector2 newPosition = Position + cameraMovement;

            if (clampToMap) {
                Position = MapClampedPosition(newPosition);
            }
            else {
                Position = newPosition;
            }
        }

        public Rectangle ViewportWorldBoundry() {
            Vector2 viewPortCorner = ScreenToWorld(new Vector2(0, 0));
            Vector2 viewPortBottomCorner = ScreenToWorld(ViewportSize);

            return new Rectangle((int)viewPortCorner.X,
               (int)viewPortCorner.Y,
               (int)(viewPortBottomCorner.X - viewPortCorner.X),
               (int)(viewPortBottomCorner.Y - viewPortCorner.Y));
        }

        // Center the camera on specific pixel coordinates
        public void CenterOn(Vector2 position) {
            Position = position;
        }

        // Center the camera on a specific cell in the map 
        public void CenterOn(Coordinates coords) {
            Position = CenteredPosition(coords, true);
        }

        private Vector2 CenteredPosition(Coordinates coords, bool clampToMap = false) {
            var cameraPosition = ((CoordsToScreen(coords) - RenderPosition - ViewportSize / 2f) / Zoom);
            if (clampToMap) {
                return MapClampedPosition(cameraPosition);
            }

            return cameraPosition;
        }

        // Clamp the camera so it never leaves the visible area of the map.
        private Vector2 MapClampedPosition(Vector2 position) {
            var cameraMax = (MapSize) - (ViewportSize / Zoom / 2f);

            return Vector2.Clamp(position, ViewportSize / Zoom / 2f, cameraMax);
        }

        public Vector2 WorldToScreen(Vector2 worldPosition) {
            return Vector2.Transform(worldPosition, TranslationMatrix) + RenderPosition;
        }

        public Vector2 ScreenToWorld(Vector2 screenPosition) {
            Matrix3x2.Invert(TranslationMatrix, out var res);
            return Vector2.Transform(screenPosition - RenderPosition, res);
        }

        public Vector2 CoordsToScreen(Coordinates coords) {
            var offset = new Vector2((coords.LBX()) * 192f + coords.LocalX, (0xFE - coords.LBY()) * 192f + 192f - coords.LocalY);
            offset = offset / ((0xFF * 192f)) * MapSize;
            return WorldToScreen(offset);
        }

        public Coordinates ScreenToCoords(Vector2 screenPosition) {
            var worldPos = ScreenToWorld(screenPosition);
            var offset = (new Vector2(worldPos.X, MapSize.Y - worldPos.Y)) * 192f / (MapSize / 0xFF);
            var landblock = ((uint)Math.Floor(offset.X / 192f) << 24) + ((uint)Math.Floor(offset.Y / 192f) << 16);

            return new Coordinates(landblock, offset.X % 192f, offset.Y % 192f, 0);
        }

        public void HandleInput() {
            if (ImGui.IsItemHovered() && ImGui.GetIO().MouseWheel != 0) {
                AdjustZoom(ImGui.GetIO().MouseWheel * 0.05f, ScreenToWorld(ImGui.GetMousePos()));
            }

            if (ImGui.IsItemActive() && ImGui.IsMouseDragging(ImGuiMouseButton.Left)) {
                if (!_isDragging) {
                    _dragStart = ImGui.GetMousePos();
                }
                else {
                    MoveCamera((_lastDrag - ImGui.GetMousePos()) / Zoom, true);
                }
                _isDragging = true;
                _lastDrag = ImGui.GetMousePos();
            }
            else if (_isDragging) {
                //_panOffset = _panOffset + ImGui.GetMousePos() - _dragStart;
                _isDragging = false;
            }
        }
    }
}
