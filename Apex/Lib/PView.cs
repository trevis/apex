﻿using AcClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.Settings;

namespace Apex.Lib {
    public unsafe struct PView {
        public portal_view_type outside_view;
        public int draw_landscape;
        public CBldPortal** outdoor_portal_list;
        public DArray<CEnvCell> cell_draw_list; // CEnvCell*
        public uint cell_draw_num;
        public DArray<CellListType> cell_todo_list; // CellListType*
        public uint cell_todo_num;
        public LScape* lscape;
    };

    public unsafe struct CellListType {
        public CEnvCell* cell;
        float dist;
    };

    public unsafe struct RenderMesh {
        public DBObj* DBObj;
        public uint m_InstanceFlags;
        public uint m_MeshAppearanceType;
        public int m_pMaterialArray;
        public int m_pVerticesArray;
        public int m_pIndicesArray;
        public SmartArray<int> m_Fragments;
        public SmartArray<ulong> m_RenderLODFragmentIndices;
        public SmartArray<long> m_OccluderFragmentIndices;
        public uint m_ShadowLODFragmentIndex;
        public bool m_CachedSupportsLighting;
        public bool m_CachedSupportsMultiPassLighting;
        public bool m_CachedSupportsCombinedAmbientPass;
        public bool m_CachedIsGlowing;
        public bool m_CachedNeedAlphaBlendPass;
        public bool m_CachedHasSkeletalData;
        public bool m_CachedAllSkeletalLODsWereBlended;
        public uint m_CachedSkeletalRenderLODIndex;
        public uint m_CachedSkeletalShadowVolumeLODIndex;
        public BBox m_CachedRenderBoundingBox;
        public BBox m_CachedSkinnedBoundingBox;
        public bool m_IsSkinnedBoundingBoxValid;
    };
}
