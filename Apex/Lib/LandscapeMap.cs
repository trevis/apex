﻿using Decal.Adapter;
using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Metrics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views;
/*
namespace Apex.Lib {
    public class LandscapeMap : IDisposable {
        private const int MIN_MAP_WIDTH = 50;
        private const int MIN_MAP_HEIGHT = 50;
        private const uint MAP_BORDER_COLOR = 0xAAAAAAAA;

        private ManagedTexture? _mapTexture;
        private Vector2 _contentSize = Vector2.One;
        private float _contentAspectRatio;
        private Vector2 _contentUpperLeft;
        private Vector2 _contentLowerRight;

        private MapCamera2D _camera;
        private bool _textureIsLoading = false;
        private bool _isFirstRender;
        private bool _showPopup;
        private Coordinates _contextOpenPosition;
        private bool _didInitContextData;

        public Coordinates? RouteStart { get; private set; } = null;
        public Coordinates? RouteEnd { get; private set; } = null;

        public LandscapeMap() {
            _camera = new MapCamera2D();
        }

        private async void CalculateRoute() {
            _path = null;

            if (RouteStart is null || RouteEnd is null)
                return;

            var path = await PluginCore.Instance.Nav.FindWorldPath(RouteStart, RouteEnd);

            if (path is not null) {
                _path = path;
            }
        }

        private void Update() {
            try {
                if (!_textureIsLoading && _mapTexture is null) {
                    _textureIsLoading = true;
                    Task.Run(() => {
                        var sw = new System.Diagnostics.Stopwatch();
                        sw.Start();
                        _mapTexture = new ManagedTexture(System.IO.Path.Combine(PluginCore.AssemblyDirectory, "Resources", "landscapemap.jpg"));

                        // dds loads a bit faster but is generally much bigger filesize than png from what i've seen...
                        //using var stream = System.IO.File.OpenRead(System.IO.Path.Combine(PluginCore.AssemblyDirectory, "Resources", "ac-landscape-map.dds"));
                        //_texture = TextureLoader.FromStream(PluginCore.D3Ddevice, stream, 4096, 4096, 1, Usage.None, Format.Dxt1, Pool.Default, Filter.None, Filter.None, 1);

                        sw.Stop();
                        CoreManager.Current.Actions.AddChatText($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to load landscape map texture", 1);
                        _isFirstRender = true;
                    });
                }

                var available = ImGui.GetContentRegionAvail();
                _contentSize.X = Math.Max(MIN_MAP_WIDTH, available.X);
                _contentSize.Y = Math.Max(MIN_MAP_HEIGHT, available.Y);
                _contentAspectRatio = _contentSize.X / _contentSize.Y;
                _contentUpperLeft = ImGui.GetWindowPos() + ImGui.GetCursorPos();
                _contentLowerRight = _contentUpperLeft + _contentSize;

                _camera.ViewportSize = _contentSize;
                _camera.RenderPosition = _contentUpperLeft;

                if (_mapTexture is not null) {
                    _camera.MapSize = new Vector2(_mapTexture.Bitmap.Width, _mapTexture.Bitmap.Height);
                }
            }
            catch { }
        }

        public unsafe void Render() {
            try {

                ImGui.Text($"Start: {RouteStart}");
                ImGui.Text($"End: {RouteEnd}");

                Update();

                var drawList = ImGui.GetWindowDrawList();

                drawList.PushClipRect(_contentUpperLeft, _contentLowerRight);

                var lowerRight = new Vector2(Math.Max(_contentSize.X, _contentSize.Y), Math.Max(_contentSize.X, _contentSize.Y));
                var transform = _camera.TranslationMatrix;
                var imageMin = Vector2.Transform(Vector2.Zero, transform);
                var imageMax = Vector2.Transform(_camera.MapSize, transform);

                if (!_isFirstRender && _mapTexture is not null) {
                    drawList.AddImage(_mapTexture.TexturePtr, _contentUpperLeft + imageMin, _contentUpperLeft + imageMax);
                }

                if (_isFirstRender || _mapTexture is null) {
                    var text = "Loading map...";
                    var textSize = ImGui.CalcTextSize(text);
                    drawList.AddText((_contentUpperLeft + _contentSize / 2f) - (textSize / 2f), 0xFFFFFFFF, text);

                    if (_mapTexture is not null && _isFirstRender) {
                        _camera.ViewportSize = _contentSize;
                        _camera.MapSize = new Vector2(_mapTexture.Bitmap.Width, _mapTexture.Bitmap.Height);
                        _camera.CenterOn(Coordinates.Me);
                        _isFirstRender = false;
                    }
                }

                //DrawBorderGrids(drawList);

                if (_mapTexture is not null) {
                    if (_path is not null) {
                        DrawPath(drawList);
                    }

                    // character
                    drawList.AddCircleFilled(_camera.CoordsToScreen(Coordinates.Me), 2, 0xFFFF00FF);
                }
                drawList.PopClipRect();

                // map outside border
                drawList.AddRect(_contentUpperLeft, _contentLowerRight, MAP_BORDER_COLOR);

                var cursorStart = ImGui.GetCursorPos();
                // if you dont draw the interactive controls before the invisible button, the
                // invisible button actually blocks inputs
                DrawControls();

                // drawing an invisible button over the entire map lets use get mouse events like click...
                ImGui.SetCursorPos(cursorStart);
                ImGui.InvisibleButton("mapcanvas", _contentSize);

                if (ImGui.BeginPopupContextWindow("map_ctx_menu", ImGuiPopupFlags.MouseButtonRight)) {
                    if (!_didInitContextData) {
                        _contextOpenPosition = _camera.ScreenToCoords(ImGui.GetMousePos());
                        _didInitContextData = true;
                    }

                    ImGui.TextColored(new Vector4(0, 1, 0, 1), $"{string.Join(" ", _contextOpenPosition.ToString().Split(' ').Take(2)).TrimEnd(',')}");
                    ImGui.Separator();

                    if (ImGui.Selectable("Print Location")) {
                        CoreManager.Current.Actions.AddChatText($"{_contextOpenPosition}", 1);
                    }
                    if (ImGui.Selectable("Set route start")) {
                        RouteStart = _contextOpenPosition;
                        CalculateRoute();
                    }
                    if (ImGui.Selectable("Set route end")) {
                        RouteEnd = _contextOpenPosition;
                        CalculateRoute();
                    }

                    ImGui.EndPopup();
                }
                else {
                    _didInitContextData = false;
                }

                _camera.HandleInput();

                // debug...
                if (false && ImGui.IsItemHovered()) {
                    drawList.AddRectFilled(_contentUpperLeft, _contentUpperLeft + new Vector2(_contentSize.X, ImGui.GetTextLineHeightWithSpacing() * 4), 0x99000000);
                    var mapCursorPos = ImGui.GetMousePos();
                    ImGui.SetCursorPos(cursorStart);
                    ImGui.Text($"Mouse: {_camera.ScreenToCoords(mapCursorPos)}");
                    ImGui.Text($"Pos: {_camera.Position}");
                    ImGui.Text($"Zoom: {_camera.Zoom}");
                }
            }
            catch (Exception ex) { UtilityBelt.Service.UBService.LogException(ex); }
        }

        private void DrawPath(ImDrawListPtr drawList) {
            LBNode? prevNode = null;
            for (var i = 0; i < _path.Count; i++) {
                var node = _path[i];
                var reqs = node.Requirements == null ? "" : node.Requirements.ToString();

                var coords = node.Frame.ToCoords();
                if (prevNode != null) {
                    var start = _camera.CoordsToScreen(prevNode.Frame.ToCoords());
                    var end = _camera.CoordsToScreen(coords);

                    var color = 0xFF00FF00;
                    if (prevNode.Type == LocationType.Portal) {
                        color = 0xFFAA00AA;
                    }
                    //else if (prevNode.Type == LocationType.PortalExit)

                    drawList.AddLine(start, end, color, 2f);
                }
                prevNode = node;
            }
        }

        private void DrawControls() {
            if (ImGui.Button("Test Button")) {
                CoreManager.Current.Actions.AddChatText($"Clicked test button... {_camera.ScreenToCoords(ImGui.GetMousePos())}", 1);
            }
        }

        private void DrawBorderGrids(ImDrawListPtr drawList) {
            var gridBgColor = 0x88000000;
            var gridFgColor = 0xFFCCCCCC;
            var gridThickness = 16f;

            // top background
            drawList.AddRectFilled(_contentUpperLeft, _contentUpperLeft + new Vector2(_contentSize.X, gridThickness), gridBgColor);
            // bottom background
            drawList.AddRectFilled(_contentUpperLeft + new Vector2(0, _contentSize.Y - gridThickness), _contentUpperLeft + new Vector2(_contentSize.X, _contentSize.Y), gridBgColor);
            // left background
            drawList.AddRectFilled(_contentUpperLeft + new Vector2(0, gridThickness), _contentUpperLeft + new Vector2(gridThickness, _contentSize.Y - gridThickness), gridBgColor);
            // right background
            drawList.AddRectFilled(_contentUpperLeft + new Vector2(_contentSize.X - gridThickness, gridThickness), _contentUpperLeft + new Vector2(_contentSize.X, _contentSize.Y - gridThickness), gridBgColor);
        }

        public void Dispose() {
            _mapTexture?.Dispose();
        }
    }
}
*/