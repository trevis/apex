﻿using AcClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service;
using UtilityBelt.Scripting.Enums;
using Apex.Lib.Enums;
using WattleScript.Interpreter;
using ImGuiNET;
using Decal.Adapter;

namespace Apex.Lib {
    /// <summary>
    /// Raw scripting API
    /// </summary>
    public static unsafe class ScriptApi {
        internal static bool _wantsWalk = false;
        internal static ForwardMotion? _wantedForwardMotion = null;
        internal static SideStepMotion? _wantedSideStepMotion = null;
        internal static TurnToMotion? _wantedTurnToMotion = null;

        private static int _checkForwardMotion;
        private static int _checkSideStepMotion;
        private static int _checkTurnToMotion;
        private static readonly int MAX_CHECKS = 3;

        /// <summary>
        /// Get the current physics coordinates of the specified object id. This is the client side value, not neccesarily where the server thinks the object is.
        /// </summary>
        /// <param name="objectId">The id of the object to get the physics coordinates of</param>
        /// <returns>The current physics coordinates of the specified object id.</returns>
        public static  Coordinates GetPhysicsCoordinates(uint objectId) {
            try {
                if (UBService.IsInGame && UBService.Scripts.GameState?.WorldState?.WeenieExists(objectId) == true) {
                    var physObj = CPhysicsObj.GetObjectA(objectId);

                    if (physObj is null) {
                        return null;
                    }

                    return new Coordinates(physObj->m_position.objcell_id, physObj->m_position.frame.m_fOrigin.x, physObj->m_position.frame.m_fOrigin.y, physObj->m_position.frame.m_fOrigin.z);
                }
            }
            catch (Exception ex) { PluginCore.Log(ex); }

            return null;
        }

        /// <summary>
        /// Set the character's heading to face the destination coordinates
        /// </summary>
        /// <param name="destination">The coordinates to face</param>
        public static void SetHeadingTo(Coordinates destination) {
            try {
                var heading = Coordinates.Me.HeadingTo(destination);

                var physObj = *CPhysicsObj.player_object;
                if (physObj is not null) {
                    physObj->set_heading(heading, 1);
                }
            }
            catch (Exception ex) { PluginCore.Log(ex); }
        }

        /// <summary>
        /// Set the character's walking state
        /// </summary>
        /// <param name="enable">True to enable, false to disable</param>
        public static void SetWalking(bool enable) {
            if (enable == GetWalking()) return;
            CMotionInterp* cmi = GetCMI();
            if (cmi is null) return;

            _wantsWalk = enable;

            cmi->raw_state.current_holdkey = enable ? HoldKey.HoldKey_None : HoldKey.HoldKey_Run;

            // when switching between walking / running, we need to resubmit all motion commands.
            ApplyWantedMotions(false);
        }

        internal static CMotionInterp* GetCMI() {
            CPhysicsObj* phy = *CPhysicsObj.player_object;
            if (phy is null || phy->movement_manager is null) return null;
            return phy->movement_manager->motion_interpreter;
        }

        internal static ACCmdInterp* GetCMD() {
            var smartbox = *SmartBox.smartbox;
            if (smartbox is null) return null;
            return (ACCmdInterp*)smartbox->cmdinterp;
        }

        internal static CInputManager* GetInputManager() {
            return *(CInputManager**)0x00837FF4;
        }

        /// <summary>
        /// Check if the character is currently walking
        /// </summary>
        /// <returns></returns>
        public static bool GetWalking() {
            CMotionInterp* cmi = GetCMI();
            if (cmi is null) return false;

            return cmi->raw_state.current_holdkey == HoldKey.HoldKey_None;
        }

        /// <summary>
        /// Get the character's turn to motion
        /// </summary>
        /// <returns></returns>
        public static TurnToMotion GetTurnToMotion() {
            CMotionInterp* cmi = GetCMI();
            if (cmi is null) return TurnToMotion.None;

            return (TurnToMotion)cmi->raw_state.turn_command;
        }

        /// <summary>
        /// Get the character's side step motion
        /// </summary>
        /// <returns></returns>
        public static SideStepMotion GetSideStepMotion() {
            CMotionInterp* cmi = GetCMI();
            if (cmi is null) return SideStepMotion.None;

            return (SideStepMotion)cmi->raw_state.sidestep_command;
        }

        /// <summary>
        /// Get the character's forward motion
        /// </summary>
        /// <returns></returns>
        public static ForwardMotion GetForwardMotion() {
            CMotionInterp* cmi = GetCMI();
            if (cmi is null) return ForwardMotion.Ready;

            return (ForwardMotion)cmi->raw_state.forward_command;
        }

        /// <summary>
        /// Set the character's turn to motion
        /// </summary>
        /// <param name="motion">The turn to motion</param>
        public static void SetTurnToMotion(TurnToMotion motion) {
            _wantedTurnToMotion = motion;
            SetMotionRaw((uint)motion);
        }

        /// <summary>
        /// Set the character's side step motion
        /// </summary>
        /// <param name="motion">The side step motion</param>
        public static void SetSideStepMotion(SideStepMotion motion) {
            _wantedSideStepMotion = motion;
            SetMotionRaw((uint)motion);
        }

        /// <summary>
        /// Set the character's forward motion
        /// </summary>
        /// <param name="motion">The forward motion</param>
        public static void SetForwardMotion(ForwardMotion motion) {
            switch (motion) {
                case ForwardMotion.Forward:
                case ForwardMotion.Backwards:
                case ForwardMotion.Ready:
                    _wantedForwardMotion = motion;
                    break;
            }
            SetMotionRaw((uint)motion);
        }

        /// <summary>
        /// Set the character's motion, with no checks
        /// </summary>
        /// <param name="motion"></param>
        public static void SetMotionRaw(uint motion) {
            ACCmdInterp* cmd = GetCMD();
            if (cmd is null) return;

            cmd->SetMotion(motion, true);
        }

        internal static void ApplyWantedMotions(bool doWalk) {
            GetCMD()->a0.enabled = 1;
            if (_checkForwardMotion > 0) {
                if (_wantedForwardMotion.HasValue && GetForwardMotion() == _wantedForwardMotion.Value) {
                    if (_wantedForwardMotion == ForwardMotion.Ready) {
                        _wantedForwardMotion = null;
                    }
                    _checkForwardMotion = 0;
                }
                else if (_checkForwardMotion > MAX_CHECKS) {
                    Reset();
                }
                else {
                    _checkForwardMotion++;
                }
            }
            if (_wantedForwardMotion.HasValue && GetForwardMotion() != _wantedForwardMotion.Value) {
                SetForwardMotion(_wantedForwardMotion.Value);
                SetMotionRaw((uint)_wantedForwardMotion.Value);
                _checkForwardMotion = 1;
                return;
            }

            if (_checkSideStepMotion > 0) {
                if (_wantedSideStepMotion.HasValue && GetSideStepMotion() == _wantedSideStepMotion.Value) {
                    if (_wantedSideStepMotion == SideStepMotion.None) {
                        _wantedSideStepMotion = null;
                    }
                    _checkSideStepMotion = 0;
                }
                else if (_checkSideStepMotion > MAX_CHECKS) {
                    Reset();
                }
                else {
                    _checkSideStepMotion++;
                }
            }
            if (_wantedSideStepMotion.HasValue && GetSideStepMotion() != _wantedSideStepMotion.Value) {
                SetSideStepMotion(_wantedSideStepMotion.Value);
                SetMotionRaw((uint)_wantedSideStepMotion.Value);
                _checkSideStepMotion = 1;
                return;
            }

            if (_checkTurnToMotion > 0) {
                if (_wantedTurnToMotion.HasValue && GetTurnToMotion() == _wantedTurnToMotion.Value) {
                    if (_wantedTurnToMotion == TurnToMotion.None) {
                        _wantedTurnToMotion = null;
                    }
                    _checkTurnToMotion = 0;
                }
                else if (_checkTurnToMotion > MAX_CHECKS) {
                    Reset();
                }
                else {
                    _checkTurnToMotion++;
                }
            }
            else if (_wantedTurnToMotion.HasValue && GetTurnToMotion() != _wantedTurnToMotion.Value) {
                SetTurnToMotion(_wantedTurnToMotion.Value);
                SetMotionRaw((uint)_wantedTurnToMotion.Value);
                _checkTurnToMotion = 1;
                return;
            }
        }

        private static void Reset() {
            var inputManager = (CInputManager_WIN32*)GetInputManager();
            if (inputManager is null) return;
            inputManager->ReleasePressedKeys();
        }

        /// <summary>
        /// Clear all command actions
        /// </summary>
        public static void ClearMotions() {
            if (GetForwardMotion() != ForwardMotion.Ready) SetForwardMotion(ForwardMotion.Ready);
            if (GetSideStepMotion() != SideStepMotion.None) SetSideStepMotion(SideStepMotion.None);
            if (GetTurnToMotion() != TurnToMotion.None) SetTurnToMotion(TurnToMotion.None);
            SetWalking(false);
            Reset();
        }

        /// <summary>
        /// Check if the client is currently in focus
        /// </summary>
        /// <returns></returns>
        public static bool ClientHasFocus() {
            var s_cidm = GetInputManager();
            if (s_cidm is null) return false;

            return s_cidm->m_lastFocus != 0;
        }
        /// <summary>
        /// Get the players current heading
        /// </summary>
        /// <returns></returns>
        internal static float GetHeading() {
            CPhysicsObj* phy = *CPhysicsObj.player_object;
            if (phy is null) return 0;
            return phy->get_heading();
        }

        /// <summary>
        /// Set the players heading
        /// </summary>
        /// <param name="value"></param>
        internal static void SetHeading(float value) {
            CPhysicsObj* phy = *CPhysicsObj.player_object;
            if (phy is null) return;
            phy->set_heading(value, 1);
        }

        /// <summary>
        /// Enable or disable autorun
        /// </summary>
        /// <param name="enable"></param>
        internal static void SetAutoRun(bool enable) {
            CoreManager.Current.Actions.SetAutorun(enable);
        }
    }
}
