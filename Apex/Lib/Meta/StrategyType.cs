﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Lib.Meta {
    public enum StrategyType {
        Vitals = 100,
        Combat = 200,
        Navigation = 300
    }
}
