﻿using AcClient;
using Apex.Lib.Settings;
using Decal.Adapter;
using Decal.Interop.Core;
using ImGuiNET;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service;
using UtilityBelt.Service.Views;
using WattleScript.Interpreter;
using WattleScript.Interpreter.Debugging;
using Vector4 = System.Numerics.Vector4;

namespace Apex.Lib.Meta {
    public class MetaManager : IDisposable {
        private const string SCRIPT_NAME = "ApexStrategies";
        private const string STRATEGIES_DIR = @"Strategies";

        public MetaProfile CurrentProfile { get; set; }
        public bool DidInit { get; private set; } = false;

        public event EventHandler<EventArgs>? OnInit;

        private bool _isRunning = false;
        private UBScript _script;
        private FileSystemWatcher _watcher;
        private List<string> _availableProfiles = new List<string>();
        internal Dictionary<string, Table> AvailableStrategies { get; set; } = new Dictionary<string, Table>();
        public StrategyRunner Runner { get; private set; }

        private int _selectedAddStrategy;
        private string _newProfileName;
        private Task _runnerTask;

        public MetaManager() {

        }

        public void Init() {
            try {
                if (!Directory.Exists(ProfileSettings.ProfilesDirectory)) {
                    Directory.CreateDirectory(ProfileSettings.ProfilesDirectory);
                }

                _watcher = new FileSystemWatcher(ProfileSettings.ProfilesDirectory);
                _watcher.EnableRaisingEvents = true;
                _watcher.Changed += Watcher_Changed;

                UBService.Scripts.StartScript(SCRIPT_NAME, false);
                _script = UBService.Scripts.GetScript(SCRIPT_NAME);
                var strategyDir = Path.Combine(PluginCore.AssemblyDirectory, STRATEGIES_DIR);
                if (Directory.Exists(strategyDir)) {
                    foreach (var file in Directory.EnumerateFiles(strategyDir)) {
                        CoreManager.Current.Actions.AddChatText($"Loading strategies from {file}", 1);
                        _script.WattleScript.DoString(File.ReadAllText(file));
                    }
                }

                PluginCore.Instance.Game.Character.OnDeath += Character_OnDeath;
            }
            catch (OperationCanceledException) {
            }
            catch (ScriptRuntimeException ex2) {
                PluginCore.Log("An error occured! " + ex2.DecoratedMessage);
                foreach (WatchItem item in ex2.CallStack) {
                    PluginCore.Log($"  - {item.Location}");
                }
            }
            catch (SyntaxErrorException ex3) {
                PluginCore.Log("A syntax error occured! " + ex3.DecoratedMessage);
            }
            catch (Exception arg) {
                PluginCore.Log($"Error running script:\n{arg}");
            }

            OnInit?.Invoke(this, EventArgs.Empty);
            DidInit = true;

            BuildProfilesList();
            SetActiveProfile(PluginCore.Instance.Settings.LoadedProfile);
        }

        private void Character_OnDeath(object sender, DeathEventArgs e) {
            try {
                if (PluginCore.Instance.Settings.DisableOnDeath) {
                    PluginCore.Instance.Settings.Enabled = false;
                }
            } catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        private void Watcher_Changed(object sender, FileSystemEventArgs e) {
            BuildProfilesList();
        }

        private void BuildProfilesList() {
            _availableProfiles.Clear();
            _availableProfiles.Add("None");
            _availableProfiles.Add("Default");

            var files = Directory.GetFiles(ProfileSettings.ProfilesDirectory, "*.json");
            foreach (var file in files) {
                _availableProfiles.Add(Path.GetFileNameWithoutExtension(file));
            }
        }

        public void Run() {
            try {
                if (CurrentProfile is null || PluginCore.Instance?.Game?.State != ClientState.In_Game) return;

                if (!PluginCore.Instance.Settings.Enabled) {
                    if (Runner is not null) {
                        Runner.Dispose();
                        Runner = null;
                    }
                    return;
                }
                
                if (Runner is not null && CurrentProfile != Runner?.Profile) {
                    Runner.Dispose();
                    Runner = null;
                }

                if (CurrentProfile is not null && Runner is null) {
                    Runner = new StrategyRunner(this, CurrentProfile);
                    Runner.Start();
                }

                if (_runnerTask is null || _runnerTask.IsCompleted) {
                    _runnerTask = Runner.Run();
                }
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void RenderImGui() {
            var enabled = PluginCore.Instance.Settings.Enabled;
            if (ImGui.Checkbox("Enabled", ref enabled)) {
                PluginCore.Instance.Settings.Enabled = enabled;
                if (!enabled) {
                    ScriptApi.ClearMotions();
                    PluginCore.Instance.Game.ActionQueue.Clear();
                }
            }

            ImGui.SameLine();

            var idx = _availableProfiles.IndexOf(CurrentProfile?.Name ?? "None");
            if (ImGui.Combo("Profile", ref idx, _availableProfiles.ToArray(), _availableProfiles.Count)) {
                try {
                    SetActiveProfile(_availableProfiles[idx]);
                }
                catch (Exception ex) { PluginCore.Log(ex); }
            }

            if (CurrentProfile != null) {
                ImGui.SameLine();
                if (ImGui.Button("Save As")) {
                    _newProfileName = $"{CurrentProfile.Name} - Copy";
                    ImGui.OpenPopup($"Save profile as###popupSaveAs");
                }

                RenderPopup();
            }

            ImGui.Text($"Current Action: {PluginCore.Instance.Game.ActionQueue.Queue.FirstOrDefault()}");

            if (CurrentProfile != null && ImGui.BeginTabBar("profile-stuffs")) {
                var isEditable = CurrentProfile.Name != "Default";

                if (ImGui.BeginTabItem("Status")) {
                    if (Runner is not null) {
                        var strategies = Runner.Strategies.Values.ToArray();
                        foreach (var strategy in strategies) {
                            if (strategy == Runner.CurrentStrategy) ImGui.PushStyleColor(ImGuiCol.Text, 0xFF00FF00);
                            ImGui.SeparatorText(strategy.Name);
                            if (strategy == Runner.CurrentStrategy) ImGui.PopStyleColor();
                            strategy.RenderStatusUI();
                        }
                    }
                    else {
                        ImGui.Text("Not running");
                    }
                    ImGui.EndTabItem();
                }

                if (ImGui.BeginTabItem("Strategies")) {
                    if (!isEditable) {
                        ImGui.TextDisabled("Default profile cannot be edited.\nUse the 'Save As' button to create a new profile.");
                    }
                    else {
                        ImGui.TextDisabled("Move strategies up/down to change priority.");
                    }
                    if (ImGui.BeginTable("strategies-list", 2, ImGuiTableFlags.None)) {
                        ImGui.TableSetupColumn("Name");
                        ImGui.TableSetupColumn("Actions", ImGuiTableColumnFlags.WidthFixed);

                        ImGui.TableHeadersRow();

                        var profileStrategies = CurrentProfile.Settings.Strategies.ToList();
                        for (int n = 0; n < profileStrategies.Count; n++) {
                            ImGui.TableNextRow();

                            ImGui.TableSetColumnIndex(0);
                            if (AvailableStrategies.TryGetValue(profileStrategies[n].Name, out var strategy)) {
                                ImGui.Text($"#{n + 1} {profileStrategies[n].Name}");
                            }
                            else {
                                ImGui.TextColored(new Vector4(1, 0, 0, 1), $"#{n + 1} {profileStrategies[n]}");
                            }

                            ImGui.TableSetColumnIndex(1);
                            if (isEditable) {
                                if (ImGui.Button($"up##upbtn{n}")) {
                                    if (n > 0) {
                                        var oldName = CurrentProfile.Settings.Strategies[n];
                                        CurrentProfile.Settings.Strategies.RemoveAt(n);
                                        CurrentProfile.Settings.Strategies.Insert(n - 1, oldName);
                                    }
                                }

                                ImGui.SameLine();
                                if (ImGui.Button($"down##downbtn{n}")) {
                                    if (n < CurrentProfile.Settings.Strategies.Count - 1) {
                                        var oldName = CurrentProfile.Settings.Strategies[n];
                                        CurrentProfile.Settings.Strategies.RemoveAt(n);
                                        CurrentProfile.Settings.Strategies.Insert(n + 1, oldName);
                                    }
                                }

                                ImGui.SameLine();
                                if (ImGui.Button($"delete##delete{n}")) {
                                    CurrentProfile.Settings.Strategies.RemoveAt(n);
                                }
                            }
                        }

                        ImGui.EndTable();
                    }

                    if (isEditable) {
                        ImGui.Spacing();
                        ImGui.Separator();

                        var strategies2 = AvailableStrategies.Keys.ToArray();
                        if (_selectedAddStrategy > strategies2.Length - 1) _selectedAddStrategy = 0;

                        ImGui.Text($"Available: {string.Join(",", AvailableStrategies.Keys.ToArray())}");

                        ImGui.Combo("##AddStrategy", ref _selectedAddStrategy, strategies2, strategies2.Count());
                        ImGui.SameLine();
                        if (ImGui.Button("Add Strategy")) {
                            try {
                                CurrentProfile.Settings.Strategies.Add(new StrategySettings() {
                                    Name = strategies2[_selectedAddStrategy]
                                });
                            }
                            catch (Exception ex) { PluginCore.Log(ex); }
                        }
                    }

                    ImGui.EndTabItem();
                }

                if (isEditable && ImGui.BeginTabItem("Settings")) {
                    if (ImGui.BeginTabBar("Settings")) {
                        if (ImGui.BeginTabItem("Profile")) {
                            ImGui.TextDisabled("General Profile settings...");

                            var disableOnDeath = PluginCore.Instance.Settings.DisableOnDeath;
                            if (ImGui.Checkbox("Disable on death", ref disableOnDeath)) {
                                PluginCore.Instance.Settings.DisableOnDeath = disableOnDeath;
                            }
                            ImGui.EndTabItem();
                        }
                        foreach (var item in CurrentProfile?.Settings.Strategies.ToList() ?? new List<StrategySettings>()) {
                            if (Runner?.Strategies.TryGetValue(item.Name, out var strategy) == true) {
                                if (ImGui.BeginTabItem(strategy.Name)) {
                                    strategy.RenderSettingsUI();
                                    ImGui.EndTabItem();
                                }
                            }
                        }

                        ImGui.EndTabBar();
                    }
                    ImGui.EndTabItem();
                }

                ImGui.EndTabBar();
            }
        }

        private void RenderPopup() {
            Vector2 center = ImGui.GetMainViewport().GetCenter();
            ImGui.SetNextWindowPos(center, ImGuiCond.Appearing, new Vector2(0.5f, 0.5f));
            ImGui.SetNextWindowSize(new Vector2(300, 110));

            if (ImGui.BeginPopupModal($"Save profile as###popupSaveAs")) {
                ImGui.Spacing();
                ImGui.Spacing();

                ImGui.InputText("New profile name", ref _newProfileName, 100);

                ImGui.Spacing();
                ImGui.Spacing();
                ImGui.Separator();

                if (ImGui.Button("Ok", new Vector2(120, 0))) {
                    ImGui.CloseCurrentPopup();
                    if (string.IsNullOrEmpty(_newProfileName) || _newProfileName.ToLower() == "default" || _newProfileName.ToLower() == "none") {
                        CoreManager.Current.Actions.AddChatText($"Invalid profile name", 15);
                    }
                    else {
                        CoreManager.Current.Actions.AddChatText($"New Name: {_newProfileName}", 15);
                        CurrentProfile = new MetaProfile(_newProfileName) {
                            Settings = CurrentProfile?.Settings.Clone(_newProfileName)
                        };
                        CurrentProfile.Settings.Save();
                        PluginCore.Instance.Settings.LoadedProfile = _newProfileName;
                        _availableProfiles.Add(_newProfileName);
                    }
                }

                ImGui.SameLine();
                if (ImGui.Button("Cancel", new Vector2(120, 0))) {
                    ImGui.CloseCurrentPopup();
                    CoreManager.Current.Actions.AddChatText($"Cancel", 1);
                }
                ImGui.SetItemDefaultFocus();
                ImGui.EndPopup();
            }
        }

        private void SetActiveProfile(string profileName) {
            CurrentProfile?.Dispose();
            CurrentProfile = null;
            
            if (_availableProfiles.Contains(profileName)) {
                if (profileName == "None") {
                    CurrentProfile = null;
                }
                else {
                    CurrentProfile = new MetaProfile(profileName);
                }

                PluginCore.Instance.Settings.LoadedProfile = CurrentProfile?.Name;
            }
        }

        public void RegisterStrategy(string name, Table strategy) {
            AvailableStrategies.Remove(name);
            AvailableStrategies.Add(name, strategy);

            if (Runner?.Strategies.TryGetValue(name, out var runningStrategy) == true) {
                Runner.Strategies[name] = new LuaStrategy(name, strategy, runningStrategy.GetSettings());
            }

            CoreManager.Current.Actions.AddChatText($"Registering strategy: {name}", 1);
        }

        public void UnregisterStrategy(string name) {
            AvailableStrategies.Remove(name);
            CoreManager.Current.Actions.AddChatText($"Unregistering strategy: {name}", 1);
        }

        public void Dispose() {
            PluginCore.Instance.Game.Character.OnDeath -= Character_OnDeath;
            _watcher?.Dispose();
        }
    }
}
