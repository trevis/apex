﻿using Decal.Adapter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace Apex.Lib.Meta {
    public class StrategyRunner : IDisposable {
        private MetaManager metaManager;
        private object _lockObj = new();
        private DynValue _res2;

        public MetaProfile Profile { get; }
        public IMetaStrategy CurrentStrategy { get; set; }
        public CancellationTokenSource TokenSource { get; private set; }
        public Dictionary<string, IMetaStrategy> Strategies { get; } = new Dictionary<string, IMetaStrategy>();
        public ConcurrentDictionary<string, Coroutine> DoWorkCoroutines { get; } = new ConcurrentDictionary<string, Coroutine>();
        public ConcurrentDictionary<string, DynValue> DoWorkValues { get; } = new ConcurrentDictionary<string, DynValue>();
        public bool IsRunning => TokenSource != null && !TokenSource.IsCancellationRequested;

        public StrategyRunner(MetaManager metaManager, MetaProfile profile) {
            this.metaManager = metaManager;
            Profile = profile;
        }

        public void Start() {
            if (IsRunning) Stop();
            TokenSource = new CancellationTokenSource();
        }

        public async Task Run() {
            var strategies = Profile.Settings.Strategies.ToArray();
            foreach (var strategySettings in strategies) {
                if (!Strategies.TryGetValue(strategySettings.Name, out var strategy)) {
                    if (metaManager.AvailableStrategies.TryGetValue(strategySettings.Name, out var strategyTable)) {
                        strategy = new LuaStrategy(strategySettings.Name, strategyTable, strategySettings.Settings);
                        Strategies.Add(strategySettings.Name, strategy);
                    }
                }
            }

            if (!PluginCore.Instance.Settings.Enabled) return;

            foreach (var strategySettings in strategies) {
                if (Strategies.TryGetValue(strategySettings.Name, out var strategy)) {
                    try {
                        if (await strategy?.NeedsWork() == true) {
                            if (CurrentStrategy is not null && CurrentStrategy != strategy) {
                                if (DoWorkValues.TryRemove(CurrentStrategy.Name, out var workVal) && workVal.Type == DataType.UserData) {
                                    if (workVal.UserData?.Object is QueueAction action) {
                                        //CoreManager.Current.Actions.AddChatText($"Cancelling {CurrentStrategy?.Name} / {action} because {strategy.Name} wants work done ({action.Error})", 1);
                                        action.SetPermanentResult(UtilityBelt.Scripting.Enums.ActionError.Cancelled);
                                    }
                                }
                                DoWorkCoroutines.TryRemove(CurrentStrategy.Name, out _);
                            }
                            CurrentStrategy = strategy;
                            StartOrContinueTask(CurrentStrategy);
                            return;
                        }
                        else {
                            DoWorkCoroutines.TryRemove(strategy.Name, out _);
                            DoWorkValues.TryRemove(strategy.Name, out _);
                        }
                    }
                    catch (ScriptRuntimeException ex2) {
                        PluginCore.Log($"An error occured running {strategy.Name}:NeedsWork: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}");
                    }
                    catch (SyntaxErrorException ex3) {
                        PluginCore.Log($"A syntax error occured running {strategy.Name}:NeedsWork: {ex3.DecoratedMessage}");
                    }
                    catch (Exception arg) {
                        PluginCore.Log($"Error running {strategy.Name}:NeedsWork: {arg}");
                    }
                }
            }
        }

        private void StartOrContinueTask(IMetaStrategy currentStrategy) {
            lock (_lockObj) {
                if (currentStrategy == null) {
                    return;
                }

                if (DoWorkCoroutines.TryGetValue(currentStrategy.Name, out var co)) {
                    switch (co.State) {
                        case CoroutineState.NotStarted:
                        case CoroutineState.Suspended:
                        case CoroutineState.ForceSuspended:
                            CurrentStrategy = currentStrategy;
                            RunCoroutine(currentStrategy, co);
                            return;
                        case CoroutineState.Dead:
                            DoWorkCoroutines.TryRemove(currentStrategy.Name, out _);
                            DoWorkValues.TryRemove(currentStrategy.Name, out _);
                            CurrentStrategy = null;
                            break;
                        default:
                            CoreManager.Current.Actions.AddChatText($"{CurrentStrategy.Name}:DoWork in unknown state: {co.State}", 15);
                            break;
                    }
                }

                var doWorkCo = currentStrategy.DoWork();
                if (!doWorkCo.IsNil()) {
                    DoWorkCoroutines.TryAdd(currentStrategy.Name, doWorkCo.Coroutine);
                }
            }
        }

        private void RunCoroutine(IMetaStrategy strategy, Coroutine co) {
            try {
                if (DoWorkValues.TryGetValue(strategy.Name, out var coValue)) {
                    if (coValue.Type == DataType.UserData && coValue.UserData.Object is QueueAction action) {
                        if (action.IsFinished) {
                            DoWorkValues[strategy.Name] = co.Resume(action);
                        }
                    }
                    else {
                        DoWorkValues[strategy.Name] = co.Resume(coValue);
                    }
                }
                else {
                    DoWorkValues.TryAdd(strategy.Name, co.Resume((strategy as LuaStrategy).Strategy));
                }
            }
            catch (ScriptRuntimeException ex2) {
                PluginCore.Log($"An error occured running {strategy.Name}:DoWork: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}");
            }
            catch (SyntaxErrorException ex3) {
                PluginCore.Log($"A syntax error occured running {strategy.Name}:DoWork: {ex3.DecoratedMessage}");
            }
            catch (Exception arg) {
                PluginCore.Log($"Error running {strategy.Name}:DoWork: {arg}");
            }
        }

        public void Stop() {
            PluginCore.Instance.Game.ActionQueue.Clear();
            TokenSource?.Cancel();
            TokenSource = null;
        }

        internal void SaveStrategies() {
            foreach (var strategySettings in Profile.Settings.Strategies) {
                if (Strategies.TryGetValue(strategySettings.Name, out var strategy)) {
                    try {
                        strategySettings.Settings = strategy.GetSettings();
                    }
                    catch (Exception ex) { PluginCore.Log(ex); }
                }
            }
        }

        public void Dispose() {
            TokenSource.Cancel();
        }
    }
}
