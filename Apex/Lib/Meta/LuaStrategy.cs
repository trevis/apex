﻿using Decal.Adapter;
using ImGuiNET;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;
using WattleScript.Interpreter.Debugging;
using WattleScript.Interpreter.Serialization.Json;
using static AcClient._STL;

namespace Apex.Lib.Meta {
    public class LuaStrategy : IMetaStrategy {
        private JsonSerializerSettings serializerSettings => Apex.Lib.Settings.Settings.SerializerSettings;

        public string Name { get; }
        public Table Strategy { get; }
        public Dictionary<string, object> Settings { get; }

        public LuaStrategy(string name, Table strategy, Dictionary<string, object> settings = null) {
            Name = name;
            Strategy = strategy;
            Settings = settings;

            DynValue newRes;
            if (Settings is null) {
                newRes = RunLuaMethod("new");
            }
            else {
                newRes = RunLuaMethod("new", Settings);
            }

            if (newRes.Type == DataType.Table) {
                Strategy = newRes.Table; 
            }
        }

        public Dictionary<string, object> GetSettings() {
            return TableToDictionary(Strategy) as Dictionary<string, object>;
        }

        public DynValue DoWork() {
            return MakeCoroutine("DoWork");
        }

        public async Task<bool> NeedsWork() {
            var res = await RunLuaMethodAsync("NeedsWork");
            if (res.Type == DataType.Boolean) {
                return res.Boolean;
            }
            return false;
        }

        public void RenderStatusUI() { 
            try {
                RunLuaMethod("RenderStatus");
            }
            catch(Exception ex) { ImGui.Text(ex.Message); }
        }

        private IDictionary TableToDictionary(Table table) {
            IDictionary res;

            bool isStringKey = true;

            if (table.Pairs.Count() > 0 && table.Pairs.First().Key.Type == DataType.UserData) {
                Type typeDictOf = typeof(Dictionary<,>);
                Type[] paramTypes = { table.Pairs.First().Key.UserData.Object.GetType(), typeof(object) };
                Type typeDictOfUserDataKey = typeDictOf.MakeGenericType(paramTypes);
                res = Activator.CreateInstance(typeDictOfUserDataKey) as IDictionary;
                isStringKey = false;
            }
            else {
                res = new Dictionary<string, object>();
            }

            foreach (var pair in table.Pairs) {
                // ignore fields starting with '_', they are "private"
                if (pair.Key.Type == DataType.String && pair.Key.String.StartsWith("_")) continue;
                if (pair.Key.Type != DataType.String && isStringKey) continue;

                switch (pair.Value.Type) {
                    case DataType.Boolean:
                    case DataType.Number:
                    case DataType.String:
                    case DataType.UserData:
                        res.Add(ValueToDynamic(pair.Key), ValueToDynamic(pair.Value));
                        break;
                    case DataType.Table:
                        res.Add(ValueToDynamic(pair.Key), TableToDictionary(pair.Value.Table));
                        break;
                    default:
                        PluginCore.Log($"Unable to serialize strategy {Name} settings of type: {pair.Key} ({pair.Value})");
                        continue;
                }
            }

            return res;
        }

        private dynamic ValueToDynamic(DynValue value) {
            switch (value.Type) {
                case DataType.Boolean:
                    return value.Boolean;
                case DataType.Number:
                    return value.Number;
                case DataType.String:
                    return value.String;
                case DataType.UserData:
                    return value.UserData.Object;
                default:
                    PluginCore.Log($"Unable to serialize strategy {Name} settings of type: {value.Type} ({value})");
                    return null;
            }
        }

        public void RenderSettingsUI() {
            RunLuaMethod("RenderSettings");
        }

        public async Task Cancel() {
            await RunLuaMethodAsync("Cancel");
        }

        private DynValue MakeCoroutine(string methodName, params object[] args) {
            DynValue co = DynValue.Nil;
            try {
                var luaFunc = Strategy?.Get(methodName) ?? DynValue.Nil;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    co = luaFunc.Function.OwnerScript.CreateCoroutine(luaFunc);
                }

                luaFunc = Strategy?.MetaTable?.Get(methodName) ?? luaFunc;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    co = luaFunc.Function.OwnerScript.CreateCoroutine(luaFunc);
                }
                return co;
            }
            catch (ScriptRuntimeException ex2) {
                PluginCore.Log($"An error occured running {Name}:{methodName}: {(string.IsNullOrEmpty(ex2.DecoratedMessage) ? ex2.ToString() : ex2.DecoratedMessage)}");
            }
            catch (SyntaxErrorException ex3) {
                PluginCore.Log($"A syntax error occured running {Name}:{methodName}(): {ex3.DecoratedMessage}");
            }
            catch (Exception arg) {
                PluginCore.Log($"Error running {Name}:{methodName}(): {arg}");
            }

            return co;
        }

        private Task<DynValue> RunLuaMethodAsync(string methodName, params object[] args) {
            try {
                var luaFunc = Strategy?.Get(methodName) ?? DynValue.Nil;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    if (args is not null) {
                        return luaFunc.Function.CallAsync(args);
                    }
                    else {
                        return luaFunc.Function.CallAsync();
                    }
                }

                luaFunc = Strategy?.MetaTable?.Get(methodName) ?? luaFunc;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    return luaFunc.Function.CallAsync(Strategy);
                }
            }
            catch (ScriptRuntimeException ex2) {
                PluginCore.Log($"An error occured running {Name}:{methodName}(): {ex2.DecoratedMessage}");
            }
            catch (SyntaxErrorException ex3) {
                PluginCore.Log($"A syntax error occured running {Name}:{methodName}(): {ex3.DecoratedMessage}");
            }
            catch (Exception arg) {
                PluginCore.Log($"Error running {Name}:{methodName}(): {arg.Message}");
            }

            return Task.FromResult(DynValue.Nil);
        }

        private DynValue RunLuaMethod(string methodName, params object[] args) {
            try {
                var luaFunc = Strategy?.Get(methodName) ?? DynValue.Nil;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    if (args is not null) {
                        return luaFunc.Function.Call(args);
                    }
                    else {
                        return luaFunc.Function.Call();
                    }
                }

                luaFunc = Strategy?.MetaTable?.Get(methodName) ?? luaFunc;
                if (luaFunc.Type == DataType.Function && luaFunc.Function is not null) {
                    return luaFunc.Function.Call(Strategy);
                } 
            }
            catch (ScriptRuntimeException ex2) {
                PluginCore.Log($"An error occured running {Name}:{methodName}(): {ex2.DecoratedMessage}");
            }
            catch (SyntaxErrorException ex3) {
                PluginCore.Log($"A syntax error occured running {Name}:{methodName}(): {ex3.DecoratedMessage}");
            }
            catch (Exception arg) {
                PluginCore.Log($"Error running {Name}:{methodName}(): {arg}");
            }

            return DynValue.Nil;
        }

        public void Dispose() {
            Cancel();
        }
    }
}
