﻿using Apex.Lib.Settings;
using System;

namespace Apex.Lib.Meta {
    public class MetaProfile : IDisposable {

        public string Name => Settings.Name;

        public ProfileSettings Settings { get; set; }
        public MetaProfile(string name) {
            var settings = new ProfileSettings(name);
            Settings = settings;
        }

        public void Dispose() {
            
        }
    }
}