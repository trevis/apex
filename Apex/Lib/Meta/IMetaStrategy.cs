﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace Apex.Lib.Meta {
    public interface IMetaStrategy : IDisposable {
        public string Name { get; }
        public Task<bool> NeedsWork();
        public DynValue DoWork();
        public void RenderStatusUI();
        public Task Cancel();
        public void RenderSettingsUI();
        public Dictionary<string, object> GetSettings();
    }
}