﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace Apex.Lib.VTNav.Waypoints {
    public class VTNPoint {
        public double NS = 0.0;
        public double EW = 0.0;
        public double Z = 0.0;
        public List<D3DObj> shapes = new List<D3DObj>();

        public VTNPoint Previous = null;

        internal StreamReader sr;
        internal int index = 0;
        internal VTNavRoute route;
        internal bool disposed = false;

        internal eWaypointType Type = eWaypointType.Point;

        public VTNPoint(StreamReader reader, VTNavRoute parentRoute, int index) {
            sr = reader;
            route = parentRoute;
            this.index = index;
        }

        public VTNPoint GetPreviousPoint() {
            if (index == 0 && route.NavType == eNavType.Once && route.NavOffset > 0) {
                return route.points[route.NavOffset - 1];
            }

            for (var i = index - 1; i >= 0; i--) {
                var t = route.points[i].Type;
                if (t == eWaypointType.Point) {
                    return route.points[i];
                }
                else if (t == eWaypointType.OpenVendor) {
                    var ov = (VTNOpenVendor)route.points[i];
                    if (ov.closestDistance < Double.MaxValue) {
                        return route.points[i];
                    }
                }
                else if (t == eWaypointType.UseNPC) {
                    var ov = (VTNUseNPC)route.points[i];
                    if (ov.closestDistance < Double.MaxValue) {
                        return route.points[i];
                    }
                }
                else if (t == eWaypointType.Portal || t == eWaypointType.Portal2) {
                    break;
                }
            }

            return null;
        }

        public VTNPoint GetNextPoint() {
            for (var i = index + 1; i < route.points.Count - 1; i++) {
                if (route.points[i].Type == eWaypointType.Point) return route.points[i];
            }

            return null;
        }

        public bool Parse() {
            if (!double.TryParse(sr.ReadLine(), out EW)) return false;
            if (!double.TryParse(sr.ReadLine(), out NS)) return false;
            if (!double.TryParse(sr.ReadLine(), out Z)) return false;

            // i dont know what this value is, always 0?
            sr.ReadLine();

            return true;
        }

        internal virtual void Write(StreamWriter file) {
            file.WriteLine((int)Type);
            file.WriteLine(EW);
            file.WriteLine(NS);
            file.WriteLine(Z);
            file.WriteLine(0);
        }

        public double DistanceTo(VTNPoint rp) {
            return Math.Abs(Math.Sqrt(Math.Pow(rp.NS - NS, 2) + Math.Pow(rp.EW - EW, 2) + Math.Pow(rp.Z - Z, 2))) * 240;
        }
    }
}
