using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNJump : VTNPoint {
        public int Heading = 0;
        public double Milliseconds = 0;
        public bool ShiftJump = false;

        public VTNJump(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.Jump;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            var facingLine = base.sr.ReadLine();
            if (!int.TryParse(facingLine, out Heading)) {
                PluginCore.Log("Could not parse Facing: " + facingLine);
                return false;
            }

            var shiftLine = base.sr.ReadLine();
            if (shiftLine.Trim().ToLower() == "true") {
                ShiftJump = true;
            }

            var millisecondsLine = base.sr.ReadLine();
            if (!double.TryParse(millisecondsLine, out Milliseconds)) {
                PluginCore.Log("Could not parse Milliseconds: " + millisecondsLine);
                return false;
            }

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Heading);
            file.WriteLine(ShiftJump);
            file.WriteLine(Milliseconds);
        }
    }
}
