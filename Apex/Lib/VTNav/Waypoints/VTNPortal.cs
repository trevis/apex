﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNPortal : VTNPoint {
        public string Name = "Portal";
        public int Id = 0;
        public ObjectClass ObjectClass = ObjectClass.Unknown;

        public double PortalNS = 0;
        public double PortalEW = 0;
        public double PortalZ = 0;

        internal double closestDistance = double.MaxValue;

        public VTNPortal(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.Portal;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;
            Name = base.sr.ReadLine();

            var objectClassLine = base.sr.ReadLine(); // 14 ObjectClass?
            var objectClassInt = 0;
            if (!int.TryParse(objectClassLine, out objectClassInt)) {
                PluginCore.Log("Could not parse ObjectClass");
                return false;
            }
            ObjectClass = (ObjectClass)objectClassInt;

            base.sr.ReadLine(); // true ?

            // these are portal exit coordinates
            if (!double.TryParse(sr.ReadLine(), out PortalNS)) {
                PluginCore.Log("Could not parse PortalNS");
                return false;
            }

            if (!double.TryParse(sr.ReadLine(), out PortalEW)) {
                PluginCore.Log("Could not parse PortalEW");
                return false;
            }

            if (!double.TryParse(sr.ReadLine(), out PortalZ)) {
                PluginCore.Log("Could not parse PortalZ");
                return false;
            }

            NS = PortalNS;
            EW = PortalEW;
            Z = PortalZ;

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Name);
            file.WriteLine((int)ObjectClass);
            file.WriteLine(true); //??
            file.WriteLine(NS);
            file.WriteLine(EW);
            file.WriteLine(Z);
        }
    }
}
