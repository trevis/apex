﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNUseNPC : VTNPoint {
        public string Name = "Vendor";
        public int Id = 0;
        public ObjectClass ObjectClass = ObjectClass.Npc;

        public double NpcNS = 0;
        public double NpcEW = 0;
        public double NpcZ = 0;

        internal double closestDistance = double.MaxValue;

        public VTNUseNPC(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.UseNPC;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            Name = base.sr.ReadLine();
            var objectClass = 0;
            if (!int.TryParse(sr.ReadLine(), out objectClass)) {
                PluginCore.Log($"Could not parse objectClass");
                return false;
            }
            ObjectClass = (ObjectClass)objectClass;

            base.sr.ReadLine(); // true?

            if (!double.TryParse(sr.ReadLine(), out NpcEW)) {
                PluginCore.Log($"Could not parse NpcEW");
                return false;
            }
            if (!double.TryParse(sr.ReadLine(), out NpcNS)) {
                PluginCore.Log($"Could not parse NpcNS");
                return false;
            }
            if (!double.TryParse(sr.ReadLine(), out NpcZ)) {
                PluginCore.Log($"Could not parse NpcZ");
                return false;
            }

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Name);
            file.WriteLine((int)ObjectClass);
            file.WriteLine(NpcEW);
            file.WriteLine(NpcNS);
            file.WriteLine(NpcZ);
        }
    }
}
