﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNPause : VTNPoint {
        public int Pause = 0;

        public VTNPause(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.Pause;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            var pauseText = base.sr.ReadLine();

            if (!int.TryParse(pauseText, out Pause)) {
                PluginCore.Log("Could not parse pause: " + pauseText);
                return false;
            }

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Pause);
        }
    }
}
