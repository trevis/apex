﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNRecall : VTNPoint {
        public int RecallSpellId = 0;

        public VTNRecall(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.Recall;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            var recallId = base.sr.ReadLine();

            if (!int.TryParse(recallId, out RecallSpellId)) {
                PluginCore.Log("Could not parse recall spell id: " + recallId);
                return false;
            }

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(RecallSpellId);
        }
    }
}
