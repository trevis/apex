﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNOpenVendor : VTNPoint {
        public string Name = "Vendor";
        public int Id = 0;

        internal double closestDistance = double.MaxValue;

        public VTNOpenVendor(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.OpenVendor;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            var idLine = base.sr.ReadLine();
            if (!int.TryParse(idLine, out Id)) {
                PluginCore.Log("Could not parse VendorID");
                return false;
            }

            Name = base.sr.ReadLine();

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Id);
            file.WriteLine(Name);
        }
    }
}
