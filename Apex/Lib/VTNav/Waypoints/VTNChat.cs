﻿using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace Apex.Lib.VTNav.Waypoints {
    class VTNChat : VTNPoint {
        public string Message = "";

        public VTNChat(StreamReader reader, VTNavRoute parentRoute, int index) : base(reader, parentRoute, index) {
            Type = eWaypointType.ChatCommand;
        }

        new public bool Parse() {
            if (!base.Parse()) return false;

            Message = base.sr.ReadLine();

            return true;
        }

        internal override void Write(StreamWriter file) {
            base.Write(file);
            file.WriteLine(Message);
        }
    }
}
