﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using Apex.Lib.VTNav.Waypoints;

namespace Apex.Lib.VTNav {

    public class VTNavRoute : IDisposable {
        private bool disposed = false;

        public string NavPath;
        public string Header = "uTank2 NAV 1.2";
        public int RecordCount = 0;
        public eNavType NavType = eNavType.Circular;

        public int TargetId = 0;
        public string TargetName = "";

        public static string NoneNavName = " [None]";

        public List<VTNPoint> points = new List<VTNPoint>();
        public int NavOffset = 0;
        public Dictionary<string, double> offsets = new Dictionary<string, double>();

        public VTNavRoute(string navPath) {
            NavPath = navPath;
        }

        public void AddOffset(double ns, double ew, double offset) {
            var key = $"{ns},{ew}";

            if (offsets.ContainsKey(key)) {
                offsets[key] += offset;
            }
            else {
                offsets.Add(key, GetZOffset(ns, ew) + offset);
            }
        }

        public double GetZOffset(double ns, double ew) {
            var key = $"{ns},{ew}";

            if (offsets.ContainsKey(key)) {
                return offsets[key];
            }
            else {
                return 0.25;
            }
        }

        public bool Parse() {
            try {
                using (StreamReader sr = File.OpenText(NavPath)) {
                    Header = sr.ReadLine();

                    var navTypeLine = sr.ReadLine();
                    int navType = 0;
                    if (!int.TryParse(navTypeLine, out navType)) {
                        PluginCore.Log("Could not parse navType from nav file: " + navTypeLine);
                        return false;
                    }
                    NavType = (eNavType)navType;

                    if (NavType == eNavType.Target) {
                        if (sr.EndOfStream) {
                            PluginCore.Log("Follow nav is empty");
                            return true;
                        }

                        TargetName = sr.ReadLine();
                        var targetId = sr.ReadLine();
                        if (!int.TryParse(targetId, out TargetId)) {
                            PluginCore.Log("Could not parse target id: " + targetId);
                            return false;
                        }

                        return true;
                    }

                    var recordCount = sr.ReadLine();
                    if (!int.TryParse(recordCount, out RecordCount)) {
                        PluginCore.Log("Could not read record count from nav file: " + recordCount);
                        return false;
                    }

                    int x = 0;
                    VTNPoint previous = null;
                    while (!sr.EndOfStream && points.Count < RecordCount) {
                        int recordType = 0;
                        var recordTypeLine = sr.ReadLine();

                        if (!int.TryParse(recordTypeLine, out recordType)) {
                            PluginCore.Log($"Unable to parse recordType: {recordTypeLine}");
                            return false;
                        }

                        VTNPoint point = null;

                        switch ((eWaypointType)recordType) {
                            case eWaypointType.ChatCommand:
                                point = new VTNChat(sr, this, x);
                                ((VTNChat)point).Parse();
                                break;

                            case eWaypointType.Checkpoint:
                                point = new VTNPoint(sr, this, x);
                                ((VTNPoint)point).Parse();
                                break;

                            case eWaypointType.Jump:
                                point = new VTNJump(sr, this, x);
                                ((VTNJump)point).Parse();
                                break;

                            case eWaypointType.OpenVendor:
                                point = new VTNOpenVendor(sr, this, x);
                                ((VTNOpenVendor)point).Parse();
                                break;

                            case eWaypointType.Other: // no clue here...
                                throw new System.Exception("eWaypointType.Other");

                            case eWaypointType.Pause:
                                point = new VTNPause(sr, this, x);
                                ((VTNPause)point).Parse();
                                break;

                            case eWaypointType.Point:
                                point = new VTNPoint(sr, this, x);
                                ((VTNPoint)point).Parse();
                                break;

                            case eWaypointType.Portal:
                                point = new VTNPortal(sr, this, x);
                                ((VTNPortal)point).Parse();
                                break;

                            case eWaypointType.Portal2:
                                point = new VTNPortal(sr, this, x);
                                ((VTNPortal)point).Parse();
                                break;

                            case eWaypointType.Recall:
                                point = new VTNRecall(sr, this, x);
                                ((VTNRecall)point).Parse();
                                break;

                            case eWaypointType.UseNPC:
                                point = new VTNUseNPC(sr, this, x);
                                ((VTNUseNPC)point).Parse();
                                break;
                        }

                        if (point != null) {
                            point.Previous = previous;
                            points.Add(point);
                            previous = point;
                            x++;
                        }

                    }

                    return true;
                }
            }
            catch (Exception ex) { PluginCore.Log(ex); }

            return false;
        }

        internal void Write(StreamWriter file) {
            file.WriteLine(Header);
            file.WriteLine((int)NavType);
            file.WriteLine(RecordCount);

            foreach (var point in points) {
                point.Write(file);
            }
        }

        public void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing) {
            if (!disposed) {
                disposed = true;
            }
        }
    }
}