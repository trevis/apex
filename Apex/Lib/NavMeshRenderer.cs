﻿using DotRecast.Detour;
using ImGuiNET;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using Decal.Adapter;
using Apex.Lib.Autonav;
using UtilityBelt.Service.Lib.ACClientModule;
using System.Diagnostics.Metrics;
using Microsoft.DirectX.Direct3D;
using Apex.Lib.Geometry;
using Decal.Adapter.Wrappers;
using BoundingBox = Apex.Lib.Geometry.BoundingBox;
using System.Drawing;
using DotRecast.Detour.TileCache;

namespace Apex.Lib {
    public class NavMeshRenderer : IDisposable {
        private DtNavMesh _mesh;

        //private DtTileCache _tileCache;
        private VertexBuffer _vertexBuffer;

        public BoundingBox BoundingBox { get; private set; } = new BoundingBox();
        public CustomVertex.PositionColored[] Vertices { get; private set; } = [];

        public NavMeshRenderer(DtNavMesh mesh) {
            _mesh = mesh;

            BuildMesh();
        }

        private void BuildMesh() {
            int vertexOffset = 1;
            var verts = new List<Vector3>();
            for (int i = 0; i < _mesh.GetTileCount(); i++) {
                DtMeshTile tile = _mesh.GetTile(i);
                if (tile != null) {
                    for (int p = 0; p < tile.data.header.polyCount; p++) {
                        DtPoly poly = tile.data.polys[p];
                        if (poly.GetPolyType() == DtPolyTypes.DT_POLYTYPE_OFFMESH_CONNECTION) {
                            continue;
                        }
                        verts.AddRange(GetPolyVerts(tile, p));
                    }

                    vertexOffset += tile.data.header.vertCount;
                }
            }

            Vertices = verts.Select(v => {
                unchecked {
                    return new CustomVertex.PositionColored(new Microsoft.DirectX.Vector3(v.X, v.Z, v.Y), Color.FromArgb(155, 255, 00, 255).ToArgb());
                }
            }).ToArray();
            //_tileCache.Update();
            BoundingBox = new BoundingBox(verts);
            MakeBuffers();
        }

        private void MakeBuffers() {
            DestroyBuffers();

            _vertexBuffer = new VertexBuffer(Vertices[0].GetType(), Vertices.Length, PluginCore.D3Ddevice, Usage.WriteOnly, CustomVertex.PositionColored.Format, Pool.Managed);
            _vertexBuffer.SetData(Vertices, 0, LockFlags.None);
        }

        private void DestroyBuffers() {
            _vertexBuffer?.Dispose();
            _vertexBuffer = null;
        }

        internal void Render() {
            DestroyBuffers();
            BuildMesh();
            if (_vertexBuffer is not null) {
                using (var stateBlock = new StateBlock(PluginCore.D3Ddevice, StateBlockType.All)) {
                    stateBlock.Capture();
                    PluginCore.ResetDirectXState();
                    PluginCore.D3Ddevice.Transform.World = Microsoft.DirectX.Matrix.Identity;
                    PluginCore.D3Ddevice.Transform.View = Camera.GetD3DViewTransform();

                    PluginCore.D3Ddevice.RenderState.CullMode = Cull.CounterClockwise;

                    PluginCore.D3Ddevice.VertexFormat = CustomVertex.PositionColored.Format;
                    PluginCore.D3Ddevice.SetStreamSource(0, _vertexBuffer, 0);
                    PluginCore.D3Ddevice.RenderState.FillMode = FillMode.WireFrame;
                    PluginCore.D3Ddevice.DrawPrimitives(PrimitiveType.TriangleList, 0, Vertices.Length / 3);
                    PluginCore.D3Ddevice.RenderState.FillMode = FillMode.Solid;
                    PluginCore.D3Ddevice.DrawPrimitives(PrimitiveType.TriangleList, 0, Vertices.Length / 3);

                    stateBlock.Apply();
                }
            }
        }

        private List<Vector3> GetPolyVerts(DtMeshTile tile, int index) {
            DtPoly p = tile.data.polys[index];
            var vertices = new List<Vector3>();

            if (tile.data.detailMeshes != null) {
                ref DtPolyDetail pd = ref tile.data.detailMeshes[index];
                for (int j = 0; j < pd.triCount; ++j) {
                    int t = (pd.triBase + j) * 4;
                    for (int k = 0; k < 3; ++k) {
                        int v = tile.data.detailTris[t + k];
                        if (v < p.vertCount) {
                            vertices.Add(new Vector3(tile.data.verts[p.verts[v] * 3], tile.data.verts[p.verts[v] * 3 + 2],
                                tile.data.verts[p.verts[v] * 3 + 1]));
                        }
                        else {
                            vertices.Add(new Vector3(tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3],
                                tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3 + 2],
                                tile.data.detailVerts[(pd.vertBase + v - p.vertCount) * 3 + 1]));
                        }
                    }
                }
            }
            else {
                for (int j = 1; j < p.vertCount - 1; ++j) {
                    vertices.Add(new Vector3(tile.data.verts[p.verts[0] * 3], tile.data.verts[p.verts[0] * 3 + 2],
                        tile.data.verts[p.verts[0] * 3 + 1]));
                    for (int k = 0; k < 2; ++k) {
                        vertices.Add(new Vector3(tile.data.verts[p.verts[j + k] * 3], tile.data.verts[p.verts[j + k] * 3 + 2],
                            tile.data.verts[p.verts[j + k] * 3 + 1]));
                    }
                }
            }

            return vertices;
        }

        public void Dispose() {
            DestroyBuffers();
        }
    }
}
