﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Lib.Enums {
    /// <summary>
    /// Turn motions
    /// </summary>
    public enum TurnToMotion : uint {
        /// <summary>
        /// None
        /// </summary>
        None = 0x0,

        /// <summary>
        /// Turn right
        /// </summary>
        TurnRight = 0x6500000d,

        /// <summary>
        /// Turn left
        /// </summary>
        TurnLeft = 0x6500000e,

    }

    /// <summary>
    /// Side step motions
    /// </summary>
    public enum SideStepMotion : uint {
        /// <summary>
        /// None
        /// </summary>
        None = 0x0,

        /// <summary>
        /// Side step right
        /// </summary>
        SideStepRight = 0x6500000f,

        /// <summary>
        /// Side step left
        /// </summary>
        SideStepLeft = 0x65000010,
    }

    /// <summary>
    /// Forward motions
    /// </summary>
    public enum ForwardMotion : uint {
        /// <summary>
        /// Ready state (idle)
        /// </summary>
        Ready = 0x41000003,

        /// <summary>
        /// Move forward
        /// </summary>
        Forward = 0x45000005,

        /// <summary>
        /// Move backwards
        /// </summary>
        Backwards = 0x45000006,

        Crouch = 0x41000012,
        Sitting = 0x41000013,
        Sleeping = 0x41000014,

        Cheer = 0x1300004c,

        ShakeFist = 0x13000079,
        Beckon = 0x1300007a,
        BeSeeingYou = 0x1300007b,
        BlowKiss = 0x1300007c,
        BowDeep = 0x1300007d,
        ClapHands = 0x1300007e,
        Cry = 0x1300007f,
        Laugh = 0x13000080,
        MimeEat = 0x13000081,
        MimeDrink = 0x13000082,
        Nod = 0x13000083,
        Point = 0x13000084,
        ShakeHead = 0x13000085,
        Shrug = 0x13000086,
        Wave = 0x13000087,
        Akimbo = 0x13000088,
        HeartyLaugh = 0x13000089,
        Salute = 0x1300008a,
        ScratchHead = 0x1300008b,
        SmackHead = 0x1300008c,
        TapFoot = 0x1300008d,
        WaveHigh = 0x1300008e,
        WaveLow = 0x1300008f,
        YawnStretch = 0x13000090,
        Cringe = 0x13000091,
        Kneel = 0x13000092,
        Plead = 0x13000093,
        Shiver = 0x13000094,
        Shoo = 0x13000095,
        Slouch = 0x13000096,
        Spit = 0x13000097,
        Surrender = 0x13000098,
        Woah = 0x13000099,
        Winded = 0x1300009a,
        YMCA = 0x1200009b,

        Pray = 0x130000ca,
        Mock = 0x130000cb,
        Teapot = 0x130000cc,

        ShakeFistState = 0x430000ea,
        PrayState = 0x430000eb,
        BowDeepState = 0x430000ec,
        ClapHandsState = 0x430000ed,
        CrossArmsState = 0x430000ee,
        ShiverState = 0x430000ef,
        PointState = 0x430000f0,
        WaveState = 0x430000f1,
        AkimboState = 0x430000f2,
        SaluteState = 0x430000f3,
        ScratchHeadState = 0x430000f4,
        TapFootState = 0x430000f5,
        LeanState = 0x430000f6,
        KneelState = 0x430000f7,
        PleadState = 0x430000f8,
        ATOYOT = 0x420000f9,
        SlouchState = 0x430000fa,
        SurrenderState = 0x430000fb,
        WoahState = 0x430000fc,
        WindedState = 0x430000fd,


        SnowAngelState = 0x43000118,
        WarmHands = 0x13000119,
        CurtseyState = 0x4300011a,
        AFKState = 0x4300011b,
        MeditateState = 0x4300011c,

        SitState = 0x4300013d,
        SitCrossleggedState = 0x4300013e,
        SitBackState = 0x4300013f,
        PointLeftState = 0x43000140,
        PointRightState = 0x43000141,
        TalktotheHandState = 0x43000142,
        PointDownState = 0x43000143,
        DrudgeDanceState = 0x43000144,
        PossumState = 0x43000145,
        ReadState = 0x43000146,
        ThinkerState = 0x43000147,
        HaveASeatState = 0x43000148,
        AtEaseState = 0x43000149,
        NudgeLeft = 0x1300014a,
        NudgeRight = 0x1300014b,
        PointLeft = 0x1300014c,
        PointRight = 0x1300014d,
        PointDown = 0x1300014e,

        Knock = 0x1300014f,
        ScanHorizon = 0x13000150,
        DrudgeDance = 0x13000151,
        HaveASeat = 0x13000152,

    }

    /// <summary>
    /// A motion command
    /// </summary>
    public enum MotionCommand : uint {
        Invalid = 0x0,
        /// <summary>
        /// Ready state (idle)
        /// </summary>
        Ready = 0x41000003,

        /// <summary>
        /// Move forward
        /// </summary>
        WalkForward = 0x45000005,

        /// <summary>
        /// Move backwards
        /// </summary>
        WalkBackwards = 0x45000006,

        /// <summary>
        /// Turn right
        /// </summary>
        TurnRight = 0x6500000d,

        /// <summary>
        /// Turn left
        /// </summary>
        TurnLeft = 0x6500000e,

        /// <summary>
        /// Side step right
        /// </summary>
        SideStepRight = 0x6500000f,

        /// <summary>
        /// Side step left
        /// </summary>
        SideStepLeft = 0x65000010,

        Crouch = 0x41000012,
        Sitting = 0x41000013,
        Sleeping = 0x41000014,

        StopTurning = 0x2000003a,

        Cheer = 0x1300004c,

        ShakeFist = 0x13000079,
        Beckon = 0x1300007a,
        BeSeeingYou = 0x1300007b,
        BlowKiss = 0x1300007c,
        BowDeep = 0x1300007d,
        ClapHands = 0x1300007e,
        Cry = 0x1300007f,
        Laugh = 0x13000080,
        MimeEat = 0x13000081,
        MimeDrink = 0x13000082,
        Nod = 0x13000083,
        Point = 0x13000084,
        ShakeHead = 0x13000085,
        Shrug = 0x13000086,
        Wave = 0x13000087,
        Akimbo = 0x13000088,
        HeartyLaugh = 0x13000089,
        Salute = 0x1300008a,
        ScratchHead = 0x1300008b,
        SmackHead = 0x1300008c,
        TapFoot = 0x1300008d,
        WaveHigh = 0x1300008e,
        WaveLow = 0x1300008f,
        YawnStretch = 0x13000090,
        Cringe = 0x13000091,
        Kneel = 0x13000092,
        Plead = 0x13000093,
        Shiver = 0x13000094,
        Shoo = 0x13000095,
        Slouch = 0x13000096,
        Spit = 0x13000097,
        Surrender = 0x13000098,
        Woah = 0x13000099,
        Winded = 0x1300009a,
        YMCA = 0x1200009b,

        Pray = 0x130000ca,
        Mock = 0x130000cb,
        Teapot = 0x130000cc,

        ShakeFistState = 0x430000ea,
        PrayState = 0x430000eb,
        BowDeepState = 0x430000ec,
        ClapHandsState = 0x430000ed,
        CrossArmsState = 0x430000ee,
        ShiverState = 0x430000ef,
        PointState = 0x430000f0,
        WaveState = 0x430000f1,
        AkimboState = 0x430000f2,
        SaluteState = 0x430000f3,
        ScratchHeadState = 0x430000f4,
        TapFootState = 0x430000f5,
        LeanState = 0x430000f6,
        KneelState = 0x430000f7,
        PleadState = 0x430000f8,
        ATOYOT = 0x420000f9,
        SlouchState = 0x430000fa,
        SurrenderState = 0x430000fb,
        WoahState = 0x430000fc,
        WindedState = 0x430000fd,


        SnowAngelState = 0x43000118,
        WarmHands = 0x13000119,
        CurtseyState = 0x4300011a,
        AFKState = 0x4300011b,
        MeditateState = 0x4300011c,

        SitState = 0x4300013d,
        SitCrossleggedState = 0x4300013e,
        SitBackState = 0x4300013f,
        PointLeftState = 0x43000140,
        PointRightState = 0x43000141,
        TalktotheHandState = 0x43000142,
        PointDownState = 0x43000143,
        DrudgeDanceState = 0x43000144,
        PossumState = 0x43000145,
        ReadState = 0x43000146,
        ThinkerState = 0x43000147,
        HaveASeatState = 0x43000148,
        AtEaseState = 0x43000149,
        NudgeLeft = 0x1300014a,
        NudgeRight = 0x1300014b,
        PointLeft = 0x1300014c,
        PointRight = 0x1300014d,
        PointDown = 0x1300014e,

        Knock = 0x1300014f,
        ScanHorizon = 0x13000150,
        DrudgeDance = 0x13000151,
        HaveASeat = 0x13000152,
    }
}
