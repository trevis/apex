﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Lib.Autonav {
    public class NavRoute {
        public readonly List<NavPoint> NavPoints = new();
        public string Name { get; set; } = "New Nav Route";

        public NavPoint? CurrentPoint { get; set; }

        public NavRoute(string name) {
            
        }
    }
}
