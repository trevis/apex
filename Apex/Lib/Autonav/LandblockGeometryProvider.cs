﻿using DotRecast.Core.Numerics;
using DotRecast.Recast;
using DotRecast.Recast.Geom;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Lib.Autonav {
    public class LandblockGeometryProvider : IInputGeomProvider {
        public void AddConvexVolume(RcConvexVolume convexVolume) {
            throw new NotImplementedException();
        }

        public void AddOffMeshConnection(RcVec3f start, RcVec3f end, float radius, bool bidir, int area, int flags) {
            throw new NotImplementedException();
        }

        public IList<RcConvexVolume> ConvexVolumes() {
            throw new NotImplementedException();
        }

        public RcTriMesh GetMesh() {
            throw new NotImplementedException();
        }

        public RcVec3f GetMeshBoundsMax() {
            throw new NotImplementedException();
        }

        public RcVec3f GetMeshBoundsMin() {
            throw new NotImplementedException();
        }

        public List<RcOffMeshConnection> GetOffMeshConnections() {
            throw new NotImplementedException();
        }

        public IEnumerable<RcTriMesh> Meshes() {
            throw new NotImplementedException();
        }

        public void RemoveOffMeshConnections(Predicate<RcOffMeshConnection> filter) {
            throw new NotImplementedException();
        }
    }
}
