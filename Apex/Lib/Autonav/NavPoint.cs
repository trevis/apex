﻿using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.Lib.Autonav {
    public class NavPoint {
        public Coordinates Coordinates { get; private set; }

        public NavPoint() {
            Coordinates = Coordinates.Me;
        }

        public NavPoint(Coordinates coords) {
            Coordinates = coords;
        }
    }
}