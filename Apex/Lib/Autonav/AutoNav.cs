﻿using DotRecast.Detour;
using DotRecast.Recast.Toolset;
using DotRecast.Recast;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using Decal.Adapter;
using System.IO;
using DotRecast.Detour.Io;
using DotRecast.Detour.TileCache;
using DotRecast.Core;
using UtilityBelt.Service;
using System.Threading;
using Apex.Lib.Geometry;
using System.Linq;
using UtilityBelt.Scripting.Events;
using AcClient;
using ACE.Entity;
using DotRecast.Core.Numerics;
using DotRecast.Recast.Toolset.Tools;
using Apex.Tools;

namespace Apex.Lib.Autonav {
    public class TestTileCacheMeshProcess : IDtTileCacheMeshProcess {
        public void Process(DtNavMeshCreateParams option) {
            for (int i = 0; i < option.polyCount; ++i) {
                option.polyFlags[i] = 1;
            }
        }
    }

    public class AutoNav : IDisposable {
        private const int VERTS_PER_POLY = 6;
        private Dictionary<uint, long> _obstacleRefs = [];
        
        private long _lastRef;
        internal DtNavMesh? Mesh;

        public string InsideMeshDirectory => Path.Combine(PluginCore.AssemblyDirectory, "Meshes", "Inside");
        public string OutsideMeshDirectory => Path.Combine(PluginCore.AssemblyDirectory, "Meshes", "Outside");

        public LandblockGeometry Geometry { get; private set; }
        public List<CellGeometry> Neighbors { get; private set; }
        //public DtTileCache TileCache { get; private set; }

        public AutoNav() {
            if (!Directory.Exists(InsideMeshDirectory)) {
                Directory.CreateDirectory(InsideMeshDirectory);
            }
            if (!Directory.Exists(OutsideMeshDirectory)) {
                Directory.CreateDirectory(OutsideMeshDirectory);
            }

            PluginCore.Instance.Game.World.OnObjectCreated += World_OnObjectCreated;
            PluginCore.Instance.Game.World.OnObjectReleased += World_OnObjectReleased;
        }

        internal Coordinates GetRandomPointOnMesh() {
            if (PluginCore.Instance.Nav?.Mesh is null) {
                PluginCore.Log($"No navmesh loaded.");
                return null;
            }

            var query = new DtNavMeshQuery(PluginCore.Instance.Nav?.Mesh);
            var m_filter = new DtQueryDefaultFilter();
            var frand = new RcRand(DateTime.Now.Ticks);

            query.FindRandomPoint(m_filter, frand, out long randomRef, out var randomPt);

            return new Coordinates(Coordinates.Me.LandCell, randomPt.X, randomPt.Z, randomPt.Y);
        }

        /// <summary>
        /// Find a route to the specified destination coordinates.
        /// </summary>
        /// <returns>null if no route found.</returns>
        public List<Coordinates> FindRoute(Coordinates destination) {
            if (PluginCore.Instance.Nav?.Mesh is null) {
                PluginCore.Log($"No navmesh loaded.");
                return null;
            }

            var me = Coordinates.Me;

            if ((me.LandCell & 0xFFFF0000) != (destination.LandCell & 0xFFFF0000)) {
                PluginCore.Log($"FindRoute only works inside a single landblock.");
                return null;
            }

            var rc = new RcTestNavMeshTool();

            var halfExtents = new RcVec3f(1.25f, 1.25f, 1.25f);

            var query = new DtNavMeshQuery(PluginCore.Instance.Nav?.Mesh);
            var m_filter = new DtQueryDefaultFilter();

            var startStatus = query.FindNearestPoly(me.ToNavLocal(), halfExtents, m_filter, out long startRef, out var startPt, out bool isStartOverPoly);
            var endStatus = query.FindNearestPoly(destination.ToNavLocal(), halfExtents, m_filter, out long endRef, out var endPt, out bool isEndOverPoly);

            var polys = new List<long>();
            var path = new List<DtStraightPath>();

            var res = rc.FindStraightPath(query, startRef, endRef, startPt, endPt, m_filter, true, ref polys, ref path, 0);

            //var res = rc.FindFollowPath(PluginCore.Instance.Nav?.Mesh, query, startRef, endRef, startPt, endPt, m_filter, false, ref polys, ref pts);

            var pts = path.Select(p => p.pos).ToList();

            NavTool._path = pts;

            var coordList = pts?.Select(p => new Coordinates(me.LandCell, p.X, p.Z, p.Y)).ToList();

            if (coordList.LastOrDefault()?.DistanceTo(destination) > 1.25f) {
                return new();
            }
            return coordList;
        }

        private void World_OnObjectReleased(object sender, ObjectReleasedEventArgs e) {
            try {
                TryRemoveObstacle(e.ObjectId);
            }
            catch(Exception ex) { PluginCore.Log(ex); }
        }

        private void World_OnObjectCreated(object sender, ObjectCreatedEventArgs e) {
            try {
                TryAddObstacle(e.ObjectId);
            }
            catch (Exception ex) { PluginCore.Log(ex); }
        }

        private void TryAddExistingObstacles() {
            var allObjs = PluginCore.Instance.Game.World.GetAll();
            foreach (var wo in allObjs) {
                TryAddObstacle(wo.Id);
            }
        }

        private unsafe void TryAddObstacle(uint objectId) {
            /*
            if (_obstacleRefs.TryGetValue(objectId, out var refId)) {
                TryRemoveObstacle(objectId);
            }

            var obj = PluginCore.Instance.Game.World.Get(objectId);
            if (obj is null || TileCache is null) {
                return;
            }

            var obstacleRadius = 0.3f;

            switch (obj.ObjectClass) {
                case UtilityBelt.Scripting.Enums.ObjectClass.Npc:
                case UtilityBelt.Scripting.Enums.ObjectClass.Vendor:
                    break;
                case UtilityBelt.Scripting.Enums.ObjectClass.Lifestone:
                    obstacleRadius = 1f;
                    break;
                case UtilityBelt.Scripting.Enums.ObjectClass.Portal:
                    obstacleRadius = 1f;
                    break;
                default:
                    return;
            }

            var pObj = CPhysicsObj.GetObjectA(objectId);
            if (pObj is null) {
                return;
            }

            var pos = pObj->m_position.frame.m_fOrigin;
            var obstacleId = TileCache.AddObstacle(new DotRecast.Core.Numerics.RcVec3f(pos.x, pos.y, pos.z), obstacleRadius, 1.8f);

            CoreManager.Current.Actions.AddChatText($"Adding obstacle: {obj.Name}", 1);

            _obstacleRefs.Add(objectId, obstacleId);
            */
        }

        private void TryRemoveObstacle(uint objectId) {
            _obstacleRefs.Remove(objectId);
        }

        internal void Update() {
            //var updated = TileCache?.Update();
        }

        public async Task<List<Coordinates>> LoadMesh(Coordinates start, Coordinates end) {
            var path = new List<Coordinates>();

            if (Math.Abs(start.LBX() - end.LBX()) > 1 || Math.Abs(start.LBY() - end.LBY()) > 1) {
                throw new Exception($"Start landblock and end landblock need to be at most 1 tile apart... 0x{start.LandCell:X8} vs 0x{end.LandCell:X8}");
            }

            var meshData = await LoadLocalLandblock(start);

            if (meshData is not null) {
                Mesh = new DtNavMesh(meshData, VERTS_PER_POLY, 0);
                /*
                var settings = GetMeshSettings();
                TileCache = new DtTileCache(new DtTileCacheParams() {
                    ch = settings.cellHeight,
                    cs = settings.cellSize,
                    maxObstacles = 2048,
                    maxTiles = 100,
                    height = 10,
                    width = 10,
                    walkableRadius = settings.agentRadius,
                    walkableClimb = settings.agentMaxClimb,
                    walkableHeight = settings.agentHeight,
                }, new DtTileCacheStorageParams(RcByteOrder.LITTLE_ENDIAN, true), Mesh, null, new TestTileCacheMeshProcess());

                TryAddExistingObstacles();
                while (true) {
                    if (TileCache.Update()) {
                        break;
                    }
                }
                */
            }

            return path;
        }

        private async Task<DtMeshData?> LoadLocalLandblock(Coordinates coords) {
            if (coords.IsOutside()) {
                PluginCore.Log($"Autonav only works inside dungeons / buildings!");
                return null;
            }

            return await LoadInsideLandblock(coords);
        }

        private async Task<DtMeshData> LoadInsideLandblock(Coordinates coords) {
            if (coords.IsOutside()) {
                PluginCore.Log($"Autonav only works inside dungeons!");
                return null;
            }

            var geometry = new LandblockGeometry(coords.LandCell & 0xFFFF0000);
            if (!geometry.DungeonCells.TryGetValue(coords.LandCell, out var cellGeometry)) {
                PluginCore.Log($"Could not load cell geometry! {coords} cellGeometry:{cellGeometry}");
                return null;
            }

            Dictionary<uint, bool> checkedCells = new();
            var cells = cellGeometry.GetConnectedCells(ConnectionStrategy.Visible, checkedCells, out var neighbors);

            cells.Sort((a, b) => a.CellId.CompareTo(b.CellId));

            Geometry = geometry;
            Neighbors = cells;

            var lowCellId = Neighbors.FirstOrDefault()?.CellId;

            if (lowCellId == 0) {
                PluginCore.Log($"lowCellId was 0!"); 
                return null;
            }

            var meshPath = Path.Combine(InsideMeshDirectory, $"{lowCellId:X8}.mesh");
            if (File.Exists(meshPath)) {
                try {
                    var meshReader = new DtMeshDataReader();

                    using (var stream = File.OpenRead(meshPath))
                    using (var reader = new BinaryReader(stream)) {
                        var rcBytes = new RcByteBuffer(reader.ReadBytes((int)stream.Length));
                        var meshData = meshReader.Read(rcBytes, VERTS_PER_POLY, true);
                        
                        return meshData;
                    }
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }

            var geom = CellGeometryProvider.LoadGeometry(Geometry, Neighbors);
            if (geom is null) return null;

            var builder = new NavMeshBuilder();
            var settings = GetMeshSettings(coords);
            var res = builder.Build(geom, settings);

            if (res is not null) {
                var meshWriter = new DtMeshDataWriter();
                using (var stream = File.OpenWrite(meshPath))
                using (var writer = new BinaryWriter(stream)) {
                    meshWriter.Write(writer, res, RcByteOrder.LITTLE_ENDIAN, false); 
                }
            }

            return res;
        }

        private RcNavMeshBuildSettings GetMeshSettings(Coordinates? coords = null) {
            return new RcNavMeshBuildSettings() {
                agentHeight = 1.6f, // todo, lugians have different height at least...
                agentMaxClimb = 0.95f,
                agentMaxSlope = 50f,
                cellHeight = 0.1f,
                cellSize = 0.1f,
                agentRadius = 0.45f,
                detailSampleDist = 6.0f, 
                detailSampleMaxError = 1.0f,
                edgeMaxError = 1f,
                edgeMaxLen = 12.0f,
                mergedRegionSize = 20, 
                minRegionSize = 8,
                vertsPerPoly = VERTS_PER_POLY,
                partitioning = (int)RcPartition.WATERSHED  
            };
        }

        public void Dispose() {
            PluginCore.Instance.Game.World.OnObjectCreated -= World_OnObjectCreated;
            PluginCore.Instance.Game.World.OnObjectReleased -= World_OnObjectReleased;
        }
    }
}
