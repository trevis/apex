﻿using Decal.Adapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Apex.Lib.Settings {
    public class ProfileSettings : INotifyPropertyChanged {
        private string _name = "";
        private ObservableCollection<StrategySettings> _strategies = new ObservableCollection<StrategySettings>();

        [JsonIgnore]
        internal static string ProfilesDirectory {
            get {
                return Path.Combine(PluginCore.AssemblyDirectory, "profiles");
            }
        }

        private string ProfilePath {
            get {
                var name = new string(Name.Where(ch => !Path.GetInvalidFileNameChars().Contains(ch)).ToArray());
                return Path.Combine(ProfilesDirectory, $"{name}.json");
            }
        }

        public string Name {
            get { return _name; }
            set {
                if (value == _name) return;
                _name = value;
                NotifyPropertyChanged();
            }
        }

        public ObservableCollection<StrategySettings> Strategies {
            get { return _strategies; }
            set {
                if (value == _strategies) return;
                _strategies = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public ProfileSettings() {
        
        }

        public ProfileSettings(string name) {
            try {
                Name = name;

                if (Name != "Default") {
                    Load();
                }
                else {
                    this.Strategies = new ObservableCollection<StrategySettings>() {
                        new StrategySettings() {
                            Name = "Manage Vitals"
                        },
                        new StrategySettings() {
                            Name = "Combat"
                        },
                        new StrategySettings() {
                            Name = "Navigation"
                        }
                    };
                }

                foreach (var strategy in Strategies) {
                    strategy.PropertyChanged += (_, _) => {
                        NotifyPropertyChanged(nameof(Strategies));
                    };
                }

                Strategies.CollectionChanged += (_, e) => {
                    try {
                        if (e.Action == System.Collections.Specialized.NotifyCollectionChangedAction.Add) {
                            foreach (var strategy in e.NewItems) {
                                (strategy as StrategySettings).PropertyChanged += (_, _) => {
                                    NotifyPropertyChanged(nameof(Strategies));
                                };
                            }
                        }
                        NotifyPropertyChanged(nameof(Strategies));
                    }
                    catch (Exception ex) {
                        PluginCore.Log(ex);
                    }
                };

                PropertyChanged += (_, _) => Save();
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void Load() {
            try {
                if (File.Exists(ProfilePath)) {
                    var json = File.ReadAllText(ProfilePath);
                    var settings = JsonConvert.DeserializeObject<ProfileSettings>(json, Settings.SerializerSettings);
                    if (settings != null) {
                        _strategies = settings.Strategies; 
                    }
                }
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void Save() {
            try {
                if (Name.ToLower() == "default") return;
                var json = JsonConvert.SerializeObject(this, Formatting.Indented, Settings.SerializerSettings);
                File.WriteAllText(ProfilePath, json);
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        internal ProfileSettings Clone(string newProfileName) {
            var clone = new ProfileSettings(newProfileName);
            foreach (var strategy in Strategies) {
                clone.Strategies.Add(strategy.Clone());
            }
            return clone;
        }
    }
}