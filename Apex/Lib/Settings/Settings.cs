﻿using Decal.Adapter;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.Settings.Serializers;

namespace Apex.Lib.Settings { 
    public class Settings : INotifyPropertyChanged {
        public static readonly JsonSerializerSettings SerializerSettings = new() {
            Converters = new List<JsonConverter>() {
                new ImGuiVectorConverter()
            },
            TypeNameHandling = TypeNameHandling.Auto
        }; 
        private bool _enabled = false;
        private bool _disableOnDeath = true;
        private Vector2 _windowPosition = new Vector2(120, 120);
        private string _loadedProfile = "Default";

        private string SettingsFilePath {
            get {
                var charName = CoreManager.Current.CharacterFilter.Name;
                charName = new string(charName.Where(ch => !Path.GetInvalidFileNameChars().Contains(ch)).ToArray());
                return Path.Combine(PluginCore.AssemblyDirectory, "settings", $"{charName}.json");
            }
        }

        /// <summary>
        /// enabled
        /// </summary>
        public bool Enabled {
            get => _enabled;
            set {
                if (value != _enabled) {
                    _enabled = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// disable on death
        /// </summary>
        public bool DisableOnDeath {
            get => _disableOnDeath;
            set {
                if (value != _disableOnDeath) {
                    _disableOnDeath = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// main plugin window position
        /// </summary>
        public Vector2 WindowPosition {
            get => _windowPosition;
            set {
                if (value.X != _windowPosition.X || value.Y != _windowPosition.Y) {
                    _windowPosition = value;
                    NotifyPropertyChanged();
                }
            }
        }

        /// <summary>
        /// loaded profile
        /// </summary>
        public string LoadedProfile {
            get => _loadedProfile;
            set {
                if (value != _loadedProfile) {
                    _loadedProfile = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public Settings() {
            if (!Directory.Exists(Path.Combine(PluginCore.AssemblyDirectory, "settings"))) {
                Directory.CreateDirectory(Path.Combine(PluginCore.AssemblyDirectory, "settings"));
            }

            PropertyChanged += (_, _) => Save();
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public void Load() {
            try {
                if (File.Exists(SettingsFilePath)) {
                    var json = File.ReadAllText(SettingsFilePath);
                    var settings = Newtonsoft.Json.JsonConvert.DeserializeObject<Settings>(json, SerializerSettings);
                    if (settings != null) {
                        _enabled = settings.Enabled;
                        _disableOnDeath = settings.DisableOnDeath;
                        _windowPosition = settings.WindowPosition;
                        _loadedProfile = settings.LoadedProfile;
                        PluginCore.Log($"Window Position: {_windowPosition}");
                    }
                }
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void Save() {
            try {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(this, Formatting.Indented, SerializerSettings);
                File.WriteAllText(SettingsFilePath, json);
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }
    }
}
