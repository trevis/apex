﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Apex.Lib.Settings {
    public class StrategySettings : INotifyPropertyChanged {
        private string _name;
        private Dictionary<string, object> _settings = new Dictionary<string, object>();

        public string Name { get { return _name; } set { _name = value; NotifyPropertyChanged(); } }
        public Dictionary<string, object> Settings {
            get => _settings;
            set {
                if (JsonConvert.SerializeObject(_settings) == JsonConvert.SerializeObject(value)) return;

                _settings = value;
                NotifyPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        internal StrategySettings Clone() {
            return new StrategySettings() {
                Name = Name,
                Settings = new Dictionary<string, object>(Settings)
            };
        }

        private void NotifyPropertyChanged([CallerMemberName] string propertyName = "") {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}