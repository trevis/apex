﻿using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Apex.Lib.Geometry.Maps
{
    public class MapCellGeometry
    {
        private List<MapCellGeometry> _connectedPitCells;
        private List<MapCellGeometry> _neighbors;
        private bool? _isPit;
        private bool? _isCatwalk;
        private static Dictionary<CellGeometry, MapCellGeometry> _cache = new Dictionary<CellGeometry, MapCellGeometry>();

        public CellGeometry CellGeometry { get; }

        /// <summary>
        /// Wether or not this cell is a "pit". pits are defined as having no walkable polys, or multiple levels
        /// like in the case of the side rooms in marketplace where the catwalks are.
        /// </summary>
        public bool IsPit => _isPit ??= _IsPit();

        public Vector3 Position => new Vector3(
            CellGeometry.EnvCell.Position.Origin.X,
            CellGeometry.EnvCell.Position.Origin.Y,
            CellGeometry.EnvCell.Position.Origin.Z + (CellGeometry.IsCatwalk && !IsPit ? 6f : 0f)
        );

        public IEnumerable<MapCellGeometry> Neighbors
        {
            get
            {
                if (_neighbors == null)
                {
                    _neighbors = GetNeighbors();
                }
                return _neighbors;
            }
        }

        public IEnumerable<MapCellGeometry> ConnectedPitCells
        {
            get
            {
                if (_connectedPitCells == null)
                {
                    _connectedPitCells = GetConnectedPitCells();
                }
                return _connectedPitCells;
            }
        }

        public static MapCellGeometry FromCache(CellGeometry cellGeometry)
        {
            if (!_cache.TryGetValue(cellGeometry, out var mapCellGeometry))
            {
                mapCellGeometry = new MapCellGeometry(cellGeometry);
                _cache.Add(cellGeometry, mapCellGeometry);
            }
            return mapCellGeometry;
        }

        private MapCellGeometry(CellGeometry cellGeometry)
        {
            CellGeometry = cellGeometry;
        }

        /// <summary>
        /// Check if this map cell is directly connected to the specified neighbor in the specified direction.
        /// </summary>
        /// <param name="neighbor">The neighbor to check</param>
        /// <param name="direction">The direction to check in.</param>
        /// <returns></returns>
        public bool IsDirectlyConnectedTo(MapCellGeometry neighbor, Vector3 direction)
        {
            var wantedDirection = Vector3.Normalize(direction);
            var actualDirection = Vector3.Normalize(neighbor.CellGeometry.EnvCell.Position.Origin.ToNumerics() - CellGeometry.EnvCell.Position.Origin.ToNumerics());

            var diff = new Vector3(Math.Abs(wantedDirection.X - actualDirection.X), Math.Abs(wantedDirection.Y - actualDirection.Y), Math.Abs(wantedDirection.Z - actualDirection.Z));

            var allowance = 0.05f;

            return diff.X < allowance && diff.Y < allowance && diff.Z < allowance;
        }

        private List<MapCellGeometry> GetConnectedPitCells()
        {
            return Neighbors.Where(n => n.IsPit).ToList();
        }

        private List<MapCellGeometry> GetNeighbors()
        {
            var neighbors = new List<MapCellGeometry>();

            foreach (var portal in CellGeometry.EnvCell.CellPortals)
            {
                neighbors.Add(FromCache(CellGeometry.FromCache((CellGeometry.CellId & 0xFFFF0000) + portal.OtherCellId, CellGeometry.Type)));
            }

            return neighbors;
        }

        private bool _IsPit()
        {
            if (!CellGeometry.HasWalkablePolys)
            {
                return true;
            }

            /*
            foreach (var neighbor in Neighbors) {
                if (neighbor.IsPit && IsDirectlyConnectedTo(neighbor, new Vector3(0, 0, 1))) {
                    return true;
                }
            }
            */

            return false;
        }
    }
}