﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Apex.Lib.Geometry.Maps
{
    /// <summary>
    /// A collection of dungeon cells in a landblock, where all contained cells are connected to eachother.
    /// </summary>
    public class DungeonChunkGeometry : MapCellGeometryContainer
    {
        private List<MapCellGeometryContainer> _floorChunks = null;

        /// <summary>
        /// Cells contained within this chunk, grouped by same floor and visibility.
        /// </summary>
        public IEnumerable<MapCellGeometryContainer> FloorChunks
        {
            get
            {
                if (_floorChunks == null)
                {
                    _floorChunks = GetFloorChunks();
                }

                return _floorChunks;
            }
        }

        public DungeonChunkGeometry(IEnumerable<MapCellGeometry> cells) : base(cells) {

        }


        private List<MapCellGeometryContainer> GetFloorChunks()
        {
            var floorChunks = new List<MapCellGeometryContainer>();
            var checkedCells = new Dictionary<uint, bool>();

            // loop over each cell in this dungeon, checking that it isnt already used in a layer, and, starting with that cell, find
            // all connected cells that are on the same floor.
            foreach (var cell in Cells)
            {
                if (checkedCells.ContainsKey(cell.CellGeometry.CellId))
                    continue;

                // this grabs all cells connected to the current cell, that are also on the same floor.
                // the first pass limits to the same floor to try and form big chunks of cells that we know
                // wont overlap eachother.
                var floorCells = cell.CellGeometry.GetConnectedCells(ConnectionStrategy.VisibleSameFloor, checkedCells, out var neighbors);

                if (floorCells.Count > 0)
                {
                    floorChunks.Add(new MapCellGeometryContainer(floorCells.Select(f => MapCellGeometry.FromCache(f))));
                }
            }

            var catwalks = floorChunks.Where(c => c.Cells.All(g => g.CellGeometry.IsCatwalk)).ToList();

            floorChunks.RemoveAll(catwalks.Contains);

            while (true) {
                var didMerge = false;

                for (var i = 0; i < catwalks.Count; i++) {
                    for (var x = i + 1; x < catwalks.Count; x++) {
                        var layer = catwalks[i];
                        var nextLayer = catwalks[x];

                        if (layer.IsNeighborsWithCatwalk(nextLayer)) {
                            layer.Merge(nextLayer);
                            catwalks.Remove(nextLayer);
                            // we did a merge, so start over
                            didMerge = true;
                            break;
                        }
                    }
                    if (didMerge) {
                        break;
                    }
                }

                // none left to merge, break loop
                if (!didMerge) {
                    break;
                }
            }

            floorChunks.AddRange(catwalks);
            
            floorChunks.Sort((a, b) => {
                var az = a.Cells.First().Position.Z;
                var bz = b.Cells.First().Position.Z;

                return az.CompareTo(bz);
            });

            return floorChunks;
        }
    }
}
