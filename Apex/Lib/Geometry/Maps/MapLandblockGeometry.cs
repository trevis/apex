﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Apex.Lib.Geometry.Maps
{
    /// <summary>
    /// Landblock geometry wrapper class for use with map stuff
    /// </summary>
    public class MapLandblockGeometry
    {
        private LandblockGeometry _landblockGeometry;
        private List<DungeonChunkGeometry> _dungeonChunks = null;

        /// <summary>
        /// The id of the landblock this map geometry represents, in the format of 0xAAAA0000 where AAAA is the landblock id
        /// </summary>
        public uint LandblockId { get; }
        public uint? StartingCell { get; private set; }

        /// <summary>
        /// A list of all dungeon cells in this landblock, grouped by cell visibility.
        /// </summary>
        public IEnumerable<DungeonChunkGeometry> DungeonChunks
        {
            get
            {
                if (_dungeonChunks == null)
                {
                    _dungeonChunks = GetDungeonChunks();
                }

                return _dungeonChunks;
            }
        }

        public LandblockGeometry LandblockGeometry
        {
            get
            {
                if (_landblockGeometry == null)
                {
                    _landblockGeometry = new LandblockGeometry(LandblockId);
                }

                return _landblockGeometry;
            }
        }

        public MapLandblockGeometry(uint landblockId, uint? startingCell = null)
        {
            if (landblockId <= 0xFFFF)
            {
                landblockId = landblockId << 16;
            }
            LandblockId = landblockId & 0xFFFF0000;
            StartingCell = startingCell;
        }

        /// <summary>
        /// Get a list of all dungeon chunks in this landblock. A dungeon chunk is a list of cells that are visible
        /// to eachother but are not connected to terrain via a building portal.
        /// </summary>
        /// <param name="checkedCells">Cells that have already been checked, and should be ignored.</param>
        /// <returns>A list of dungeon cells grouped by visibility</returns>
        public List<DungeonChunkGeometry> GetDungeonChunks(Dictionary<uint, bool> checkedCells = null)
        {
            if (checkedCells == null)
            {
                checkedCells = new Dictionary<uint, bool>();
            }

            var dungeonChunks = new List<DungeonChunkGeometry>();

            if (StartingCell != null && StartingCell != 0 && LandblockGeometry.DungeonCells.Any(c => (c.Value.CellId & 0x0000FFFF) == StartingCell))
            {
                var cell = CellGeometry.FromCache(LandblockId + StartingCell.Value, CellType.Dungeon);
                var cells = cell.GetConnectedCells(ConnectionStrategy.Visible, checkedCells, out var neighbors);
                if (cells.Where(c => {
                    return !(c.EnvCell.EnvironmentId == (uint)0x0D0000CA || c.EnvCell.EnvironmentId == (uint)0x0D00016D);
                }).Count() > 1) {
                    dungeonChunks.Add(new DungeonChunkGeometry(cells.Select(c => MapCellGeometry.FromCache(c))));
                }
            }
            else
            {
                foreach (var cell in LandblockGeometry.DungeonCells.Values)
                {
                    if (checkedCells.ContainsKey(cell.CellId))
                        continue;

                    var cells = cell.GetConnectedCells(ConnectionStrategy.Visible, checkedCells, out var neighbors);
                    if (cells.Where(c => {
                        return !(c.EnvCell.EnvironmentId == (uint)0x0D0000CA || c.EnvCell.EnvironmentId == (uint)0x0D00016D);
                    }).Count() > 1)
                    {
                        dungeonChunks.Add(new DungeonChunkGeometry(cells.Select(c => MapCellGeometry.FromCache(c))));
                    }
                }
            }

            return dungeonChunks;
        }
    }
}
