﻿using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace Apex.Lib.Geometry.Maps {
    public class MapGrid {
        public List<MapChunkGrid> Chunks = new List<MapChunkGrid>();

        public static MapGrid ByFloors(List<MapCellGeometry> cells) {
            var grid = new MapGrid();
            var floorChunks = new List<MapChunkGrid>();
            var checkedCells = new Dictionary<uint, bool>();

            // loop over each cell in this dungeon, checking that it isnt already used in a layer, and, starting with that cell, find
            // all connected cells that are on the same floor.
            foreach (var cell in cells) {
                if (cell.CellGeometry.ShouldIgnoreEnvironment(cell.CellGeometry.Environment.Id))
                    continue;

                if (checkedCells.ContainsKey(cell.CellGeometry.CellId))
                    continue;

                // this grabs all cells connected to the current cell, that are also on the same floor.
                // the first pass limits to the same floor to try and form big chunks of cells that we know
                // wont overlap eachother.
                var floorCells = cell.CellGeometry.GetConnectedCells(ConnectionStrategy.VisibleSameFloorSeparatePits, checkedCells, out var neighbors);

                if (floorCells.Count > 0) {
                    var chunk = new MapChunkGrid(floorCells.Select(f => MapCellGeometry.FromCache(f)), grid);
                    floorChunks.Add(chunk);
                }
            }

            MergeCatwalkChunks(ref floorChunks);
            MergePits(ref floorChunks);

            //floorChunks = floorChunks.Where(fc => fc.Cells.Count() > 0).ToList();

            floorChunks.Sort((a, b) => {
                var az = a.Cells.FirstOrDefault()?.Position.Z ?? float.MinValue;
                var bz = b.Cells.FirstOrDefault()?.Position.Z ?? float.MinValue;
                return az.CompareTo(bz);
            });

            grid.Chunks.AddRange(floorChunks);

            for (var i = 0; i < grid.Chunks.Count; i++) {
                grid.Chunks[i].Id = (uint)i + 1;
            }

            foreach (var chunk in grid.Chunks) {
                var otherCells = grid.Chunks.Where(c => c.Id != chunk.Id).SelectMany(c => c.Cells.Select(c => c.CellGeometry.CellId & 0x0000FFFF)).ToList();
                foreach (var cell in chunk.Cells) {
                    foreach (var portal in cell.CellGeometry.EnvCell.CellPortals) {
                        if (!chunk.Cells.Any(c => (c.CellGeometry.CellId & 0x0000FFFF) == portal.OtherCellId) && otherCells.Contains(portal.OtherCellId)) {
                            var otherCell = CellGeometry.FromCache(portal.OtherCellId + (cell.CellGeometry.CellId & 0xFFFF0000), CellType.Dungeon);
                            var pos = (cell.CellGeometry.EnvCell.Position.Origin + otherCell.EnvCell.Position.Origin) / 2f;
                            var otherChunk = grid.Chunks.FirstOrDefault(ch => ch.Cells.Any(c => c.CellGeometry.CellId == otherCell.CellId));

                            var cellStruct = cell.CellGeometry.Environment.Cells[cell.CellGeometry.EnvCell.CellStructure];
                            var poly = cellStruct.Polygons[portal.PolygonId];

                            // check that we can fit through this portal poly
                            var bb = new BoundingBox(poly.VertexIds.Select(vId => cellStruct.VertexArray.Vertices[(ushort)vId].Origin.ToNumerics()));
                            if (bb.Size.Z < 1.9)
                                continue;

                            var cell2 = MapCellGeometry.FromCache(otherCell);
                            var c1 = new Vector2(cell.Position.X, -cell.Position.Y);
                            var c2 = new Vector2(cell2.Position.X, -cell2.Position.Y);

                            var n = Vector2.Normalize(c2 - c1);
                            c1 += n * 4;
                            c2 -= n * 4;

                            if (float.IsNaN(n.X) || float.IsInfinity(n.X) || float.IsNaN(n.Y) || float.IsInfinity(n.Y)) {
                                continue;
                            }

                            chunk.Connections.Add(new ChunkGridConnection() {
                                X1 = c1.X,
                                Y1 = c1.Y,
                                X2 = c2.X,
                                Y2 = c2.Y,
                                OtherChunkId = otherChunk.Id
                            });
                        }
                    }
                }
            }

            return grid;
        }

        /// <summary>
        /// merge pits into the floor chunk below them, unless there is a catwalk occupying its space
        /// </summary>
        /// <param name="floorChunks"></param>
        /// <exception cref="NotImplementedException"></exception>
        private static void MergePits(ref List<MapChunkGrid> floorChunks) {
            var pits = floorChunks.Where(fc => fc.Cells.All(c => c.IsPit));
            foreach (var pit in pits.ToArray()) {
                var belowChunks = floorChunks.Where(fc => fc.Cells.Where(c => !c.IsPit).Select(c => (int)c.Position.Z).Any(c => c < pit.FloorZ));
                foreach (var chunk in belowChunks) {
                    var x = (int)pit.Cells.First().Position.X;
                    var y = (int)pit.Cells.First().Position.Y;
                    if (chunk.AtXY(x, y).Count() > 0) {
                        floorChunks.Remove(pit);
                        chunk.Merge(pit);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// combine catwalks that are touching eachother and on the same z layer into the same chunk
        /// </summary>
        /// <param name="floorChunks"></param>
        private static void MergeCatwalkChunks(ref List<MapChunkGrid> floorChunks) {
            var catwalks = floorChunks.Where(c => c.Cells.All(g => g.CellGeometry.IsCatwalk)).ToList();

            floorChunks.RemoveAll(catwalks.Contains);

            while (true) {
                var didMerge = false;

                for (var i = 0; i < catwalks.Count; i++) {
                    for (var x = i + 1; x < catwalks.Count; x++) {
                        var layer = catwalks[i];
                        var nextLayer = catwalks[x];

                        if (layer.IsNeighborsWithCatwalk(nextLayer)) {
                            layer.Merge(nextLayer);
                            catwalks.Remove(nextLayer);
                            // we did a merge, so start over
                            didMerge = true;
                            break;
                        }
                    }
                    if (didMerge) {
                        break;
                    }
                }

                // none left to merge, break loop
                if (!didMerge) {
                    break;
                }
            }

            foreach (var catwalk in catwalks.ToArray()) {
                var sameLevelChunks = floorChunks.Where(fc => fc.FloorZ == catwalk.FloorZ);
                foreach (var chunk in sameLevelChunks) {
                    var x = (int)catwalk.Cells.First().Position.X;
                    var y = (int)catwalk.Cells.First().Position.Y;
                    if (chunk.Cells.All(c => c.IsPit) && chunk.AtXY(x, y).Count() > 0) {
                        catwalks.Remove(catwalk);
                        chunk.Merge(catwalk);
                        break;
                    }
                }
            }

            floorChunks.AddRange(catwalks);
        }

        private MapGrid() {
            
        }

        public IEnumerable<MapChunkGrid> AtFloorLevel(int level) {
           return Chunks.Where(fc => fc.FloorZ == level);
        }
    }
}
