﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace Apex.Lib.Geometry.Maps
{
    public class MapCellGeometryContainer
    {
        /// <summary>
        /// A list of cells contained in this chunk
        /// </summary>
        public List<MapCellGeometry> Cells { get; } = new List<MapCellGeometry>();

        private List<uint> _extendedNeighbors = new List<uint>();

        private List<uint> _visibleCells = null;
        private List<uint> VisibleCells {
            get {
                BuildVisibleCells();
                return _visibleCells;
            }
        }

        private List<uint> _cellIds = null;
        private List<uint> CellIds {
            get {
                BuildCellIds();

                return _cellIds;
            }
        }

        public MapCellGeometryContainer()
        {

        }

        public MapCellGeometryContainer(IEnumerable<MapCellGeometry> cells) : this()
        {
            Cells.AddRange(cells);
        }

        private void BuildVisibleCells() {
            if (_visibleCells == null) {
                _visibleCells = Cells.SelectMany(c => c.CellGeometry.EnvCell.VisibleCells.Select(x => (uint)x)).ToList();
            }
        }

        private void BuildCellIds() {
            if (_cellIds == null) {
                _cellIds = Cells.Select(c => c.CellGeometry.EnvCell.Id & 0x0000FFFF).ToList();
            }
        }

        public bool IsNeighborsWith(MapCellGeometryContainer nextGeometry)
        {
            var cellPortals = Cells.SelectMany(c => c.CellGeometry.EnvCell.CellPortals.Select(p => p.OtherCellId));
            var neighborCells = nextGeometry.Cells.Select(n => (ushort)(n.CellGeometry.EnvCell.Id & 0xFFFF));

            return cellPortals.Intersect(neighborCells).Any();
        }

        public bool IsNeighborsWithCatwalk(MapCellGeometryContainer nextGeometry)
        {
            if (VisibleCells.Any(v => nextGeometry.CellIds.Contains(v))) {
                return true;
            }

            return false;
        }

        internal void Merge(MapCellGeometryContainer nextLayer)
        {
            BuildCellIds();
            BuildVisibleCells();
            Cells.AddRange(nextLayer.Cells);
            _visibleCells.AddRange(nextLayer.VisibleCells);
            _cellIds.AddRange(nextLayer.CellIds);
            _extendedNeighbors.Clear();
        }
    }
}