﻿using System.Numerics;

namespace Apex.Lib.Geometry.Maps {
    public class ChunkGridConnection {
        public float X1 { get; set; }
        public float Y1 { get; set; }
        public uint OtherChunkId { get; set; }
        public Vector2 Position { get; internal set; }
        public float X2 { get; internal set; }
        public float Y2 { get; internal set; }
    }
}