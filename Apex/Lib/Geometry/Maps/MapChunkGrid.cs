﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Apex.Lib.Geometry.Maps {
    public class MapChunkGrid {
        private Dictionary<int, Dictionary<int, List<MapCellGeometry>>> _cellsByXY = new Dictionary<int, Dictionary<int, List<MapCellGeometry>>>();

        public uint Id { get; set; }
        public MapGrid Parent { get; set; }
        public List<ChunkGridConnection> Connections { get; } = new List<ChunkGridConnection>();
        private List<uint> _visibleCells = null;
        private List<uint> _cellIds = null;

        private List<uint> VisibleCells {
            get {
                return _visibleCells;
            }
        }

        private List<uint> CellIds {
            get {
                return _cellIds;
            }
        }

        public IEnumerable<MapCellGeometry> Cells {
            get {
                return _cellsByXY.SelectMany(x => x.Value.SelectMany(y => y.Value));
            }
        }

        public int FloorZ {
            get {
                var cells = Cells.ToList();
                cells.Sort((a, b) => a.Position.Z.CompareTo(b.Position.Z));
                return (int)((cells.FirstOrDefault()?.Position.Z) ?? int.MinValue);
            }
        }

        public MapChunkGrid(IEnumerable<MapCellGeometry> cells, MapGrid parent) {
            Parent = parent;
            foreach (var cell in cells) {
                AddCell(cell);
            }
        }

        public void AddCell(MapCellGeometry cell) {
            var x = (int)cell.Position.X;
            var y = (int)cell.Position.Y;

            if (!_cellsByXY.ContainsKey(x)) {
                _cellsByXY.Add(x, new Dictionary<int, List<MapCellGeometry>>());
            }
            if (!_cellsByXY[x].ContainsKey(y)) {
                _cellsByXY[x].Add(y, new List<MapCellGeometry>());
            }

            _cellsByXY[x][y].Add(cell);
            _cellsByXY[x][y].Sort((a, b) => a.Position.Z.CompareTo(b.Position.Z));

            if (_visibleCells == null) {
                _visibleCells = new List<uint>();
            }
            if (_cellIds == null) {
                _cellIds = new List<uint>();
            }

            _visibleCells.AddRange(cell.CellGeometry.EnvCell.VisibleCells.Select(x => (uint)x));
            _cellIds.Add(cell.CellGeometry.EnvCell.Id & 0x0000FFFF);
        }

        public void RemoveCell(MapCellGeometry cell) {
            var x = (int)cell.Position.X;
            var y = (int)cell.Position.Y;

            if (_cellsByXY.TryGetValue(x, out var yCells) && yCells.TryGetValue(y, out var cells)) {
                cells.Remove(cell);
            }
        }

        public IEnumerable<MapCellGeometry> AtXY(int x, int y) {
            if (_cellsByXY.TryGetValue(x, out var xCells)) {
                if (xCells.TryGetValue(y, out var cells)) {
                    return cells;
                }
            }
            return new MapCellGeometry[0];
        }

        public bool IsNeighborsWith(MapChunkGrid nextGeometry) {
            var cellPortals = Cells.SelectMany(c => c.CellGeometry.EnvCell.CellPortals.Select(p => p.OtherCellId));
            var neighborCells = nextGeometry.Cells.Select(n => (ushort)(n.CellGeometry.EnvCell.Id & 0xFFFF));

            return cellPortals.Intersect(neighborCells).Any();
        }

        public bool IsNeighborsWithCatwalk(MapChunkGrid nextGeometry) {
            if (VisibleCells.Any(v => nextGeometry.CellIds.Contains(v))) {
                return true;
            }

            return false;
        }

        public void Merge(MapChunkGrid nextLayer) {
            foreach (var cell in nextLayer.Cells) {
                AddCell(cell);
            }
        }
    }
}
