﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Apex.Lib.Geometry
{
    public enum CellType
    {
        Terrain,
        Indoors,
        Dungeon
    }
}
