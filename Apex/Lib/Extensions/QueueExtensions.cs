﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Lib.Extensions {
    public static class QueueExtensions {
        public static bool TryDequeue<T>(this Queue<T> q, out T item) {
            if (q.Count > 0) {
                item = (T)q.Dequeue();
                return true;
            }

            item = default;

            return false;
        }
    }
}
