﻿using DotRecast.Core.Numerics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.Lib.Extensions {
    public static class CoordinatesExtensions {
        public static uint LBX(this Coordinates coords) {
            return coords.LandCell >> 24 & 0xFF;
        }

        public static uint LBY(this Coordinates coords) {
            return coords.LandCell >> 16 & 0xFF;
        }

        public static bool IsOutside(this Coordinates coords) {
            return (coords.LandCell & 0xFFFF) < 0x100;
        }

        public static RcVec3f ToNav(this Coordinates coords) {
            return new RcVec3f(coords.LBX() * 192f + coords.LocalX, coords.LocalZ, coords.LBY() * 192f + coords.LocalY);
        }

        public static RcVec3f ToNavLocal(this Coordinates coords) {
            return new RcVec3f(coords.LocalX, coords.LocalZ, coords.LocalY);
        }

        public static Coordinates ToCoords(this RcVec3f v) {
            var lbx = (uint)Math.Floor(v.X / 192f);
            var lby = (uint)Math.Floor(v.Z / 192f);
            var localX = v.X % 192f;
            var localY = v.Z % 192f;
            return new Coordinates((lbx << 24) + (lby << 16), localX, localY, v.Y);
        }
    }
}
