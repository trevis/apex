﻿local apex = require("apex")
local ac = require("acclient")
local ImGui = require("imgui").ImGui

---@class DefaultNavigationStrategy : DefaultNavigationInstance
local DefaultNavigationStrategy = {}

-- optionally define a .new() method that returns a new instance of strategy
-- The new method is passed a settings argument that is a serialized version
-- of the last loaded strategy with this same name. So you can use it to restore
-- profile strategy settings
function DefaultNavigationStrategy.new(settings)
  -- we define a new DefaultNavigationInstance class here to make intellisense happy
  -- with these "new" field definitions
  
  ---@class DefaultNavigationInstance
  local instance = setmetatable(settings or {}, DefaultNavigationStrategy)

  -- instance fields starting with _ are not serialized
  ---@type Coordinates | nil
  instance._randomNavCoords = nil

	return instance
end

--- Check if strategy needs to do work
---@return boolean NeedsWork True if work is needed
function DefaultNavigationStrategy:NeedsWork()
  -- always needs work
  return true
end

---Needs work implementation
---@return QueueAction | nil action The action performed, if any
function DefaultNavigationStrategy:DoWork()
  while true do
    self._randomNavCoords = self._randomNavCoords ~= nil and self._randomNavCoords or apex.AutoNav.GetRandomCoordinates()
    coroutine.yield(apex.AutoNav.NavTo(self._randomNavCoords, 5.0))
    self._randomNavCoords = apex.AutoNav.GetRandomCoordinates()
  end 
end

function DefaultNavigationStrategy:RenderStatus()
  ImGui.Text("Wandering to: " .. tostring(self._randomNavCoords))
end

function DefaultNavigationStrategy:RenderSettings()
  ImGui.Text("no nav settings...")
end

apex.Meta.RegisterStrategy("Navigation", DefaultNavigationStrategy)