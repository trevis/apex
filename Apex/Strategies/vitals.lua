﻿local apex = require("apex")
local ac = require("acclient")
local ImGui = require("imgui").ImGui

---Get an appropriate healing kit for the specified vital / amount
---@param missingVital VitalId
---@param missingAmount number
---@return WorldObject | nil
local GetKit = function (missingVital, missingAmount)
  local nameFilter = (missingVital == VitalId.Health and "Healing" or tostring(missingVital)) .. " Kit"
  return game.Character.GetFirstInventory(function (res)
    return res.ObjectClass == ObjectClass.HealingKit and string.find(res.Name, nameFilter) ~= nil
  end)
end

---@class DefaultVitalStrategy : DefaultVitalInstance
local DefaultVitalStrategy = {}

-- optionally define a .new() method that returns a new instance of strategy
-- The new method is passed a settings argument that is a serialized version
-- of the last loaded strategy with this same name. So you can use it to restore
-- profile strategy settings
function DefaultVitalStrategy.new(settings)
  -- we define a new DefaultVitalInstance class here to make intellisense happy
  -- with these "new" field definitions
  
  ---@class DefaultVitalInstance
  local instance = setmetatable(settings or {}, DefaultVitalStrategy)

  -- instance fields will automatically be serialized to profile settings

  ---@type { [VitalId]: number }
  instance.vitalRechargePercents = instance.vitalRechargePercents or {}
  
  -- lol, this is to ensure the table has actual good default entries...
  instance.vitalRechargePercents[VitalId.Health] = type(instance.vitalRechargePercents[VitalId.Health]) == "number" and instance.vitalRechargePercents[VitalId.Health] or 0.7
  instance.vitalRechargePercents[VitalId.Stamina] = type(instance.vitalRechargePercents[VitalId.Stamina]) == "number" and instance.vitalRechargePercents[VitalId.Stamina] or 0.7
  instance.vitalRechargePercents[VitalId.Mana] = type(instance.vitalRechargePercents[VitalId.Mana]) == "number" and instance.vitalRechargePercents[VitalId.Mana] or 0.7

  -- instance fields starting with _ are not serialized
  instance._statusText = "Idle"
  instance._missingVitalAmount = 0
  instance._missingVital = VitalId.Health
  instance._vitalAction = nil

	return instance
end

--- Check if strategy needs to do work
---@return boolean NeedsWork True if work is needed
function DefaultVitalStrategy:NeedsWork()
  local vitals = game.Character.Weenie.Vitals
  for vital, percent in pairs(self.vitalRechargePercents) do
    local currentPercent = vitals[vital].Current / vitals[vital].Max
    local missingVitalAmount = vitals[vital].Max - vitals[vital].Current
    if currentPercent < percent and missingVitalAmount > 0 then
      self._missingVitalAmount = missingVitalAmount
      self._missingVital = vital
      return true
    end
  end

  self._statusText = "Idle"
  return false
end

---Needs work implementation
---@return QueueAction | nil action The action performed, if any
function DefaultVitalStrategy:DoWork()
  -- do work here. if an action was run, return the action
  local kit = GetKit(self._missingVital, self._missingVitalAmount)
  if kit == nil then
    -- no kit... can't perform action
    self._statusText = "Need " .. tostring(self._missingVitalAmount) .. " " .. tostring(self._missingVital) .. " but no kit found!"
    return
  end
  
  self._statusText = "Need " .. tostring(self._missingVitalAmount) .. " " .. tostring(self._missingVital) .. " (" .. kit.Name .. ")"
  local opts = ActionOptions.new()
  ---@diagnostic disable-next-line: assign-type-mismatch
  opts.TimeoutMilliseconds = 500
  coroutine.yield(kit.UseOn(game.Character.Id, opts))
end

function DefaultVitalStrategy:RenderStatus()
  -- render a status ui, shown on main plugin window
  ImGui.Text(self._statusText)
end

function DefaultVitalStrategy:RenderSettings()
  for vitalId in pairs(self.vitalRechargePercents) do
    local didChange, newValue = ImGui.SliderFloat(tostring(vitalId), self.vitalRechargePercents[vitalId], 0, 1)
    if didChange then
      self.vitalRechargePercents[vitalId] = newValue
    end
  end
end

apex.Meta.RegisterStrategy("Manage Vitals", DefaultVitalStrategy)