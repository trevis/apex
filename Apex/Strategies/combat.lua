﻿local apex = require("apex")
local ac = require("acclient")
local ImGui = require("imgui").ImGui

local _attackTracker = {}

---get closest mob
---@return WorldObject|nil
local getClosestMob = function()
  local closestMob = nil
  local closestDistance = 100000
  for i,wo in ipairs(game.World.GetLandscape(ObjectClass.Monster)) do
    local woCoords = apex.Physics.GetPhysicsCoordinates(wo.Id)
    local woDist = ac.Coordinates.Me.DistanceTo(woCoords)
    if woCoords ~= nil and woDist < closestDistance and (_attackTracker[wo.Id] == nil or _attackTracker[wo.Id] < 5) then
      closestDistance = woDist
      closestMob = wo
    end 
  end
  return closestMob
end

---@class DefaultCombatStrategy : DefaultCombatInstance
local DefaultCombatStrategy = {}

-- optionally define a .new() method that returns a new instance of strategy
-- The new method is passed a settings argument that is a serialized version
-- of the last loaded strategy with this same name. So you can use it to restore
-- profile strategy settings
function DefaultCombatStrategy.new(settings)
  -- we define a new DefaultCombatInstance class here to make intellisense happy
  -- with these "new" field definitions
  
  ---@class DefaultCombatInstance
  local instance = setmetatable(settings or {}, DefaultCombatStrategy)

  -- instance fields starting with _ are not serialized
  instance._combatStatus = "Idle"

	return instance
end

--- Check if strategy needs to do work
---@return boolean NeedsWork True if work is needed
function DefaultCombatStrategy:NeedsWork()
  local closestMob = getClosestMob()
  if closestMob == nil then
    self._combatStatus = "Idle"
  end
  return closestMob ~= nil
end

---Needs work implementation
---@return QueueAction | nil action The action performed, if any
function DefaultCombatStrategy:DoWork()
  -- todo: sticky to target a bit... instead of always nearest
  if game.Character.CombatMode == CombatMode.NonCombat then
    coroutine.yield(game.Actions.SetCombatMode(CombatMode.Melee))
  end

  ---@type WorldObject | nil
  local closestMob = getClosestMob()
  if closestMob == nil then return nil end

  local coords = apex.Physics.GetPhysicsCoordinates(closestMob.Id)
  if coords == nil then return nil end
  
  _attackTracker[closestMob.Id] = _attackTracker[closestMob.Id] and (_attackTracker[closestMob.Id] + 1) or 1

  -- close enough to attack?
  
  local route = apex.AutoNav.FindRoute(coords)
  if #route == 0 then
    -- no path?
    _attackTracker[closestMob.Id] = _attackTracker[closestMob.Id] and (_attackTracker[closestMob.Id] + 5) or 5
    print("could not path to:", closestMob, "- adding to ignore")
    return
  end

  if #route <= 2 or ac.Coordinates.Me.DistanceTo(coords) < 2 then
    ac.Combat.RepeatAttacks = false
    apex.Movement.SetHeadingTo(coords)
    self._combatStatus = "Attacking " .. tostring(closestMob)
    local res = coroutine.yield(ac.Combat.Attack(closestMob.Id))
    
    if not res.Success then
      self._combatStatus = "Doing a little wiggle because probably stuck?"
      apex.Movement.SetMotionRaw(0x6500000d);
      coroutine.yield(game.Actions.Sleep(100))
      apex.Movement.ClearMotions()
  
      _attackTracker[closestMob.Id] = _attackTracker[closestMob.Id] and (_attackTracker[closestMob.Id]) or 1
    end
  else
    -- run to mob
    self._combatStatus = "Running to " .. tostring(closestMob)
    local opts = ActionOptions.new()
    opts.TimeoutMilliseconds = 1000
    local res = coroutine.yield(apex.AutoNav.NavToObject(closestMob.Id, opts))
    if not res.Success then
      _attackTracker[closestMob.Id] = _attackTracker[closestMob.Id] and (_attackTracker[closestMob.Id]) or 1
    end
  end
end

function DefaultCombatStrategy:RenderStatus()
  -- render a status ui, shown on main plugin window
  ImGui.Text(self._combatStatus)
end

function DefaultCombatStrategy:RenderSettings()
  ImGui.Text("no combat settings...")
end

apex.Meta.RegisterStrategy("Combat", DefaultCombatStrategy)