﻿using Decal.Adapter;
using Apex.Lib.Autonav;
using Apex.Lib;
using ProtoBuf.Meta;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Runtime.InteropServices;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX;
using Apex.Tools;
using UtilityBelt.Scripting.Lib.ScriptTypes;
using Apex.Lib.Meta;
using Apex.Lib.Settings;

namespace Apex {
    /// <summary>
    /// This is the main plugin class. When your plugin is loaded, Startup() is called, and when it's unloaded Shutdown() is called.
    /// </summary>
    [FriendlyName("Apex")]
    public class PluginCore : PluginBase {
        private static string _assemblyDirectory = null;
        private ExampleUI ui;

        internal static Guid IID_IDirect3DDevice9 = new Guid("{D0223B96-BF7A-43fd-92BD-A43B0D82B9EB}");
        internal IntPtr unmanagedD3dPtr;

        internal static Device D3Ddevice { get; private set; }
        public Settings Settings { get; private set; }
        internal Picker Picker { get; private set; }
        internal CameraH CameraH { get; private set; }
        internal AutoNav Nav { get; private set; }
        public MetaManager MetaManager { get; private set; } = new MetaManager();

        internal List<ITool> Tools = new List<ITool>();
        internal UtilityBelt.Scripting.Interop.Game Game = new UtilityBelt.Scripting.Interop.Game();
        private DateTime _lastSaveCheck = DateTime.MinValue;
        private bool _inFocus;

        public static PluginCore Instance { get; private set; }

        /// <summary>
        /// Assembly directory containing the plugin dll
        /// </summary>
        public static string AssemblyDirectory {
            get {
                if (_assemblyDirectory == null) {
                    try {
                        _assemblyDirectory = System.IO.Path.GetDirectoryName(typeof(PluginCore).Assembly.Location);
                    }
                    catch {
                        _assemblyDirectory = Environment.CurrentDirectory;
                    }
                }
                return _assemblyDirectory;
            }
            set {
                _assemblyDirectory = value;
            }
        }

        /// <summary>
        /// Called when your plugin is first loaded.
        /// </summary>
        protected override void Startup() {
            try {
                if (CoreManager.Current.CharacterFilter.LoginStatus == 3) {
                    Init();
                    ScriptableTypes.ReloadAssemblyScriptTypes(GetType().Assembly, System.IO.Path.Combine(PluginCore.AssemblyDirectory, "Apex.ScriptTypes.json"));
                }
                else {
                    // subscribe to CharacterFilter_LoginComplete event, make sure to unscribe later.
                    // note: if the plugin was reloaded while ingame, this event will never trigger on the newly reloaded instance.
                    CoreManager.Current.CharacterFilter.Login += CharacterFilter_Login;
                    ScriptableTypes.TryLoadAssemblyScriptTypes(GetType().Assembly, System.IO.Path.Combine(PluginCore.AssemblyDirectory, "Apex.ScriptTypes.json"));
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private unsafe void Init() {
            try {
                Instance = this;
                object a = CoreManager.Current.Decal.Underlying.GetD3DDevice(ref IID_IDirect3DDevice9);
                Marshal.QueryInterface(Marshal.GetIUnknownForObject(a), ref IID_IDirect3DDevice9, out unmanagedD3dPtr);
                D3Ddevice = new Device(unmanagedD3dPtr);

                Settings = new Settings();
                Settings.Load();

                ui = new ExampleUI(this);
                Picker = new Picker();
                CameraH = new CameraH();
                Nav = new AutoNav();

                Tools.Add(new RenderTool());
                Tools.Add(new VisibleCellsTool());
                Tools.Add(new PickerTool());
                Tools.Add(new DragDropTool());
                Tools.Add(new ClientUITool());
                Tools.Add(new TransformTool());
                Tools.Add(new DatTool());
                Tools.Add(new PropTool());
                Tools.Add(new NavTool());

                CoreManager.Current.RenderFrame += Current_RenderFrame;

                if (CoreManager.Current.CharacterFilter.LoginStatus == 3) {
                    MetaManager.Init();
                }
                else {
                    CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
                }
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
                MetaManager.Init();
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                CameraH?.Update();
                MetaManager.Run();

                if (DateTime.UtcNow - _lastSaveCheck > TimeSpan.FromSeconds(5)) {
                    _lastSaveCheck = DateTime.UtcNow;
                    MetaManager?.Runner?.SaveStrategies();
                }

                ScriptApi.ApplyWantedMotions(true);
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        internal static void ResetDirectXState() {
            D3Ddevice.Transform.World = Matrix.Identity;

            // thanks paradox for proper dx state resetting
            D3Ddevice.RenderState.Lighting = false;
            D3Ddevice.Lights[0].Enabled = true;
            D3Ddevice.Lights[0].Type = LightType.Directional;
            D3Ddevice.Lights[0].Attenuation0 = 5f;
            D3Ddevice.Lights[0].XPosition = 80;
            D3Ddevice.Lights[0].YPosition = 100;
            D3Ddevice.Lights[0].ZPosition = 19;
            D3Ddevice.Lights[0].XDirection = 1;
            D3Ddevice.Lights[0].YDirection = 1;
            D3Ddevice.Lights[0].ZDirection = -1;
            D3Ddevice.RenderState.FogEnable = false;
            /*
            _plugin.D3Ddevice.RenderState.FogStart = 0;
            _plugin.D3Ddevice.RenderState.FogEnd = 120;
            _plugin.D3Ddevice.RenderState.FogColor = Color.FromArgb(255, 0, 20, 20);  
            _plugin.D3Ddevice.RenderState.FogDensity = 0.1f;
            */

            D3Ddevice.RenderState.ZBufferEnable = true;
            D3Ddevice.RenderState.ZBufferWriteEnable = true;
            D3Ddevice.RenderState.ZBufferFunction = Compare.LessEqual;

            D3Ddevice.RenderState.MultiSampleAntiAlias = true;
            D3Ddevice.RenderState.AntiAliasedLineEnable = true;

            D3Ddevice.RenderState.CullMode = Cull.CounterClockwise;

            D3Ddevice.RenderState.DiffuseMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.AmbientMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.SpecularMaterialSource = ColorSource.Material;
            D3Ddevice.RenderState.EmissiveMaterialSource = ColorSource.Material;

            D3Ddevice.RenderState.AlphaBlendEnable = true;
            D3Ddevice.RenderState.SourceBlend = Blend.SourceAlpha;
            D3Ddevice.RenderState.DestinationBlend = Blend.InvSourceAlpha;
            D3Ddevice.RenderState.BlendOperation = BlendOperation.Add;
        }

        /// <summary>
        /// CharacterFilter_LoginComplete event handler.
        /// </summary>
        private void CharacterFilter_Login(object sender, EventArgs e) {
            try {
                Init();
                CoreManager.Current.CharacterFilter.Login -= CharacterFilter_Login;
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        /// <summary>
        /// Called when your plugin is unloaded. Either when logging out, closing the client, or hot reloading.
        /// </summary>
        protected override void Shutdown() {
            try {
                // make sure to unsubscribe from any events we were subscribed to. Not doing so
                // can cause the old plugin to stay loaded between hot reloads.
                CoreManager.Current.CharacterFilter.Login -= CharacterFilter_Login;
                CoreManager.Current.RenderFrame -= Current_RenderFrame;

                // clean up our ui view
                ui?.Dispose();

                foreach (var tool in Tools) {
                    try {
                        tool?.Dispose();
                    }
                    catch { }
                }

                MetaManager?.Dispose();
            }
            catch (Exception ex) {
                Log(ex);
            }
        }

        #region logging
        /// <summary>
        /// Log an exception to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="ex"></param>
        internal static void Log(Exception ex) {
            Log(ex.ToString());
        }

        /// <summary>
        /// Log a string to log.txt in the same directory as the plugin.
        /// </summary>
        /// <param name="message"></param>
        internal static void Log(string message) {
            try {
                File.AppendAllText(System.IO.Path.Combine(AssemblyDirectory, "log.txt"), $"{message}\n");

                CoreManager.Current.Actions.AddChatText(message, 1);
            }
            catch { }
        }
        #endregion // logging
    }
}
