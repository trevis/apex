﻿using Apex.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.ScriptModules {
    /// <summary>
    /// Provides access to client physics
    /// </summary>
    public class PhysicsModule {
        /// <inheritdoc cref="ScriptApi.GetPhysicsCoordinates(uint)"/>
        unsafe public Coordinates GetPhysicsCoordinates(uint objectId) {
            return ScriptApi.GetPhysicsCoordinates(objectId);
        }
    }
}
