﻿using AcClient;
using Apex.Lib;
using Apex.Lib.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.ScriptModules {
    /// <summary>
    /// Provides access to raw player movement
    /// </summary>
    public class MovementModule {
        /// <summary>
        /// Get / set the players forward motion
        /// </summary>
        public ForwardMotion ForwardMotion {
            get => ScriptApi.GetForwardMotion();
            set => ScriptApi.SetForwardMotion(value);
        }

        /// <summary>
        /// Get / set the players side step motion
        /// </summary>
        public SideStepMotion SideStepMotion {
            get => ScriptApi.GetSideStepMotion();
            set => ScriptApi.SetSideStepMotion(value);
        }

        /// <summary>
        /// Get / set the players turn to motion
        /// </summary>
        public TurnToMotion TurnToMotion {
            get => ScriptApi.GetTurnToMotion();
            set => ScriptApi.SetTurnToMotion(value);
        }

        /// <summary>
        /// Get / set the players heading
        /// </summary>
        public float Heading {
            get => ScriptApi.GetHeading();
            set => ScriptApi.SetHeading(value);
        }

        /// <summary>
        /// Get / set whether or not the player is walking
        /// </summary>
        public bool Walk {
            get => ScriptApi.GetWalking();
            set => ScriptApi.SetWalking(value);
        }

        /// <inheritdoc cref="ScriptApi.SetHeadingTo(Coordinates)"/>
        public void SetHeadingTo(Coordinates destination) {
            ScriptApi.SetHeadingTo(destination);
        }

        /// <inheritdoc cref="ScriptApi.SetMotionRaw(uint)"/>
        public void SetMotionRaw(uint motionId) {
            ScriptApi.SetMotionRaw(motionId);
        }

        /// <inheritdoc cref="ScriptApi.ClearMotions()"/>
        public void ClearMotions() {
            ScriptApi.ClearMotions();
        }

        /// <inheritdoc cref="ScriptApi.SetAutoRun(bool)"/>
        public void SetAutoRun(bool enable) {
            ScriptApi.SetAutoRun(enable);
        }
    }
}
