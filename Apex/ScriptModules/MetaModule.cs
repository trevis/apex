﻿using Apex.Lib.Meta;
using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace Apex.ScriptModules {
    public class MetaModule : IDisposable {
        public class StrategyRef {
            public Table Strategy { get; set; }
            public string Name { get; set; }
        }

        public List<StrategyRef> RegisteredStrategies = new List<StrategyRef>();

        public string Test => "6";

        public MetaModule() {
            CoreManager.Current.Actions.AddChatText($"Creating new MetaModule: {Test}", 1);
            if (!PluginCore.Instance.MetaManager.DidInit) {
                PluginCore.Instance.MetaManager.OnInit += OnMetaManagerInit;
            }
        }

        private void OnMetaManagerInit(object sender, EventArgs e) {
            foreach (var strategy in RegisteredStrategies) {
                PluginCore.Instance.MetaManager.RegisterStrategy(strategy.Name, strategy.Strategy);
            }
        }

        /// <summary> 
        /// Log a message to the chat window
        /// </summary>
        /// <param name="message">The message to log</param>
        /// <param name="color">The chat color to use</param>
        public void Log(string message, int color) {
            CoreManager.Current.Actions.AddChatText(message, color);
        }

        /// <summary>
        /// Register a custom strategy
        /// </summary>
        /// <param name="name">The unique name of the strategy</param>
        /// <param name="strategy">Strategy implementation</param>
        public void RegisterStrategy(string name, Table strategy) {
            RegisteredStrategies.Add(new StrategyRef { Name = name, Strategy = strategy });
            if (PluginCore.Instance.MetaManager.DidInit) {
                PluginCore.Instance.MetaManager.RegisterStrategy(name, strategy);
            }
        }

        /// <summary>
        /// Unregister a custom strategy
        /// </summary>
        /// <param name="name">The name of the strategy</param>
        public void UnregisterStrategy(string name) {
            RegisteredStrategies.RemoveAll(x => x.Name == name);
            PluginCore.Instance.MetaManager.UnregisterStrategy(name);
        }

        public void Dispose() {
            var strats = RegisteredStrategies.ToList();
            foreach (var strategy in strats) {
                UnregisterStrategy(strategy.Name);
            }
        }
    }
}
