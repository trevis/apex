﻿using AcClient;
using ACE.Entity;
using Apex.Lib;
using Apex.Lib.Extensions;
using Decal.Adapter;
using DotRecast.Core.Numerics;
using DotRecast.Detour;
using DotRecast.Recast.Toolset.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.ScriptModules {
    /// <summary>
    /// AutoNavModule
    /// </summary>
    public class AutoNavModule : IDisposable {
        /// <summary>
        /// Checks if the navmesh is loaded
        /// </summary>
        public bool HasNavMeshLoaded => PluginCore.Instance?.Nav?.Mesh is not null;

        /// <summary>
        /// pathfind to the destination coordinates
        /// </summary>
        /// <param name="coordinates">The coordinates to run to</param>
        /// <param name="accuracy">The accuracy in units to allow, when detecting finished.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public NavToAction NavTo(Coordinates coordinates, double accuracy = 2, ActionOptions options = null, Action<NavToAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new NavToAction(coordinates, accuracy, options), callback);
        }

        /// <summary>
        /// pathfind to the destination object
        /// </summary>
        /// <param name="objectId">The objectId to run to</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public NavToObjectAction NavToObject(uint objectId, ActionOptions options = null, Action<NavToObjectAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new NavToObjectAction(objectId, options), callback);
        }

        /// <summary>
        /// Get a random point on the navmesh
        /// </summary>
        /// <returns></returns>
        public Coordinates GetRandomCoordinates() {
            return PluginCore.Instance?.Nav?.GetRandomPointOnMesh();
        }

        /// <summary>
        /// Find a route to the specified destination coordinates.
        /// </summary>
        /// <returns>null if no route found.</returns> 
        public List<Coordinates> FindRoute(Coordinates destination) {
            return PluginCore.Instance?.Nav?.FindRoute(destination);
        }

        public void Dispose() {
            
        }
    }
}
