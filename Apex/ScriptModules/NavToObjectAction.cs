﻿using Decal.Adapter;
using Microsoft.DirectX.Direct3D;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service;
using AcClient;
using Apex.Tools;
using System.IO;
using Microsoft.Extensions.Logging;
using ACE.Entity;
using ACE.Entity.Models;
using Apex.Lib.Enums;
using Apex.Lib;

namespace Apex.ScriptModules {


    /// <summary>
    /// Run your character the the specified object, using auto nav.
    /// </summary>
    public class NavToObjectAction : QueueAction {
        /// <summary>
        /// The Object id to run to
        /// </summary>
        public uint ObjectId { get; }

        private DateTime _lastExecuted;
        private DateTime _lastWiggle = DateTime.UtcNow;
        private SideStepMotion _motion;
        private DateTime _lastPositionCheck = DateTime.UtcNow;
        private Coordinates _lastPosition = Coordinates.Me;
        private bool _hasMotion;
        private int _idx;

        public NavToObjectAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        public override ActionType ActionType => ActionType.Navigation;

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 30000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 1;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }

            if (PluginCore.Instance?.Nav?.Mesh is null) {
                SetPermanentResult(ActionError.TimedOut);
                return false;
            }

            return true;
        }

        protected unsafe override void Start() {
            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        protected unsafe override void Stop() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
            ScriptApi.ClearMotions();
        }

        unsafe protected override bool Execute() {
            if (UBService.IsInGame) {
                var physObj = CPhysicsObj.GetObjectA(ObjectId);

                if (physObj is null) {
                    SetPermanentResult(ActionError.InvalidTargetObject);
                    return false;
                }

                var coords = new Coordinates(physObj->m_position.objcell_id, physObj->m_position.frame.m_fOrigin.x, physObj->m_position.frame.m_fOrigin.y, physObj->m_position.frame.m_fOrigin.z);

                var path = PluginCore.Instance?.Nav?.FindRoute(coords);

                if (path is not null && path.Count > 1) {

                    if (path.Count <= 2) {
                        SetPermanentResult(ActionError.None);
                        return false;
                    }

                    var next = path[1];
                    var me = Coordinates.Me;
                    if (me.DistanceTo(next) < 0.5f) {
                        path = path.Skip(1).ToList();
                    }
                    else {
                        var offset = me.HeadingTo(next);
                        if (Math.Abs(offset - CoreManager.Current.Actions.Heading) > 1) {
                            var physObj2 = *CPhysicsObj.player_object;
                            physObj2->set_heading(offset, 1);
                        }
                        ScriptApi.SetForwardMotion(ForwardMotion.Forward);
                    }
                }
                else {
                    SetPermanentResult(ActionError.TimedOut);
                    return false;
                }

                return true;
            }
            return false;
        }

        private unsafe void Finish() {
            SetPermanentResult(ActionError.None);
        }

        private unsafe void Current_RenderFrame(object sender, EventArgs e) {
            try {
                var me = Coordinates.Me;
                Execute();

                if (_hasMotion) {
                    _hasMotion = false;
                    ScriptApi.SetSideStepMotion(SideStepMotion.None);
                }

                if (DateTime.UtcNow - _lastWiggle > TimeSpan.FromMilliseconds(600)) {
                    _lastWiggle = DateTime.UtcNow;
                    _motion = (_idx++ % 2 == 0) ? SideStepMotion.SideStepLeft : SideStepMotion.SideStepRight;
                    //ScriptApi.SetSideStepMotion(_motion);
                    _hasMotion = true;
                }

                if (_lastPosition.DistanceTo(me) > 4.5f) {
                    _lastPositionCheck = DateTime.UtcNow;
                    _lastPosition = me;
                }

                if (DateTime.UtcNow - _lastPositionCheck >= TimeSpan.FromMilliseconds((Options.TimeoutMilliseconds ?? 5000) - 100)) {
                    _lastPositionCheck = DateTime.UtcNow;
                    ScriptApi.SetAutoRun(true);
                }
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }
    }
}
