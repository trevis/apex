﻿using AcClient;
using ACE.DatLoader.FileTypes;
using ACE.Entity;
using Apex.Tools;
using Apex;
using Decal.Adapter;
using Decal.Interop.D3DService;
using ImGuiNET;
using JeremyAnsel.Media.WavefrontObj;
using Microsoft.DirectX.Direct3D;
using Apex.Lib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service;
using UtilityBelt.Service.Views;
using UtilityBelt.Service.Lib.ACClientModule;
using Microsoft.Extensions.Logging;
using UtilityBelt.Scripting.Enums;
using Apex.Lib.Enums;
using WattleScript.Interpreter;

namespace Apex {
    internal unsafe class ExampleUI : IDisposable {
        private readonly PluginCore _plugin;
        private bool _didNavResize = false;
        private bool _on;

        /// <summary>
        /// The UBService Hud
        /// </summary>
        private readonly Hud hud;

        public ExampleUI(PluginCore plugin) {
            _plugin = plugin;
            // Create a new UBService Hud
            hud = UBService.Huds.CreateHud("Apex");

            // set to show our icon in the UBService HudBar
            hud.ShowInBar = true;

            // subscribe to the hud render event so we can draw some controls
            hud.OnPreRender += Hud_OnPreRender;
            hud.OnRender += Hud_OnRender;

            hud.Visible = true;
            hud.WindowSettings |= ImGuiWindowFlags.AlwaysAutoResize;
        }

        private void Hud_OnPreRender(object sender, EventArgs e) {
            ImGui.SetNextWindowPos(_plugin.Settings.WindowPosition, ImGuiCond.Once);

            hud.Title = $"Apex: [Strategy: {_plugin.MetaManager.Runner?.CurrentStrategy?.Name ?? "None"}]";
        }

        /// <summary>
        /// Called every time the ui is redrawing.
        /// </summary>
        private unsafe void Hud_OnRender(object sender, EventArgs e) {
            try {
                _plugin.Settings.WindowPosition = ImGui.GetWindowPos();
                var nav = _plugin.Tools.FirstOrDefault(t => t.Name == "Nav");
                if (nav is null) {
                    return;
                }
                nav.IsActive = true;
                nav.Render();
                _plugin.MetaManager.RenderImGui();
            }
            catch (Exception ex) {
                PluginCore.Log(ex);
            }
        }

        public void Dispose() {
            hud.Dispose();
        }
    }
}