﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Service;

namespace Apex.Tools {
    public unsafe class DatTool : ITool {
        private bool wireframe;
        private bool fogDisabled;
        private DBObj* _obj;

        public string Name => "Dats";
        public bool IsActive { get; set; }

        public DatTool() {

        }
        public uint ImgTex_get_width(ImgTex* This) {
            return ((delegate* unmanaged[Thiscall]<ImgTex*, uint>)0x0053F1F0)(This);
        }
        //.text:0053F1F0 ; int __thiscall ImgTex::get_width(ImgTex *this)

        public int ImgTex_GetD3DTexture(ImgTex* This) {
            return ((delegate* unmanaged[Thiscall]<ImgTex*, int>)0x0053F310)(This);
        }
        //.text:0053F310 ; int __thiscall ImgTex::GetD3DTexture(ImgTex *this)

        public PixelFormatID ImgTex_get_image_type(ImgTex* This) {
            return ((delegate* unmanaged[Thiscall]<ImgTex*, PixelFormatID>)0x00536BC0)(This);
        }
        //.text:00536BC0 ; int __thiscall ImgTex::get_image_type(_DWORD *this)

        private uint _lr = 1;

        public void Render() {

            var currentRegion = _lr;
            if (ImGui.Button($"Swith to {(currentRegion == 1 ? "MegaDat" : "EOR")}")) {
                /*
                var qid = new QualifiedDataID() {
                    ID = 0x0600127Du,
                    Type = 12u
                };
                CoreManager.Current.Actions.AddChatText($"Get QID: {qid}", 1);
                // const unsigned int DB_TYPE_RENDERSURFACE = 12u; // idb
                _obj = DBCache.Get(0x0600127Du, 12u);
                //_obj = DBObj.Get(&qid);
                if (_obj is not null) {
                    var h = new PreprocHeader();
                    try {
                        var rs = (RenderSurface*)_obj;
                        CoreManager.Current.Actions.AddChatText($"saved... {rs->sourceData.imageSize} vs {rs->sourceData.width * rs->sourceData.height * (int)rs->m_pSurfaceBits}", 1);
                        using (var bmp = new Bitmap(@"C:\Turbine\Asheron's Call\0600127D.png")) {
                            var buf = new char[rs->sourceData.width * rs->sourceData.height * (int)rs->m_pSurfaceBits];
                            for (int i = 0; i < bmp.Height; i++) {
                                for (int j = 0; j < bmp.Width; j++) {
                                    var p = bmp.GetPixel(j, i);
                                    buf[(i * j * 3) + 0] = (char)p.B;
                                    buf[(i * j * 3) + 1] = (char)p.G;
                                    buf[(i * j * 3) + 2] = (char)p.R;
                                }
                            }
                            fixed (char* _c = buf) {
                                //rs->sourceData.sourceBits = _c;
                                
                                rs->dBObj.SaveToDisk(&h);
                                rs->dBObj.ReloadFromDisk();
                            }
                        }
                        CoreManager.Current.Actions.AddChatText($"saved...", 1);
                    }
                    catch (Exception ex) { UBService.LogException(ex); }
                }
                */

                var nr = (uint)(_lr == 1 ? 2 : 1);
                _lr = nr;

                var cache = (CLCache*)AcClient.Client.m_instance[0]->m_pDBCache;
                cache->SetRegion(nr);
                AcClient.SmartBox.smartbox[0]->SetRegion(nr);
                //var r2 = DBCache.s_pCache[0]->SetRegion(nr);
            }

            if (_obj is not null) {
                ImGui.Text($"Obj: {_obj->m_DID:X8} L:{_obj->m_timeStamp}");
                var imTex = (RenderSurface*)_obj;
                ImGui.Text($"imTex: {imTex->graphicsResource.m_TimeUsed} /  {imTex->graphicsResource.m_FrameUsed}");
                ImGui.Text($"s: {(int)imTex->sourceData.imageSize}");
                //ImGui.Image((IntPtr)imTex->m_pD3DTexture, new System.Numerics.Vector2(512, 512));
            }
            else {

                ImGui.Text($"Obj: null");
            }
        }

        public void Dispose() {

        }
    }
}
