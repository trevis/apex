﻿using AcClient;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using Apex.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.ACClientModule;

namespace Apex.Tools {
    public unsafe class PickerTool : ITool {
        public string Name => "Picker";
        public bool IsActive { get; set; }

        private ManagedMesh _mesh;
        private D3DObj _arrow;

        public PickerTool() {
            //_mesh = ObjMeshLoader.FromFile(System.IO.Path.Combine(PluginCore.AssemblyDirectory, "Resources", $"cube.obj"));

            //CoreManager.Current.RenderFrame += Current_RenderFrame;

            unchecked { 
                _arrow = CoreManager.Current.D3DService.MarkObjectWithShape(0, D3DShape.VerticalArrow, (int)0xFFFF00FF);
            }
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            if (!IsActive && PluginCore.Instance.Tools.FirstOrDefault(t => t is VisibleCellsTool)?.IsActive != true) {
                return;
            }
            try {
                var pickedWorldObject = PluginCore.Instance.Picker.PickWorldObject();
                if (pickedWorldObject != null) {
                    var physicsObj = (CPhysicsObj*)CoreManager.Current.Actions.PhysicsObject((int)pickedWorldObject.Id);
                    if (physicsObj != null) {
                        _arrow.Anchor((int)pickedWorldObject.Id, 0.2f, 0, 0, physicsObj->GetHeight() * physicsObj->m_scale);
                    }
                }
                else {
                    _arrow.Anchor(0, 0, 0, 0, 0);
                }

                var pickedCoords = PluginCore.Instance.Picker.PickTerrain();
                if (pickedCoords is not null) {
                    using (var stateBlock = new StateBlock(PluginCore.D3Ddevice, StateBlockType.All)) {
                        stateBlock.Capture();

                        PluginCore.ResetDirectXState();

                        PluginCore.D3Ddevice.RenderState.CullMode = Cull.Clockwise;

                        var cf = SmartBox.smartbox[0]->viewer.frame.m_fOrigin;
                        var camera = new Coordinates(SmartBox.smartbox[0]->viewer.objcell_id, cf.x, cf.y, cf.z);
                        var cameraLBX = (int)((camera.LandCell >> 24) & 0xFF);
                        var cameraLBY = (int)((camera.LandCell >> 16) & 0xFF);

                        var pickedLBX = (int)((pickedCoords.LandCell >> 24) & 0xFF);
                        var pickedLBY = (int)((pickedCoords.LandCell >> 16) & 0xFF);

                        var scale = 0.2f;
                        var pos = new Microsoft.DirectX.Vector3(
                            pickedCoords.LocalX + (pickedLBX - cameraLBX) * 192f,
                            pickedCoords.LocalZ,
                            pickedCoords.LocalY + (pickedLBY - cameraLBY) * 192f
                        );

                        var rot = Microsoft.DirectX.Quaternion.Identity;
                        rot.RotateAxis(new Microsoft.DirectX.Vector3(0, 1, 1), (float)Math.PI);
                        var world = Microsoft.DirectX.Matrix.Transformation(new Microsoft.DirectX.Vector3(0, 0, 0), Microsoft.DirectX.Quaternion.Identity, new Microsoft.DirectX.Vector3(scale, scale, scale), new Microsoft.DirectX.Vector3(0, 0, 0), rot, pos);
                        PluginCore.D3Ddevice.Transform.World = world;

                        _mesh.Render(PluginCore.D3Ddevice);

                        stateBlock.Apply();
                    }
                    return;
                }
            }
            catch (Exception ex) {
                UBService.LogException(ex);
            }
        }

        public void Render() {
            ImGui.Text($"Cursor: {*AcClient.Render.selection_x}, {*AcClient.Render.selection_y}");

            var pickedWorldObject = PluginCore.Instance.Picker.PickWorldObject();
            ImGui.Text($"Mouse Hover Object: {(pickedWorldObject is null ? "none" : pickedWorldObject)}");

            var pickedCoords = PluginCore.Instance.Picker.PickTerrain();
            ImGui.Text($"Mouse Hover Coords: {(pickedCoords is null ? "none" : pickedCoords)}");
        }

        public void Dispose() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
            _mesh.Dispose();
        }
    }
}
