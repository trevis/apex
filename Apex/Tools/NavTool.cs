﻿using AcClient;
using Decal.Adapter;
using DotRecast.Core.Numerics;
using ImGuiNET;
using Apex.Lib.Autonav;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotRecast.Detour;
using DotRecast.Recast.Toolset.Tools;
using UtilityBelt.Service.Lib.ACClientModule;
using System.Diagnostics;
using UtilityBelt.Service.Views;
using UtilityBelt.Service;
using Apex.Lib;
using Apex;
using UtilityBelt.Scripting.Events;
using Decal.Adapter.Wrappers;
using Microsoft.DirectX.Direct3D;
using System.Drawing;

namespace Apex.Tools {
    public class NavTool : ITool {
        internal static List<RcVec3f> _path;
        private bool _doRenderMesh = false;
        private NavMeshRenderer _navMeshRenderer;
        private D3DObj _arrow;
        private D3DObj _arrow2;
        private Coordinates _pickedCoords;
        private bool wireframe;
        public static ACMovement Movement;
        private bool _needsStop;

        private AutoNav Nav => PluginCore.Instance.Nav;

        public string Name => "Nav";
        public bool IsActive { get; set; } = true;

        public uint Landblock { get; private set; }

        public NavTool() {
            PluginCore.Instance.Game.OnRender3D += Game_OnRender3D;

            unchecked {
                _arrow = CoreManager.Current.D3DService.MarkObjectWithShape(0, D3DShape.VerticalArrow, (int)0xFFFF0000);
                _arrow2 = CoreManager.Current.D3DService.MarkObjectWithShape(0, D3DShape.VerticalArrow, (int)0xFF00FF00);
            }

            Movement ??= new ACMovement();
        }

        unsafe private void Game_OnRender3D(object sender, EventArgs e) {
            if (Nav is not null) {
                Nav.Update();
            }

            var landblock = (uint)CoreManager.Current.Actions.Landcell & 0xFFFF0000;
            if (landblock != Landblock) {
                Landblock = landblock;
                UpdateLandblock(landblock);
            }

            //PluginCore.ResetDirectXState();

            if (_navMeshRenderer is not null) {
                _navMeshRenderer.Render();
            }
            return;


            if (ImGui.IsKeyDown(ImGuiKey.LeftCtrl)) {
                _pickedCoords = PluginCore.Instance.Picker.PickTerrain();
                if (_pickedCoords is not null && Nav?.Mesh is not null) {
                    var query = new DtNavMeshQuery(Nav.Mesh);
                    unchecked {
                        var m_filter = new DtQueryDefaultFilter();
                        var halfExtents = new RcVec3f(1.2f, 1.2f, 1.2f);

                        var pos = new RcVec3f(_pickedCoords.LocalX, _pickedCoords.LocalZ, _pickedCoords.LocalY);
                        var startStatus = query.FindNearestPoly(pos, halfExtents, m_filter, out long startRef, out var startPt, out bool isStartOverPoly);

                        if (startStatus.Succeeded() && startRef != 0) {
                            if (isStartOverPoly) {
                                _arrow.Color = Color.FromArgb(255, 0, 255, 0).ToArgb();
                            }
                            else {
                                _arrow.Color = Color.FromArgb(255, 255, 255, 0).ToArgb();
                            }
                        }
                        else {
                            _arrow.Color = Color.FromArgb(255, 255, 0, 0).ToArgb();
                        }

                        _arrow.Anchor(_pickedCoords.NS, _pickedCoords.EW, _pickedCoords.Z);
                    }
                }
                else {
                    _arrow.Anchor(0, 0, 0, 0, 0);
                }
            }
            else {
                _arrow.Anchor(0, 0, 0, 0, 0);
            }

            DoNav();
        }

        private unsafe void DoNav() {
            return;
            if (_path is null) return;

            if (_path.Count > 1) {
                _needsStop = true;
                var next = _path[1];
                var me = Coordinates.Me;
                var coords = new Coordinates(me.LandCell, next.X, next.Z, next.Y);
                if (me.DistanceTo(coords) < 0.5f) {
                    _path = _path.Skip(1).ToList();
                }
                else {
                    var offset = me.HeadingTo(coords);
                    var physObj = *CPhysicsObj.player_object;
                    physObj->set_heading(offset, 1);
                    Movement.SetMotion(ACMotion.Forward, true); 
                }
            }
            else if (_needsStop) {
                _needsStop = false;
                Movement.SetMotion(ACMotion.Forward, false);
                Movement.ClearAllMotions();
            }
        }

        public unsafe void Render() {
            var dl = ImGui.GetBackgroundDrawList();
            
            if (ImGui.Checkbox("Draw Mesh", ref _doRenderMesh)) {
                UpdateLandblock(SmartBox.smartbox[0]->viewer.objcell_id & 0xFFFF0000);
            }

            ImGui.SameLine();
            if (ImGui.Checkbox("Wireframe", ref wireframe)) {
                (RenderDevice.render_device[0])->m_WireframeMode = (byte)(wireframe ? 1 : 0);
            }

            if (Nav?.Mesh is not null && ImGui.IsMouseClicked(ImGuiMouseButton.Left) && ImGui.IsKeyDown(ImGuiKey.LeftCtrl) && _pickedCoords is not null) {
                var query = new DtNavMeshQuery(Nav.Mesh);
                unchecked {
                    var m_filter = new DtQueryDefaultFilter();
                    var halfExtents = new RcVec3f(1.2f, 1.2f, 1.2f);

                    var pos = new RcVec3f(_pickedCoords.LocalX, _pickedCoords.LocalZ, _pickedCoords.LocalY);
                    var startStatus = query.FindNearestPoly(pos, halfExtents, m_filter, out long startRef, out var startPt, out bool isStartOverPoly);

                    if (startStatus.Succeeded() && startRef != 0) {
                        if (isStartOverPoly) {
                            _arrow2.Color = Color.FromArgb(255, 0, 255, 0).ToArgb();
                        }
                        else {
                            _arrow2.Color = Color.FromArgb(255, 255, 255, 0).ToArgb();
                        }
                    }
                    else {
                        _arrow2.Color = Color.FromArgb(255, 255, 0, 0).ToArgb();
                    }

                    var coords = new Coordinates(_pickedCoords.LandCell, startPt.X, startPt.Z, startPt.Y);
                    _arrow2.Anchor(coords.NS, coords.EW, coords.Z);
                    CoreManager.Current.Actions.AddChatText($"Click at: {coords} ({startStatus.Succeeded()} / {isStartOverPoly} / {startRef})", 1);

                    FindPickedPath(_pickedCoords);
                }
            }

            //if (Nav.OutsideMesh is not null && ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left]) {
            //    FindPickedPath();
            //}

            return;
            if (_path is not null && _path.Count > 1) {
                var cameraPos = PluginCore.Instance.CameraH.Coordinates;
                var me = Coordinates.Me;
                dl.AddLine(
                    new System.Numerics.Vector3(me.LocalX, me.LocalY, me.LocalZ),
                    new System.Numerics.Vector3(_path[1].X, _path[1].Z, _path[1].Y),
                    0xFF00FF00, 3f);
                for (var i = 2; i < _path.Count; i++) {
                    dl.AddLine(
                        new System.Numerics.Vector3(_path[i - 1].X, _path[i - 1].Z, _path[i - 1].Y),
                        new System.Numerics.Vector3(_path[i].X, _path[i].Z, _path[i].Y),
                        0xFF00FF00, 3f);
                }
            }
        }

        private void UpdateLandblock(uint landblock) {
            if (landblock == 0) {
                _navMeshRenderer = null;
                return;
            }

            if (_navMeshRenderer is not null) {
                _navMeshRenderer.Dispose();
                _navMeshRenderer = null;
                return;
            }

            CoreManager.Current.Actions.AddChatText($"Starting update...", 1);

            Task.Run(async () => {
                try {
                    var sw = new System.Diagnostics.Stopwatch();
                    sw.Start();

                    await Nav.LoadMesh(Coordinates.Me, Coordinates.Me);
                    if (_doRenderMesh && Nav.Mesh is not null) {
                        _navMeshRenderer = new NavMeshRenderer(Nav.Mesh);
                    }

                    sw.Stop();
                    CoreManager.Current.Actions.AddChatText($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to generate navmesh for {Landblock:X8}", 1);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            });
        }

        private void FindPickedPath(Coordinates picked) {
            _path = null;
            if (picked is not null) {
                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                List<RcVec3f> pts = GetStitchedPath(Coordinates.Me, picked);
                if (pts.Count > 0) {
                    _path = pts;
                    sw.Stop();
                    CoreManager.Current.Actions.AddChatText($"Took {((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0:N2}ms to generate path with {pts.Count} points", 1);
                }
                else {
                    CoreManager.Current.Actions.AddChatText($"Failed to find path", 1);
                }
            }
        }

        private List<RcVec3f> GetStitchedPath(Coordinates start, Coordinates end, int tries = 10) {
            var rc = new RcTestNavMeshTool();

            var halfExtents = new RcVec3f(1.2f, 1.2f, 1f);

            var query = new DtNavMeshQuery(Nav.Mesh);
            var pts = new List<RcVec3f>();
            unchecked {
                var m_filter = new DtQueryDefaultFilter();

                var startStatus = query.FindNearestPoly(start.ToNavLocal(), halfExtents, m_filter, out long startRef, out var startPt, out bool isStartOverPoly);
                var endStatus = query.FindNearestPoly(end.ToNavLocal(), halfExtents, m_filter, out long endRef, out var endPt, out bool isEndOverPoly);

                var polys = new List<long>();

                var res = rc.FindFollowPath(Nav.Mesh, query, startRef, endRef, startPt, endPt, m_filter, false, ref polys, ref pts);

            }
            return pts ?? new List<RcVec3f>();
        }

        public void Dispose() {
            PluginCore.Instance.Game.OnRender3D -= Game_OnRender3D;

            _arrow?.Dispose();
            _arrow = null;
            _arrow2?.Dispose();
            _arrow2 = null;

            _navMeshRenderer?.Dispose();
            _navMeshRenderer = null;

            Nav?.Dispose();
        }
    }
}
