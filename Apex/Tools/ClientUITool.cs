﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Tools {
    public unsafe class ClientUITool : ITool {
        private bool wireframe;
        private bool fogDisabled;

        public string Name => "Client UI";
        public bool IsActive { get; set; }

        public ClientUITool() {

        }
        public unsafe char InqPropertyNameStringStatic(int _name_enum, int* _name_str) {
            return ((delegate* unmanaged[Cdecl]<int, int*, char>)0x004268D0)(_name_enum, _name_str);
        }
        public unsafe char InqPropertyGroupNameStringStatic(uint _group_name_enum, PStringBase<char>* _name_str) {
            return ((delegate* unmanaged[Cdecl]<uint, PStringBase<char>*, char>)0x00426910)(_group_name_enum, _name_str);
        }
        //.text:00426910 ; char __cdecl MasterProperty::InqPropertyGroupNameStringStatic(int, int *)
        //char __cdecl MasterProperty::InqPropertyGroupNameStringStatic(unsigned int _group_name_enum, PStringBase<char> *_name_str)

        public int GetFirstChildElement(ref UIElement This) => ((delegate* unmanaged[Thiscall]<ref UIElement, int>)0x00464110)(ref This);

        public int GetNextChildElement(ref UIElement This, int i_pPrevChild) {
            return ((delegate* unmanaged[Thiscall]<ref UIElement, int, int>)4605072)(ref This, i_pPrevChild);
        }

        public void Render() {
            var ui = UIElementManager.s_pInstance;
            

            ImGui.Text($"Element:");
            if (ui->m_pRootElement is not null) {
                var root = *ui->m_pRootElement;
                if (ImGui.TreeNodeEx("Root")) {
                    WriteElements(root,6);
                    ImGui.TreePop();
                }
            }
        }

        public unsafe static delegate* unmanaged[Thiscall]<PStringBase<char>*, void> __Ctor__ = (delegate* unmanaged[Thiscall]< PStringBase<char>*, void>)4201024;

        private void WriteElements(UIElement root, int maxDepth, int depth = 1) {
            var flags = ImGuiTreeNodeFlags.None | ImGuiTreeNodeFlags.OpenOnArrow | ImGuiTreeNodeFlags.OpenOnDoubleClick | ImGuiTreeNodeFlags.AllowOverlap | ImGuiTreeNodeFlags.DefaultOpen;

            var first = (UIElement*)GetFirstChildElement(ref root);
            while (first != null) {
                var t = first->m_desc.stateDesc.m_properties.m_hashProperties.hashTable.m_Int32rusiveTable;
                var x = t.m_numElements;
                if (x > 0 && ImGui.TreeNodeEx("Properties", flags)) {
                    for (var i = 0; i < t.m_numBuckets; i++) {
                        var bucket = t.m_buckets[i];
                        if (bucket is not null) {
                            var disp = $"{bucket->m_data.m_pcPropertyDesc->m_propertyType}";
                            try {
                                disp = $"{bucket->m_data.m_pcPropertyDesc->m_propertyType}: {bucket->m_data.m_pcPropertyDesc->m_propertyName} = {(int)bucket->m_data.m_pcPropertyValue->_ref.m_cRef}";
                            }
                            catch (Exception ex) { disp = $"{bucket->m_data.m_pcPropertyDesc->m_propertyType}: {ex}"; }
                            if (ImGui.TreeNodeEx(disp, ImGuiTreeNodeFlags.Leaf)) {
                                ImGui.TreePop();
                            }
                        }
                    }

                    ImGui.TreePop();
                }

                var el = *first;
                var hasChildren = (UIElement*)GetFirstChildElement(ref el) is not null;
                if (hasChildren && ImGui.TreeNodeEx($"Children", flags)) {
                    if (ImGui.TreeNodeEx($"{(int)first:X8} {x.ToString()}", flags)) {
                        if (depth <= maxDepth) {
                            WriteElements(el, maxDepth, depth + 1);
                        }
                        ImGui.TreePop();
                    }
                    ImGui.TreePop();
                }
                first = (UIElement*)GetNextChildElement(ref root, (int)first);
            }
        }

        public void Dispose() {

        }
    }
}
