﻿using AcClient;
using ACE.DatLoader.Entity;
using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using ImGuiNET;
using ImGuizmoNET;
using Microsoft.DirectX.Direct3D;
using Microsoft.DirectX.PrivateImplementationDetails;
using Apex.Lib;
using Apex.Lib.Extensions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Service.Lib.ACClientModule;
using Plane = AcClient.Plane;
using Vector3 = System.Numerics.Vector3;

namespace Apex.Tools {
    public unsafe class VisibleCellsTool : ITool {
        private bool wireframe;
        private bool fogDisabled;
        private int visibleCellsZoom = 14;
        private CameraH _camera => PluginCore.Instance.CameraH;

        public string Name => "VisibleCells";
        public bool IsActive { get; set; }

        private bool draw3d = true;

        public VisibleCellsTool() {

        }

        internal static unsafe CEnvCell* CEnvCell_GetVisible(uint cellId) {
            return ((delegate* unmanaged[Cdecl]<uint, CEnvCell*>)0x0052E870)(cellId);
        }
        //.text:0052E870 ; int __cdecl CEnvCell::GetVisible(unsigned int)

        public void Render() {
            if ((SmartBox.smartbox[0]->viewer_cell->pos.objcell_id & 0xFFFF) >= 0x100) {
                RenderInside();
                if (draw3d) {
                    RenderInside3D();
                }
            }
            else {
                RenderLandscape();
            }
        }

        private void RenderInside() {
            ImGui.Checkbox("Render 3d", ref draw3d);

            ImGui.SliderInt("Zoom", ref visibleCellsZoom, 1, 100);

            var sx = Math.Max(400, ImGui.GetContentRegionAvail().X);
            var s = Math.Min(sx, Math.Max(sx, ImGui.GetContentRegionAvail().Y));
            var size = new Vector2(s);
            var cursor = ImGui.GetCursorPos();
            ImGui.InvisibleButton("canvas", size);
            ImGui.SetCursorPos(cursor);
            cursor += ImGui.GetWindowPos();

            var bSize = new Vector2(size.X / visibleCellsZoom * 2f, size.Y / visibleCellsZoom * 2f);

            var dlBg = ImGui.GetBackgroundDrawList();
            var dl = ImGui.GetForegroundDrawList();
            dl.PushClipRect(cursor, cursor + size);
            var mid = cursor + (size / 2f);

            var cEnvCell = CEnvCell_GetVisible(SmartBox.smartbox[0]->viewer_cell->pos.objcell_id);
            DrawEnvCell2D(dl, mid, cEnvCell);

            for (var i = 0; i < cEnvCell->cObjCell.num_stabs; i++) {
                var cell = CEnvCell_GetVisible(cEnvCell->cObjCell.stab_list[i]);
                DrawEnvCell2D(dl, mid, cell);
            }

            // camera
            dl.AddCircleFilled(mid, 2, 0xFF00FFFF);
            // player
            var me = new Vector3(
                SmartBox.smartbox[0]->player->m_position.frame.m_fOrigin.x,
                SmartBox.smartbox[0]->player->m_position.frame.m_fOrigin.y,
                SmartBox.smartbox[0]->player->m_position.frame.m_fOrigin.z);
            var cameraPos = new Vector3(
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.x,
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.y,
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.z);
            var playerPos = cameraPos - me;
            dl.AddCircleFilled(mid + new Vector2(-playerPos.X, playerPos.Y) * visibleCellsZoom, 3, 0xFF00FF00);

            dl.PopClipRect();
            dl.AddRect(cursor, cursor + size, 0xFFAAAAAA, 3f, ImDrawFlags.None, 2f);
        }

        private void RenderInside3D() {
            var dl = ImGui.GetBackgroundDrawList();
            var cEnvCell = CEnvCell_GetVisible(SmartBox.smartbox[0]->viewer_cell->pos.objcell_id);
            DrawEnvCell3D(dl, cEnvCell);

            for (var i = 0; i < cEnvCell->cObjCell.num_stabs; i++) {
                var cell = CEnvCell_GetVisible(cEnvCell->cObjCell.stab_list[i]);
                DrawEnvCell3D(dl, cell);
            }
        }

        private void DrawEnvCell2D(ImDrawListPtr dl, Vector2 mid, CEnvCell* cEnvCell) {
            if (cEnvCell is null || cEnvCell->structure is null) return;
            var structure = cEnvCell->structure;

            uint color = 0xFFFF00FF;
            float thickness = 1.0f;
            var cellMat = cEnvCell->cObjCell.pos.ToMatrix();

            var cameraPos = new Vector3(
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.x,
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.y,
                SmartBox.smartbox[0]->viewer.frame.m_fOrigin.z);

            for (var i = 0; i < structure->num_physics_polygons; i++) {
                var poly = structure->physics_polygons[i];
                for (var j = 0; j < poly.num_pts; j++) {
                    var a = new Vector3(
                        structure->vertex_array.vertices[poly.vertex_ids[j]].x,
                        structure->vertex_array.vertices[poly.vertex_ids[j]].y,
                        structure->vertex_array.vertices[poly.vertex_ids[j]].z);
                    var idxb = j == 0 ? poly.num_pts - 1 : j - 1;
                    var b = new Vector3(
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].x,
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].y,
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].z);

                    var aa = cameraPos - Vector3.Transform(a, cellMat);
                    var bb = cameraPos - Vector3.Transform(b, cellMat);

                    var a2 = mid + new Vector2(-aa.X, aa.Y) * (float)visibleCellsZoom;
                    var b2 = mid + new Vector2(-bb.X, bb.Y) * (float)visibleCellsZoom;

                    dl.AddLine(a2, b2, color, thickness);
                }
            }
        }

        private void DrawEnvCell3D(ImDrawListPtr dl, CEnvCell* cEnvCell) {
            if (cEnvCell is null || cEnvCell->structure is null) return;
            var structure = cEnvCell->structure;


            uint color = 0xFFFF00FF;
            float thickness = 2.0f;
            var cellMat = cEnvCell->cObjCell.pos.ToMatrix();

            for (var i = 0; i < structure->num_physics_polygons; i++) {
                var poly = structure->physics_polygons[i];

                var aa = new Vector3(
                    structure->vertex_array.vertices[poly.vertex_ids[2]].x,
                    structure->vertex_array.vertices[poly.vertex_ids[2]].y,
                    structure->vertex_array.vertices[poly.vertex_ids[2]].z);
                var bb = new Vector3(
                    structure->vertex_array.vertices[poly.vertex_ids[1]].x,
                    structure->vertex_array.vertices[poly.vertex_ids[1]].y,
                    structure->vertex_array.vertices[poly.vertex_ids[1]].z);
                var cc = new Vector3(
                    structure->vertex_array.vertices[poly.vertex_ids[0]].x,
                    structure->vertex_array.vertices[poly.vertex_ids[0]].y,
                    structure->vertex_array.vertices[poly.vertex_ids[0]].z);

                if (CalculateTriSurfaceNormal(aa, bb, cc).Z > -0.66417414618662751f) {
                    continue;
                }

                for (var j = 0; j < poly.num_pts; j++) {
                    var a = new Vector3(
                        structure->vertex_array.vertices[poly.vertex_ids[j]].x,
                        structure->vertex_array.vertices[poly.vertex_ids[j]].y,
                        structure->vertex_array.vertices[poly.vertex_ids[j]].z);
                    var idxb = j == 0 ? poly.num_pts - 1 : j - 1;
                    var b = new Vector3(
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].x,
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].y,
                        structure->vertex_array.vertices[poly.vertex_ids[idxb]].z);

                    dl.AddLine(Vector3.Transform(a, cellMat), Vector3.Transform(b, cellMat), color, thickness);
                }
            }

            DrawStaticObjects(dl, cEnvCell);
        }

        private void DrawStaticObjects(ImDrawListPtr dl, CEnvCell* cEnvCell) {
            for (var i = 0; i < cEnvCell->num_static_objects; i++) {
                var sObj = cEnvCell->static_objects[i];

                for (var j = 0; j < sObj->part_array->num_parts; j++) {
                    var part = sObj->part_array->parts[j];
                    var cellMat = part->pos.ToMatrix();
                    for (var k = 0; k < part->gfxobj[0]->num_physics_polygons; k++) {
                        var poly = part->gfxobj[0]->physics_polygons[k];
                        for (var q = 0; q < poly.num_pts; q++) {
                            var a = new Vector3(
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].x,
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].y,
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].z);
                            var idxb = q == 0 ? poly.num_pts - 1 : q - 1;
                            var b = new Vector3(
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].x,
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].y,
                                part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].z);

                            dl.AddLine(Vector3.Transform(a, cellMat), Vector3.Transform(b, cellMat), 0xFF00FF00, 1f);
                        }
                    }
                }
            }
        }

        internal static Vector3 CalculateTriSurfaceNormal(Vector3 a, Vector3 b, Vector3 c) {
            Vector3 normal = new Vector3();
            Vector3 u = new Vector3();
            Vector3 v = new Vector3();

            u.X = b.X - a.X;
            u.Y = b.Y - a.Y;
            u.Z = b.Z - a.Z;

            v.X = c.X - a.X;
            v.Y = c.Y - a.Y;
            v.Z = c.Z - a.Z;

            normal.X = u.Y * v.Z - u.Z * v.Y;
            normal.Y = u.Z * v.X - u.X * v.Z;
            normal.Z = u.X * v.Y - u.Y * v.X;

            return Vector3.Normalize(normal);
        }

        private void RenderLandscape() {
            var ui = ClientUISystem.GetUISystem();
            var cs = ui->AccessCameraSet();
            var cm = cs->cm;
            var smartbox = cs->sbox;
            var lscape = smartbox->lscape;

            ImGui.Text($"mid_radius: {lscape->mid_radius} mid_width: {lscape->mid_width} loaded_cell_id: 0x{lscape->loaded_cell_id:X8}");

            ImGui.SliderInt("Zoom", ref visibleCellsZoom, 1, 100);

            var sx = Math.Max(400, ImGui.GetContentRegionAvail().X);
            var s = Math.Min(sx, Math.Max(sx, ImGui.GetContentRegionAvail().Y));
            var size = new Vector2(s);
            var cursor = ImGui.GetCursorPos();
            ImGui.InvisibleButton("canvas", size);
            ImGui.SetCursorPos(cursor);
            cursor += ImGui.GetWindowPos();

            var bSize = new Vector2(size.X / visibleCellsZoom * 2f, size.Y / visibleCellsZoom * 2f);

            var cLbX = (lscape->loaded_cell_id >> 24) & 0xFF;
            var cLbY = (lscape->loaded_cell_id >> 16) & 0xFF;

            var dl = ImGui.GetWindowDrawList();
            dl.PushClipRect(cursor, cursor + size);
            dl.AddRect(cursor, cursor + size, 0xFFFF00FF);


            for (var x = 0; x < lscape->mid_width; ++x) {
                for (var y = 0; y < lscape->mid_width; ++y) {
                    var lb = lscape->land_blocks[x + y * lscape->mid_width];
                    var lbX = (lb->block_coord.x / 8);
                    var lbY = (lb->block_coord.y / 8);
                    var lbid = (uint)((lbX << 24) + (lbY << 16));
                    var offset = bSize * new Vector2(lbX - cLbX, cLbY - lbY);
                    var tl = (cursor + size / 2f) + offset - bSize / 2f;
                    var visible = lb->in_view != BoundingType.OUTSIDE;
                    var isCurrent = lbid == (lscape->loaded_cell_id & 0xFFFF0000);
                    dl.AddRect(tl, tl + bSize, isCurrent ? 0xFFFF00FF : visible ? 0xFF00FF00 : 0x2400FF00, 0, ImDrawFlags.None, 1f);
                    //dl.AddText(tl + new Vector2(2, 0), visible ? 0xFFFFFFFF : 0x66FFFFFF, $"{(lbid >> 16):X4}");
                    if (true) {
                        for (int xx = 0; xx < lb->cLandBlockStruct.side_cell_count; xx++) {
                            for (int yy = 0; yy < lb->cLandBlockStruct.side_cell_count; yy++) {
                                var idx = ((yy + xx * lb->cLandBlockStruct.side_cell_count));
                                var idx2 = (2 * (yy + xx * lb->cLandBlockStruct.side_polygon_count));
                                var cell = lb->cLandBlockStruct.lcell[idx];
                                //ImGui.Text($"cell {idx}: {cell.in_view}");
                                var cSize = bSize / lb->cLandBlockStruct.side_cell_count;
                                var m = lb->cLandBlockStruct.side_cell_count - 1;
                                var tl2 = tl + new Vector2(xx, m - yy) * cSize;
                                var visible2 = cell.in_view != BoundingType.OUTSIDE;
                                var isCurrent2 = (uint)(lbid + (idx + 1)) == (lscape->loaded_cell_id);
                                dl.AddRect(tl2, tl2 + cSize, isCurrent2 ? 0xFFFF00FF : visible2 ? 0xFFFF7700 : 0x24FF7700, 0, ImDrawFlags.None, 1f);
                            }
                        }
                    }
                }
            }
            dl.PopClipRect();
        }

        public void Dispose() {

        }
    }
}
