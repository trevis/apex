﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Tools {
    public interface ITool : IDisposable {
        public string Name { get; }
        public bool IsActive { get; set; }
        public void Render();
    }
}
