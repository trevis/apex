﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using ImGuizmoNET;
using Microsoft.DirectX;
using System.Numerics;
using Microsoft.DirectX.Direct3D;
using Apex.Lib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.ACClientModule;
using UtilityBelt.Service;
using ACE.DatLoader.FileTypes;
using Vector3 = System.Numerics.Vector3;
using Apex.Lib.Extensions;
using System.Security.Cryptography;

namespace Apex.Tools {
    public unsafe class TransformTool : ITool {
        public enum SelectionMode {
            StaticObject,
            WorldObject,
            Terrain
        };

        public string Name => "Transform";
        public bool IsActive { get; set; }
        public SelectionMode CurrentSelectionMode { get; set; }
        public PhysicsObjTransformer ObjTransformer { get; }

        private CPhysicsObj* _selectedPhysicsObject = null;
        private bool _isSelectingPhysicsObject = false;

        private readonly string[] _availableSelectionModes;

        public TransformTool() {
            ObjTransformer = new PhysicsObjTransformer();

            _availableSelectionModes = System.Enum.GetNames(typeof(SelectionMode));
        }

        public void Render() {
            try {
                var selectionMode = (int)CurrentSelectionMode;
                if (ImGui.Combo("Selection Mode", ref selectionMode, _availableSelectionModes, _availableSelectionModes.Length)) {
                    CurrentSelectionMode = (SelectionMode)selectionMode;
                    _isSelectingPhysicsObject = false;
                }

                switch (CurrentSelectionMode) {
                    case SelectionMode.WorldObject:
                        RenderWorldObjectSelection();
                        break;
                    case SelectionMode.StaticObject:
                        RenderStaticObjectSelection();
                        break;
                    case SelectionMode.Terrain:
                        RenderTerrainSelection();
                        break;
                }
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        private void RenderStaticObjectSelection() {
            if (ImGui.Button(_isSelectingPhysicsObject ? "Cancel Selection" : "Select Static Object")) {
                _isSelectingPhysicsObject = !_isSelectingPhysicsObject;
            }

            if (_isSelectingPhysicsObject) {
                _selectedPhysicsObject = null;
                ObjTransformer.ClearTarget();
                RenderStaticObjectSelector();
            }

            if (_selectedPhysicsObject is not null) {
                ImGui.Text($"Selected physics obj: {(int)_selectedPhysicsObject:X8}");
                if (ImGui.Button("Paste")) {
                    var newObj = CPhysicsObj.makeObject(_selectedPhysicsObject);
                    CObjectMaint.s_pcInstance[0]->AddObject(newObj);
                    newObj->enter_world(&_selectedPhysicsObject->m_position);
                    _selectedPhysicsObject = newObj;
                }
                ObjTransformer.SetTarget(_selectedPhysicsObject);
                ObjTransformer.Render(true);
                RenderPhysicsObject(_selectedPhysicsObject, 0xFF00FFFF);

                _selectedPhysicsObject->update_position();
            }
        }

        private void RenderStaticObjectSelector() {
            var pickRay = PluginCore.Instance.Picker.GetPickRay();
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            var cEnvCell = VisibleCellsTool.CEnvCell_GetVisible(SmartBox.smartbox[0]->viewer_cell->pos.objcell_id);

            Coordinates? bestHit = null;
            CPhysicsObj* bestObj = null;

            if (GetStaticObjectHit(cEnvCell, pickRay, out var newBestHit, out var newBestObj)) {
                bestHit = newBestHit;
                bestObj = newBestObj;
                if (bestHit == null || newBestHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords)) {
                    bestHit = newBestHit;
                    bestObj = newBestObj;
                }
            }
            for (var i = 0; i < cEnvCell->cObjCell.num_stabs; i++) {
                var cell = VisibleCellsTool.CEnvCell_GetVisible(cEnvCell->cObjCell.stab_list[i]);
                if (GetStaticObjectHit(cell, pickRay, out var newBestHit2, out var newBestObj2)) {
                    bestHit = newBestHit2;
                    bestObj = newBestObj2;
                    if (bestHit == null || newBestHit2.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords)) {
                        bestHit = newBestHit2;
                        bestObj = newBestObj2;
                    }
                }
            }

            if (bestObj is not null) {
                RenderPhysicsObject(bestObj);
                if (ImGui.GetIO().MouseClicked[(int)ImGuiMouseButton.Left]) {
                    _selectedPhysicsObject = bestObj;
                    _isSelectingPhysicsObject = false;
                }
            }
        }

        private bool GetStaticObjectHit(CEnvCell* cEnvCell, Ray pickRay, out Coordinates bestHit, out CPhysicsObj* bestObj) {
            bestHit = null;
            bestObj = null;
            var cameraCoords = PluginCore.Instance.CameraH.Coordinates;
            for (var i = 0; i < cEnvCell->num_static_objects; i++) {
                var sObj = cEnvCell->static_objects[i];

                CSphere sphere = new CSphere();
                var ret = sObj->GetSelectionSphere(&sphere);
                sphere.center.x = sObj->m_position.frame.m_fOrigin.x;
                sphere.center.y = sObj->m_position.frame.m_fOrigin.y;
                sphere.center.z = sObj->m_position.frame.m_fOrigin.z;
                sphere.radius *= sObj->m_scale;

                if (ret > 0 && pickRay.IntersectsSphere(sphere)) {
                    var newHit = sObj->m_position.ToCoords();
                    if (bestHit is null || newHit.DistanceTo(cameraCoords) < bestHit.DistanceTo(cameraCoords)) {
                        bestHit = newHit;
                        bestObj = sObj;
                    }
                }
            }

            return bestHit is not null;
        }

        private void RenderPhysicsObject(CPhysicsObj* obj, uint color = 0xFF00FF00) {
            var dl = ImGui.GetBackgroundDrawList();
            for (var j = 0; j < obj->part_array->num_parts; j++) {
                var part = obj->part_array->parts[j];
                var partMat = part->pos.ToMatrix();
                for (var k = 0; k < part->gfxobj[0]->num_polygons; k++) {
                    var poly = part->gfxobj[0]->polygons[k];
                    for (var q = 0; q < poly.num_pts; q++) {
                        var a = new Vector3(
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].x,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].y,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[q]].z);
                        var idxb = q == 0 ? poly.num_pts - 1 : q - 1;
                        var b = new Vector3(
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].x,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].y,
                            part->gfxobj[0]->vertex_array.vertices[poly.vertex_ids[idxb]].z);

                        dl.AddLine(Vector3.Transform(a * obj->m_scale, partMat), Vector3.Transform(b * obj->m_scale, partMat), color, 1f);
                    }
                }
            }
        }

        private void RenderTerrainSelection() {
            ImGui.Text($"Terrain mode...");
        }

        private void RenderWorldObjectSelection() {
            var selectedWorldObject = PluginCore.Instance.Game.World.Selected;
            var obj = selectedWorldObject is null ? null : CPhysicsObj.GetObjectA(selectedWorldObject.Id);

            if (selectedWorldObject != null) {
                ImGui.Text($"Selected: {(selectedWorldObject is null ? "none" : selectedWorldObject)}");
                ObjTransformer.SetTarget(obj);
                ObjTransformer.Render(true);
            }
            else {
                ObjTransformer.ClearTarget();
                return;
            }
        }
        public void Dispose() {
            
        }
    }
}
