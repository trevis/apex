﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Tools {
    public unsafe class PropTool : ITool {
        public string Name => "Props";
        public bool IsActive { get; set; }

        public PropTool() {

        }

        public void Render() {
            var selected = (uint)CoreManager.Current.Actions.CurrentSelection;
            if (selected == 0) return;

            var physObj = CPhysicsObj.GetObjectA(selected);
            var weenie = physObj->weenie_obj;
            var desc = weenie->pwd;

            var name = Marshal.PtrToStringAnsi((IntPtr)weenie->GetObjectName(NameType.NAME_SINGULAR, 0));

            ImGui.Text($"{name}");
            ImGui.Text($"{physObj->get_frame()->ToString()}");
        }

        public void Dispose() {

        }
    }
}
