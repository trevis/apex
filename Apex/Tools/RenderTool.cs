﻿using AcClient;
using Decal.Adapter;
using ImGuiNET;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apex.Tools {
    public unsafe class RenderTool : ITool {
        private bool wireframe;
        private bool fogDisabled;

        public string Name => "Render";
        public bool IsActive { get; set; }

        public RenderTool() {

        }

        public void Render() {
            if (ImGui.Checkbox("Wireframe", ref wireframe)) {
                (RenderDevice.render_device[0])->m_WireframeMode = (byte)(wireframe ? 1 : 0);
            }
            if (ImGui.Checkbox("Disable Fog", ref fogDisabled)) {
                SmartBox.DisableFogging(fogDisabled ? 1 : 0);
            }
        }

        public void Dispose() {

        }
    }
}
